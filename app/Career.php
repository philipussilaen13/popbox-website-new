<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $table = 'tb_website_career';
    protected $primaryKey = 'id_career';

    public static function getActiveCareer(){
        $careerDb = self::where('status','active')->get();
        return $careerDb;
    }
}
