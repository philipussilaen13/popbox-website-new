<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('sesi',function(){
	return \Session::all();
});

Route::get('/','SiteController@index');
Route::get('language/{locale}','SiteController@changeLanguage');

Route::get('contact','SiteController@getContact');
Route::post('contact','SiteController@postContact');

//tracking parcel section
Route::get('track','SiteController@getTrack');
Route::post('track','SiteController@postTrack');
// Route::get('tracking','SiteController@viewTracking');
Route::get('tracking/{q?}','SiteController@detailTracking');

//locker availability section
Route::post('lockerList','SiteController@postLockerList');
Route::post('lockerList-my','SiteController@postLockerListMy');
Route::post('lockerAvailability','SiteController@postLockerAvailability');
Route::post('lockerAvailabilityByID','SiteController@postLockerAvailabilityByID');

Route::get('article/{date}/{title}/{id}','SiteController@getArticleContent');
Route::get('career','SiteController@getCareer');
Route::get('faq','SiteController@getFaq');
Route::get('merchant','SiteController@getMerchant');
Route::get('location',function (){
    return \Illuminate\Support\Facades\Redirect::to('https://location.popbox.asia/id');
});
Route::get('promo','SiteController@getPromo');
Route::get('testimonial','SiteController@getTestimonial');
Route::get('blog','SiteController@getBlog');
Route::get('locker','SiteController@getLockerShipping');
Route::get('lockermy','SiteController@getLockerShippingMy');
Route::get('ondemand','SiteController@getOnDemandPage');
Route::get('lockers/{code}','SiteController@lockers');
Route::get('districts/{code}','SiteController@districts');

// lockerMaps
Route::get('locker/maps/{code}','SiteController@lockerMaps');
Route::get('locker/maps/{code}/{city}','SiteController@lockerMaps');

// Merchant
Route::get('omaisu','MerchantController@getOmaisuLanding');
Route::post('omaisu','MerchantController@postOmaisuOrder');
//Route::post('contactOmaisu','MerchantController@postOmaisuContact');

Route::get('tayaka','MerchantController@getTayakaLanding');
Route::post('tayaka','MerchantController@postTayakaOrder');

// disabled per 5 maret 2019
//Route::get('vcleanshoes','MerchantController@getVCleanLanding');
//Route::post('vcleanshoes','MerchantController@postVCleanOrder');

Route::get('csr','MerchantController@getCSRLanding');
Route::any('csrsubmit','MerchantController@postCSROrder');

Route::group(['prefix'=>'merchant'],function (){
    Route::get('registration','MerchantController@landingRegistration');
    Route::get('register','MerchantController@formRegistration');
    Route::post('register','MerchantController@postRegister');
    Route::get('successregister','MerchantController@successRegister');
    Route::get('activation/{code}/{email}','MerchantController@activationByEmail');    
    Route::get('howto','MerchantController@howto'); 
    Route::get('neuboditc','MerchantController@neuboditc'); 
    Route::get('neubodithankyou','MerchantController@neubodithankyou'); 
});

Route::group(['prefix'=>'merchant1'],function (){
    Route::get('register','Merchant1Controller@postRegister');
});

Route::group(['prefix'=>'dashboards'],function (){	    
    Route::get('uploadpickup','MerchantpickupController@uploadpickup');   
    Route::get('uploadpickupsingle','MerchantpickupController@uploadpickupsingle');
    Route::post('postpickup','MerchantpickupController@postpickup');        
    Route::get('pickupdetail','MerchantpickupController@pickupdetail');    
    Route::post('uploadpickupxls','MerchantpickupController@uploadpickupxls'); 
    Route::any('listuploadpickup','MerchantpickupController@listuploadpickup'); 
    Route::group(['prefix'=>'myshop'],function (){  
        Route::get('/','MerchantpickupController@myshop'); 
        Route::get('edit','MerchantpickupController@myshopedit'); 
        Route::post('update','MerchantpickupController@myshopupdate'); 
    });
    Route::get('panduan','MerchantpickupController@panduan');     
    Route::get('tracking','MerchantpickupController@tracking');
});

Route::get('email','MerchantpickupController@testemail'); 

Route::controller('/user', 'UserController');

Route::get('harbolnas','HarbolnasController@index');
Route::any('cnydeals','HarbolnasController@cnydeals');	
Route::any('valentine','HarbolnasController@valentine');
Route::any('bebasongkir','HarbolnasController@promomarch');
Route::post('submitpromomarch','HarbolnasController@submitpromomarch');
Route::any('marchpromo','HarbolnasController@pm');
Route::get('gramedia','LandingController@getGramediaLanding');
Route::get("popboxthematic", "HarbolnasController@popboxthematic");
Route::get('cop','LandingController@getCopLanding');
Route::get('/promopulsa', ['as' => 'promopulsa', 'uses' => 'LandingController@getPulsaLanding']);
Route::get('indosatpromo','LandingController@indosat');
Route::get('/ramadhanpromo', ['as' => 'ramadhan', 'uses' => 'LandingController@ramadhan']);
Route::any('/popboxagent', ['as' => 'popboxagent', 'uses' => 'LandingController@popboxagent']);
Route::any('/submitpopboxagent', ['as' => 'submitpopboxagent', 'uses' => 'LandingController@submit_popbox_agent']);
Route::get('popboxanniv','LandingController@getAnniv2');
Route::get('popboxanniv/cara','LandingController@getAnniv2cara');
Route::get('popboxanniv/syarat','LandingController@getAnniv2syarat');
Route::get('popboxanniv/promo','LandingController@getAnniv2promo');
Route::get('harbolnas17','HarbolnasController@harbolnas17');
    
// return landing
Route::get('return','LandingController@getReturnLandingNew');
Route::get('returnv1','LandingController@getReturnLanding');
Route::get('lostandfound','LandingController@getLostandfound');
Route::get('payday','LandingController@payday');
