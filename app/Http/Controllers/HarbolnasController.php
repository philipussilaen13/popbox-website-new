<?php
/**
 * Created by PhpStorm.
 * User: Arief Demiawansyah
 * Date: 29/11/2016
 * Time: 11.36
 */

namespace App\Http\Controllers;

use App\Http\Helper\API;
use App\Http\Helper\Locker;
use Illuminate\Http\Request;
use App\Http\Helper\Helper;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HarbolnasController extends Controller
{
    public function index(){    	
    	$locker = Helper::getLockerbyCities();
    	$data["lockers"] = $locker["lockers"];
    	$data["lockerBycities"] = $locker["lockerBycities"];    	
        return view('harbonas.index',$data);
    } 

    public function cnydeals(Request $req){    	
        $isLocation = $req->input("location","");
        if ($isLocation!=""){
            $long = $req->input("long");
            $lat = $req->input("lat");
            $locker = Helper::getLockerNearest($long, $lat);               
            return response()->json($locker);               
        }else{
        	$locker = Helper::getLockerbyCities();    	    	
        	$data["lockers"] = $locker["lockers"];
        	$data["lockerBycities"] = $locker["lockerBycities"];

            $lockerJkt = array();
            $lockerList = array();

            $data['lockerJkt'] = $lockerJkt;

            $set = 'Malaysia';
            $locker = Locker::getAllLocker($set);
            if ($locker) $lockerList = $locker;

            foreach ($lockerList as $index=>$locker){
                $lockerJkt[$index]['name'] = $locker->name;
                $lockerJkt[$index]['address'] = $locker->address;
                $lockerJkt[$index]['address_2'] = $locker->address_2;
                $lockerJkt[$index]['city'] =  $locker->district;
                $lockerJkt[$index]['province'] =  $locker->province;
                $lockerJkt[$index]['latitude'] =  $locker->latitude;
                $lockerJkt[$index]['longitude'] =  $locker->longitude;
                $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
            }   
            $data['lockerJkt'] = $lockerJkt;
            return view('harbonas.cnydeals',$data);
        }
    }

    public function valentine(Request $req){
        $locker = Helper::getLockerbyCities();              
        $data["lockers"] = $locker["lockers"];
        $data["lockerBycities"] = $locker["lockerBycities"];

         $lockerJkt = array();
            $lockerList = array();

            $data['lockerJkt'] = $lockerJkt;

            $set = 'Malaysia';
            $locker = Locker::getAllLocker($set);
            if ($locker) $lockerList = $locker;

            foreach ($lockerList as $index=>$locker){
                $lockerJkt[$index]['name'] = $locker->name;
                $lockerJkt[$index]['address'] = $locker->address;
                $lockerJkt[$index]['address_2'] = $locker->address_2;
                $lockerJkt[$index]['city'] =  $locker->district;
                $lockerJkt[$index]['province'] =  $locker->province;
                $lockerJkt[$index]['latitude'] =  $locker->latitude;
                $lockerJkt[$index]['longitude'] =  $locker->longitude;
                $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
            }   
            $data['lockerJkt'] = $lockerJkt;
        return view('harbonas.valentine', $data);
    }

    public function valentineid(Request $req){
        $locker = Helper::getLockerbyCities("id");              
        $data["lockers"] = $locker["lockers"];
        $data["lockerBycities"] = $locker["lockerBycities"];

         $lockerJkt = array();
            $lockerList = array();

            $data['lockerJkt'] = $lockerJkt;

            $set = 'Indonesia';
            $locker = Locker::getAllLocker($set);

            if ($locker) $lockerList = $locker;

            foreach ($lockerList as $index=>$locker){
                $lockerJkt[$index]['name'] = $locker->name;
                $lockerJkt[$index]['address'] = $locker->address;
                $lockerJkt[$index]['address_2'] = $locker->address_2;
                $lockerJkt[$index]['city'] =  $locker->district;
                $lockerJkt[$index]['province'] =  $locker->province;
                $lockerJkt[$index]['latitude'] =  $locker->latitude;
                $lockerJkt[$index]['longitude'] =  $locker->longitude;
                $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
            }
            $data['lockerJkt'] = $lockerJkt;
        return view('harbonas.valentineid', $data);
    }

    public function promomarch(){
        $locker = Helper::getLockerbyCities("id");              
        $data["lockers"] = $locker["lockers"];
        $data["lockerBycities"] = $locker["lockerBycities"];

        $lockerJkt = array();
        $lockerList = array();

        $data['lockerJkt'] = $lockerJkt;

        $set = 'Indonesia';
        $locker = Locker::getAllLocker($set);

        if ($locker) $lockerList = $locker;

        foreach ($lockerList as $index=>$locker){
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] =  $locker->district;
            $lockerJkt[$index]['province'] =  $locker->province;
            $lockerJkt[$index]['latitude'] =  $locker->latitude;
            $lockerJkt[$index]['longitude'] =  $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
            }   
            $data['lockerJkt'] = $lockerJkt;
        return view('harbonas.promomarch', $data);
    }

    public function submitpromomarch(Request $request){
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        $g_recaptcha_response = $request->input('g-recaptcha-response');
        $ip = $request->ip();

        $param = array();
        $param['secret'] = config("config.capcha.secretkey");
        $param['response'] = $g_recaptcha_response;
        $param['remoteip'] = $ip;

        $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$param['secret'].'&response='.$param['response'].'&remoteip='.$param['remoteip'];
        $param = json_encode($param);

        $opts['http']['method'] = 'POST';
        $opts['http']['timeout'] = 180;
        $opts['http']['content'] = $param;
        $context  = stream_context_create($opts);
        $response = @file_get_contents($url, false, $context);

        if ($response) {
            $response = json_decode($response);
            if($response->success==false){
                return \Redirect::back()->withErrors(['Failed Validate Captcha']);                                
            }
        }

        $params = array();
        $params["token"] = "";
        $name= $request->input("nama","");
        $email = $request->input("email","");
        $phone = $request->input("phone","");
        $subject = $request->input("subject","");
        $message = $request->input("message","");       
        $api = new API();
        $resp = $api->contactUs($name, $email, $phone, $subject, $message);
        if ($resp->response->code == 200){ 
            \Session::flash('message', $resp->response->message);
            return \Redirect::back();
        }else{
            return \Redirect::back()->withErrors(['Gagal insert']);
            
        }
    }   

    public function pm(Request $req){
        $locker = Helper::getLockerbyCities();              
        $data["lockers"] = $locker["lockers"];
        $data["lockerBycities"] = $locker["lockerBycities"];

         $lockerJkt = array();
            $lockerList = array();

            $data['lockerJkt'] = $lockerJkt;

            $set = 'Malaysia';
            $locker = Locker::getAllLocker($set);
            if ($locker) $lockerList = $locker;

            foreach ($lockerList as $index=>$locker){
                $lockerJkt[$index]['name'] = $locker->name;
                $lockerJkt[$index]['address'] = $locker->address;
                $lockerJkt[$index]['address_2'] = $locker->address_2;
                $lockerJkt[$index]['city'] =  $locker->district;
                $lockerJkt[$index]['province'] =  $locker->province;
                $lockerJkt[$index]['latitude'] =  $locker->latitude;
                $lockerJkt[$index]['longitude'] =  $locker->longitude;
                $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
            }   
            $data['lockerJkt'] = $lockerJkt;
        return view('harbonas.promomy', $data);
    }

    public function popboxthematic(Request $req){
         return view('harbonas.promomly');       
    }

    public function harbolnas17(Request $request)
    {
        // Get Region from IP
        if (!$request->session()->has('app.region')){
            $region = Helper::ipGeoPlugin($request->ip());
            if (!empty($region)){
                $region = strtolower($region);
                $listedRegion = ['id','my'];
                if (!in_array($region,$listedRegion)) $region = "id";
                App::setLocale($region);
                $request->session()->set('app.region',$region);
            }
        }
        $region = Session::get('app.region','id');

        $country = ['id'=>'Indonesia','my'=>'Malaysia'];
        if (!isset($country[$region])) {
            $set = 'Indonesia';
        } else $set = $country[$region];

        //get locker jakarta for map section
        $lockerList = array();
        $lockerJkt = array();
        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        foreach ($lockerList as $index=>$locker){
            $lockerJkt[$index]['id'] = $locker->id;
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] =  $locker->district;
            $lockerJkt[$index]['province'] =  $locker->province;
            $lockerJkt[$index]['latitude'] =  $locker->latitude;
            $lockerJkt[$index]['longitude'] =  $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
        }
        //Get Cities array
        $citiesList = array();
        foreach ($lockerList as $item) {
            if (!in_array($item->district,$citiesList)){
                $citiesList[] = $item->district;
            }
        }

        $data['lockerJkt'] = $lockerJkt;
        $data['citiesList'] = $citiesList;

        return view('harbonas.harbolnas17', $data);
    }
       
}