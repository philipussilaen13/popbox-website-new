<?php

namespace App\Http\Controllers;

use App\Article;
use App\Career;
use App\Http\Helper\API;
use App\Http\Helper\Helper;
use App\Http\Helper\Locker;
use App\Merchant;
use App\Models\VirtualLockers;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class SiteController extends Controller
{
    public function changeLanguage($locale = 'id')
    {
        Session::set('app.region', $locale);
        App::setLocale($locale);
        return back();
    }

    public function index(Request $request)
    {
        // Get Region from IP
        if (!$request->session()->has('app.region')) {
            $region = Helper::ipGeoPlugin($request->ip());
            if (!empty($region)) {
                $region = strtolower($region);
                $listedRegion = ['id', 'my'];
                if (!in_array($region, $listedRegion)) $region = "id";
                App::setLocale($region);
                $request->session()->set('app.region', $region);
            }
        }
        $region = Session::get('app.region', 'id');

        $country = ['id' => 'Indonesia', 'my' => 'Malaysia'];
        if (!isset($country[$region])) {
            $set = 'Indonesia';
        } else $set = $country[$region];

        //get locker jakarta for map section
        $lockerList = [];
        $lockerJkt = [];
        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        foreach ($lockerList as $index => $locker) {
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] = $locker->district;
            $lockerJkt[$index]['province'] = $locker->province;
            $lockerJkt[$index]['latitude'] = $locker->latitude;
            $lockerJkt[$index]['longitude'] = $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
        }
        //Get Cities array
        $citiesList = [];
        foreach ($lockerList as $item) {
            if (!in_array($item->district, $citiesList)) {
                $citiesList[] = $item->district;
            }
        }

        if ($request->input("activation") != null) {
            $activation = $request->input("activation");
            $user = \DB::table("tb_users")
                ->join('tb_user_verification', 'tb_users.id', '=', 'tb_user_verification.user_id')
                ->where("tb_user_verification.email_verification", $activation)->first();
            if (isset($user)) {
                $data["activation"] = $activation;
                $data["activateact"] = $user;
            }
        }


        //get all locker for counting section
        $allLocker = Locker::getAllLocker($set);
        $districts = [];
        $compartments = 0;
        foreach ($allLocker as $locker) {
            if (!in_array($locker->district, $districts)) $districts[] = $locker->district;
            $compartments += $locker->locker_capacity;
        }
        //get article for home

        /*$articlesDb = Article::getActiveArticle(4);

        foreach ($articlesDb as $article){
            $article->content = preg_replace('/<img[^>]+\>/i','',$article->content);
            $article->url = date('Ymd',strtotime($article)).'/'.$article->id_article.'/'.$article->short_title;
        }*/
        //get news for home
        $newsDb = Article::getNews();

        // get merchant list from DB
        $merchantList = Merchant::getMerchantIndex();

        // get gallery for locker
        $gallery = Locker::getLockerGalleryDb($region);

        // create array for image slider
        $sliders = Locker::getLockerSliderDb($region);

        // get testimonial
        $testi = Article::getTestimonial($region);
        $testimonials = [];
        foreach ($testi as $key => $element) {
            if ($key == 2) break;
            $testimonials[] = $element;
        }

        // get virtual locker / agent
        $virtualLockerDb = VirtualLockers::getVirtualLocker();
        $virtualLockerList = [];

        foreach ($virtualLockerDb as $index => $item) {
            $virtualLockerList[$index]['name'] = $item->locker_name;
            $virtualLockerList[$index]['address'] = $item->address;
            $virtualLockerList[$index]['address_2'] = $item->detail_address;
            $virtualLockerList[$index]['city'] = $item->city_name;
            $virtualLockerList[$index]['province'] = $item->province_name;
            $virtualLockerList[$index]['latitude'] = $item->latitude;
            $virtualLockerList[$index]['longitude'] = $item->longitude;
            //$virtualLockerList[$index]['operational_hours'] = $locker->operational_hours;
        }
        if ($request->input("activation") != null) {
            $activation = $request->input("activation");
            $user = \DB::table("tb_users")
                ->join('tb_user_verification', 'tb_users.id', '=', 'tb_user_verification.user_id')
                ->where("tb_user_verification.email_verification", $activation)->first();
            if (isset($user)) {
                $data["activation"] = $activation;
                $data["activateact"] = $user;
            }
        }

        $data['lockerJkt'] = $lockerJkt;
        $data['countLocker'] = count($allLocker);
        $data['countDistrict'] = count($districts);
        $data['countCompartment'] = $compartments;
        $data['citiesList'] = $citiesList;
        $data['merchantList'] = $merchantList;
        $data['sliders'] = $sliders;
        $data['gallery'] = $gallery;
        // $data['articles'] = $articlesDb;
        $data['news'] = $newsDb;
        $data['region'] = $region;
        $data['testimonials'] = $testimonials;
        $data['region'] = $region;
        $data['virtualLockerList'] = $virtualLockerList;
        return view('index', $data);
    }

    public function getContact()
    {
        return view('contact');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function postContact(Request $request)
    {
        $result = new \stdClass;
        $result->isSuccess = false;
        $this->validate($request, [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $subject = $request->input('subject');
        $message = $request->input('message');

        // Validate Google RreCaptcha
        $g_recaptcha_response = $request->input('g-recaptcha-response');
        $ip = $request->ip();

        $param = [];
        $param['secret'] = '6LerZAsUAAAAAMQMxgfQcWx7FkMqDe_TF88G53d7';
        $param['response'] = $g_recaptcha_response;
        $param['remoteip'] = $ip;

        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $param['secret'] . '&response=' . $param['response'] . '&remoteip=' . $param['remoteip'];
        $param = json_encode($param);

        $opts['http']['method'] = 'POST';
        $opts['http']['timeout'] = 180;
        $opts['http']['content'] = $param;
        $context = stream_context_create($opts);
        $response = @file_get_contents($url, false, $context);

        if ($response) {
            $response = json_decode($response);
            if ($response->success == false) {
                $result->errorMsg = 'Failed Validate Captcha';
                return json_encode($result);
            }
        }

        $API = new API();
        $postSubmit = $API->contactUs($name, $email, $phone, $subject, $message);

        if ($postSubmit->response->code != 200) {
            //kalau gagal
            $result->errorMsg = 'Failed to Submit Form';
            return json_encode($result);
        }

        $result->isSuccess = true;
        return json_encode($result);
    }

    /**
     * make view on article content
     * @param null $date
     * @param null $title
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getArticleContent($date = null, $title = null, $id = null)
    {
        $isArticle = true;
        //find by id first
        if (!empty($id)) {
            $articleDb = Article::find($id);
            // find by date and short title
            if (!$articleDb) {
                $articleDb = Article::where('date', date('Y-m-d', strtotime($date)))->where('short_title', $title)->where('status', 'publish')->first();
                if (!$articleDb) $isArticle = false;
            }
        }
        if ($isArticle == false) {
            return redirect('promo');
        }

        $data['articleDb'] = $articleDb;
        return view('article-single-full', $data);
    }

    /**
     * get promo page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPromo(Request $request)
    {
        //get article for index promo
        if (!$request->session()->has('app.region')) {
            $region = Helper::ipGeoPlugin($request->ip());
            if (!empty($region)) {
                $region = strtolower($region);
                $listedRegion = ['id', 'my'];
                if (!in_array($region, $listedRegion)) $region = "id";
                App::setLocale($region);
                $request->session()->set('app.region', $region);
            }
        }
        $region = Session::get('app.region', 'id');
        $pastPromo = Article::getPastPromo($region);
        foreach ($pastPromo as $article) {
            $article->content = preg_replace('/<img[^>]+\>/i', '', $article->content);
            $article->url = date('Ymd', strtotime($article)) . '/' . $article->id_article . '/' . $article->short_title;
        }
        $activePromo = Article::getActivePromo();
        foreach ($activePromo as $article) {
            $article->content = preg_replace('/<img[^>]+\>/i', '', $article->content);
            $article->url = date('Ymd', strtotime($article)) . '/' . $article->id_article . '/' . $article->short_title;
        }
        //get article which promo only
        $data['pastPromo'] = $pastPromo;
        $data['activePromo'] = $activePromo;
        return view('promo', $data);
    }

    /**
     * get blog page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBlog()
    {
        $data = [];
        $region = Session::get('app.region', 'id');
        $data['activeBlog'] = Article::getActiveBlog($region);
        $data['recentBlog'] = Article::getRecentBlog($region);
        return view('news', $data);
    }

    /**
     * get career page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCareer()
    {
        //get career active posting
        $careerDb = Career::getActiveCareer();

        $ulPattern = '/<ul>/';
        $ulReplace = "<ul class='iconlist iconlist-color nobottommargin'>";
        $liPattern = '/<li (.*?)>/i';
        $liReplace = "<li><i class='icon-ok'></i>";
        foreach ($careerDb as $element) {
            $tmp = preg_replace($ulPattern, $ulReplace, $element->requirement);
            $element->requirement = $tmp;
            $tmp = preg_replace($liPattern, $liReplace, $element->requirement);
            $element->requirement = $tmp;

            $tmp = preg_replace($ulPattern, $ulReplace, $element->job_desc);
            $element->job_desc = $tmp;
            $tmp = preg_replace($liPattern, $liReplace, $element->job_desc);
            $element->job_desc = $tmp;;
        }

        $data['careerDb'] = $careerDb;
        return view('career', $data);
    }

    /**
     * get FAQ page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFaq(Request $request)
    {
        $lang = $request->session()->get('app.region', 'id');
        return view('faq')->with('lang', $lang);
    }

    /**
     * get tracking page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTrack()
    {
        return view('track');
    }

    /**
     * posting tracking page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function postTrack(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $orderNumber = $request->input('order');
        $parcel = null;
        $api = new API();
        $result = $api->parcelStatus($orderNumber);
        if ($result) {
            if ($result->response->code != 200) {
                $response->errorMsg = 'Data Not Found';
                return response()->json($response);
            }
            if (empty($result->data)) {
                $response->errorMsg = 'Failed to get parcel status';
                return response()->json($response);
            }
            if ($result->data == "Not Found") {
                $response->errorMsg = 'Parcel Not Found';
                return response()->json($response);
            }
            $parcel = $result->data[0];
            $parcel->status = str_replace('_', ' ', $parcel->status);
            $parcel->storetime = empty($parcel->storetime) ? null : date('l, j M Y H:i:s', strtotime($parcel->storetime));
            $parcel->taketime = empty($parcel->taketime) ? null : date('l, j M Y H:i:s', strtotime($parcel->taketime));
        }
        $response->data = $parcel;
        $response->isSuccess = true;
        return response()->json($response);
    }

    /**
     * get merchant page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMerchant()
    {
        $merchantList = Merchant::all();
        $filtered = $merchantList->filter(function ($value, $key) {
            return $value->category != 'demand';
        });

        $merchantList = $filtered->all();
        $data['merchantList'] = $merchantList;
        return view('merchant', $data);
    }

    /**
     * get List Locker based on city/district
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLockerList(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        //get city selected
        $city = $request->input('cityName');
        if (empty($city)) {
            $response->errorMsg = 'City must not empty';
            return response()->json($response);
        }

        //get region
        $region = Session::get('app.region', 'id');
        $country = ['id' => 'Indonesia', 'my' => 'Malaysia'];
        if (!isset($country[$region])) {
            $set = 'Indonesia';
        } else $set = $country[$region];
        $lockers = Locker::getAllLocker($set);

        $matchLocker = [];
        foreach ($lockers as $locker) {
            if ($city == $locker->district) {
                $tmp = new \stdClass();
                $tmp->address = $locker->address;
                $tmp->address_2 = $locker->address_2;
                $tmp->country = $locker->country;
                $tmp->district = $locker->district;
                $tmp->latitude = $locker->latitude;
                $tmp->longitude = $locker->longitude;
                $tmp->name = $locker->name;
                $tmp->operational_hours = $locker->operational_hours;
                $tmp->province = $locker->province;
                $tmp->zip_code = $locker->zip_code;

                $matchLocker[] = $tmp;
            }
        }
        if (empty($matchLocker)) {
            $response->errorMsg = 'Locker not Found';
            return response()->json($response);
        }
        $response->data = $matchLocker;
        $response->isSuccess = true;

        return response()->json($response);
    }

    public function postLockerAvailability(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $lockerName = $request->input('lockerName');
        $lockerResult = new API();
        $result = $lockerResult->lockerAvailability($lockerName);

        if ($result->response->code != 200) {
            $response->errorMsg = 'Failed to get Locker Availability';
            return response()->json($response);
        }

        $availabilityData = $result->data;
        $availaibility = new \stdClass();
        $xs = $s = $m = $l = $xl = 0;
        foreach ($availabilityData as $item) {
            foreach ($item as $size) {
                if (isset($size->xs)) $xs = (int)$size->xs;
                elseif (isset($size->s)) $s = (int)$size->s;
                elseif (isset($size->m)) $m = (int)$size->m;
                elseif (isset($size->l)) $l = (int)$size->l;
                elseif (isset($size->xl)) $xl = (int)$size->xl;
            }
        }
        $availaibility->xs = $xs;
        $availaibility->s = $s;
        $availaibility->m = $m;
        $availaibility->l = $l;
        $availaibility->xl = $xl;
        $response->data = $availaibility;
        $response->isSuccess = true;
        return response()->json($response);
    }

    /**
     * get testimonial page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTestimonial(Request $request)
    {
        $region = Session::get('app.region', 'id');
        $dataDb = Article::getTestimonial($region);
        $data['testimonials'] = $dataDb;
        return view('testimonial', $data);
    }

    /**
     * get locker shipping from merchant page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLockerShipping(Request $request)
    {
        // Get Region from IP
        if (!$request->session()->has('app.region')) {
            $region = Helper::ipGeoPlugin($request->ip());
            if (!empty($region)) {
                $region = strtolower($region);
                $listedRegion = ['id','my']; 
                if (!in_array($region,$listedRegion)) $region = "id"; 
                App::setLocale($region);
                $request->session()->set('app.region', $region);
            }
        }
        $region = Session::get('app.region', 'id');

        $country = ['id' => 'Indonesia', 'my' => 'Malaysia'];
        if (!isset($country[$region])) {
            $set = 'Indonesia';
        } else $set = $country[$region];

        //get locker jakarta for map section
        $lockerList = [];
        $locker = Locker::getAllLocker($set);

        if (empty($locker)) $locker = [];

        foreach ($locker as $index => $locker) {
            $lockerList[$index]['name'] = $locker->name;
            $lockerList[$index]['address'] = $locker->address;
            $lockerList[$index]['address_2'] = $locker->address_2;
            $lockerList[$index]['city'] = $locker->district;
            $lockerList[$index]['province'] = $locker->province;
            $lockerList[$index]['latitude'] = $locker->latitude;
            $lockerList[$index]['longitude'] = $locker->longitude;
            $lockerList[$index]['operational_hours'] = $locker->operational_hours;
        }

        //Get Cities array
        $citiesList = [];
        foreach ($lockerList as $index => $locker) {
            if (!in_array($locker['city'], $citiesList)) {
                $citiesList[] = $locker['city'];
            }
        }
        $data = [];
        $data['citiesList'] = $citiesList;
        return view('locker-shipping', $data);
    }

    public function getLockerShippingMy (Request $request)
    {
        $set = 'Malaysia';
        //get locker jakarta for map section
        $lockerList = [];
        $locker = Locker::getAllLocker($set);

        if (empty($locker)) $locker = [];

        foreach ($locker as $index => $locker) {
            $lockerList[$index]['name'] = $locker->name;
            $lockerList[$index]['address'] = $locker->address;
            $lockerList[$index]['address_2'] = $locker->address_2;
            $lockerList[$index]['city'] = $locker->district;
            $lockerList[$index]['province'] = $locker->province;
            $lockerList[$index]['latitude'] = $locker->latitude;
            $lockerList[$index]['longitude'] = $locker->longitude;
            $lockerList[$index]['operational_hours'] = $locker->operational_hours;
        }

        //Get Cities array
        $citiesList = [];
        foreach ($lockerList as $index => $locker) {
            if (!in_array($locker['city'], $citiesList)) {
                $citiesList[] = $locker['city'];
            }
        }
        $data = [];
        $data['citiesList'] = $citiesList;
        return view('locker-shipping-my', $data);
    }

    public function postLockerListMy(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        //get city selected
        $city = $request->input('cityName');
        if (empty($city)) {
            $response->errorMsg = 'City must not empty';
            return response()->json($response);
        }

        //get region
        $set = 'Malaysia';
        $lockers = Locker::getAllLocker($set);

        $matchLocker = [];
        foreach ($lockers as $locker) {
            if ($city == $locker->district) {
                $tmp = new \stdClass();
                $tmp->address = $locker->address;
                $tmp->address_2 = $locker->address_2;
                $tmp->country = $locker->country;
                $tmp->district = $locker->district;
                $tmp->latitude = $locker->latitude;
                $tmp->longitude = $locker->longitude;
                $tmp->name = $locker->name;
                $tmp->operational_hours = $locker->operational_hours;
                $tmp->province = $locker->province;
                $tmp->zip_code = $locker->zip_code;

                $matchLocker[] = $tmp;
            }
        }
        if (empty($matchLocker)) {
            $response->errorMsg = 'Locker not Found';
            return response()->json($response);
        }
        $response->data = $matchLocker;
        $response->isSuccess = true;

        return response()->json($response);
    }

    public function getOnDemandPage(Request $request)
    {
        // Get Region from IP
        if (!$request->session()->has('app.region')) {
            $region = Helper::ipGeoPlugin($request->ip());
            if (!empty($region)) {
                $region = strtolower($region);
                $listedRegion = ['id','my']; 
                if (!in_array($region,$listedRegion)) $region = "id"; 
                App::setLocale($region);
                $request->session()->set('app.region', $region);
            }
        }
        $region = Session::get('app.region', 'id');

        // get all data merchant
        $merchantDemand = Merchant::getOnDemandPage();
        // filter based on country and type
        $filtered = $merchantDemand->filter(function ($value, $key) use ($region) {
            return $value->region == $region;
        })->filter(function ($value, $key) {
            return $value->category == 'demand';
        })->all();

        $data = [];
        $data['merchant'] = $filtered;
        //        return $filtered;
        return view('ondemand', $data);
    }
}
