<?php
/**
 * Created by PhpStorm.
 * User: Arief Demiawansyah
 * Date: 29/11/2016
 * Time: 11.36
 */

namespace App\Http\Controllers;

use App\City;
use App\Http\Helper\API;
use App\Http\Helper\Locker;
use App\Province;
use Illuminate\Http\Request;

class MerchantController extends Controller
{
    public function getOmaisuLanding(){
        $set = 'Indonesia';

        $lockerList = array();
        $lockerJkt = array();
        $citiesList = array();

        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        $provinceAllowed = ['DKI Jakarta','Jawa Barat','Banten'];

        foreach ($lockerList as $index=>$locker){
            if (!in_array($locker->province,$provinceAllowed)) continue;
            if ($locker->district=='Bandung') continue;
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] =  $locker->district;
            $lockerJkt[$index]['province'] =  $locker->province;
            $lockerJkt[$index]['latitude'] =  $locker->latitude;
            $lockerJkt[$index]['longitude'] =  $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
        }

        foreach ($lockerJkt as $item) {
            if(!in_array($item['city'],$citiesList)) $citiesList[] = $item['city'];
        }

        $data['lockerJkt'] = $lockerJkt;
        $data['citiesList'] = $citiesList;
        return view('merchant.omaisu',$data);
    }

    public function postOmaisuOrder(Request $request){
        $result = new \stdClass;
        $result->isSuccess = false;

        $cust_name = $request->input('cust_name');
        $cust_email = $request->input('cust_email');
        $cust_phone = $request->input('cust_phone');
        $shoes_total = $request->input('shoes_total');
        $shoes_brand = $request->input('shoes_brand');
        $service = $request->input('services');
        $locker = $request->input('locker');

        if (empty($cust_name) || empty($cust_email) || empty($cust_phone) || empty($shoes_total) || empty($shoes_brand) || empty($service) || empty($locker)){
            $result->errorMsg = 'Missing Parameter';
            return response()->json($result);
        }

        // post data to API
        $api = new API();
        $response = $api->omaisuOrder($cust_name,$cust_email,$cust_phone,$shoes_total,$shoes_brand,$service,$locker);
        if (empty($response)){
            $result->errorMsg = 'Failed to Submit Order';
            return response()->json($result);
        }
        if ($response->response->code!=200){
            $result->errorMsg = $response->response->message;
            return response()->json($result);
        }

        $result->isSuccess = true;
        return response()->json($result);
    }

    public function postOmaisuContact(Request $request){
        $result = new \stdClass;
        $result->isSuccess = false;
        $this->validate($request,[
            'name'=>'required|regex:/^[\pL\s\-]+$/u',
            'email'=>'required|email',
            'message'=>'required'
        ]);
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $subject = $request->input('subject');
        $message = $request->input('message');

        // Validate Google RreCaptcha
        $g_recaptcha_response = $request->input('g-recaptcha-response');
        $ip = $request->ip();

        $param = array();
        $param['secret'] = '6LerZAsUAAAAAMQMxgfQcWx7FkMqDe_TF88G53d7';
        $param['response'] = $g_recaptcha_response;
        $param['remoteip'] = $ip;

        $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$param['secret'].'&response='.$param['response'].'&remoteip='.$param['remoteip'];
        $param = json_encode($param);

        $opts['http']['method'] = 'POST';
        $opts['http']['timeout'] = 180;
        $opts['http']['content'] = $param;
        $context  = stream_context_create($opts);
        $response = @file_get_contents($url, false, $context);

        if ($response) {
            $response = json_decode($response);
            if($response->success==false){
                $result->errorMsg = 'Failed Validate Captcha';
                return json_encode($result);
            }
        }

        $API = new API();
        $postSubmit = $API->contactUs($name,$email,$phone,$subject,$message,4);

        if ($postSubmit->response->code!=200){
            //kalau gagal
            $result->errorMsg = 'Failed to Submit Form';
            return json_encode($result);
        }

        $result->isSuccess = true;
        return json_encode($result);
    }

    public function getTayakaLanding(){
        $set = "Indonesia";
        $lockerList = array();
        $lockerJkt = array();
        $citiesList = array('Bandung');

        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        foreach ($lockerList as $index=>$locker){
            if (!in_array($locker->district,$citiesList)) continue;
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] =  $locker->district;
            $lockerJkt[$index]['province'] =  $locker->province;
            $lockerJkt[$index]['latitude'] =  $locker->latitude;
            $lockerJkt[$index]['longitude'] =  $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
        }

        $data['lockerJkt'] = $lockerJkt;
        $data['citiesList'] = $citiesList;
        return view('merchant.tayaka',$data);
    }

    public function postTayakaOrder(Request $request){
        $result = new \stdClass;
        $result->isSuccess = false;

        $cust_name = $request->input('cust_name');
        $cust_email = $request->input('cust_email');
        $cust_phone = $request->input('cust_phone');
        $service = $request->input('service');
        $locker = $request->input('locker');
        $address = $request->input('address');

        if (empty($cust_name) || empty($cust_email) || empty($cust_phone) ||  empty($service) || empty($locker) ){
            $result->errorMsg = 'Missing Parameter';
            return response()->json($result);
        }

        if ($service=='drop'){
            if (empty($address)){
                $result->errorMsg = 'Address Required';
                return response()->json($result);
            }
        }

        // post data to API
        $api = new API();
        $response = $api->tayakaOrder($cust_name,$cust_email,$cust_phone,$service,$locker,$address);
        if (empty($response)){
            $result->errorMsg = 'Failed to Submit Order';
            return response()->json($result);
        }
        if ($response->response->code!=200){
            $result->errorMsg = $response->response->message;
            return response()->json($result);
        }

        $result->isSuccess = true;
        return response()->json($result);
    }

    public function getVCleanLanding(){
        $set = 'Indonesia';

        $lockerList = array();
        $lockerJkt = array();
        $citiesList = array();

        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        $provinceAllowed = ['DKI Jakarta','Jawa Barat','Banten'];

        foreach ($lockerList as $index=>$locker){
            if (!in_array($locker->province,$provinceAllowed)) continue;
            if ($locker->district=='Bandung') continue;
            $lockerJkt[$index]['name'] = $locker->name;
            $lockerJkt[$index]['address'] = $locker->address;
            $lockerJkt[$index]['address_2'] = $locker->address_2;
            $lockerJkt[$index]['city'] =  $locker->district;
            $lockerJkt[$index]['province'] =  $locker->province;
            $lockerJkt[$index]['latitude'] =  $locker->latitude;
            $lockerJkt[$index]['longitude'] =  $locker->longitude;
            $lockerJkt[$index]['operational_hours'] = $locker->operational_hours;
        }

        foreach ($lockerJkt as $item) {
            if(!in_array($item['city'],$citiesList)) $citiesList[] = $item['city'];
        }

        $data['lockerJkt'] = $lockerJkt;
        $data['citiesList'] = $citiesList;
        return view('merchant.vclean',$data);
    }

        public function postVCleanOrder(Request $request){
        $result = new \stdClass;
        $result->isSuccess = false;

        $cust_name = $request->input('cust_name');
        $cust_email = $request->input('cust_email');
        $cust_phone = $request->input('cust_phone');
        $shoes_total = $request->input('shoes_total');
        $service = $request->input('services');
        $locker = $request->input('locker');

        if (empty($cust_name) || empty($cust_email) || empty($cust_phone) || empty($shoes_total) || empty($service) || empty($locker)){
            $result->errorMsg = 'Missing Parameter';
            return response()->json($result);
        }

        // post data to API
        $api = new API();
        $response = $api->vcleanOrder($cust_name,$cust_email,$cust_phone,$shoes_total,null,$service,$locker);
        if (empty($response)){
            $result->errorMsg = 'Failed to Submit Order';
            return response()->json($result);
        }
        if ($response->response->code!=200){
            $result->errorMsg = $response->response->message;
            return response()->json($result);
        }

        $result->isSuccess = true;
        return response()->json($result);
    }

    /**
    * ===== Merchant Registration =====
     */
    /**
     * Get
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function landingRegistration(Request $request){        
        return view('merchant.landing-registration');
    }

    public function howto(){
        return view('merchant.howto');
    }

    /**
     * get form registration
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formRegistration(Request $request){
        // $cityDb = City::all();
        
        
        $cityDb = City::whereIn("city_name",['bandung','bogor','bekasi','depok','tangerang'])
                        ->orWhere(function ($query) {
                              $query->where('city_name', 'like', '%jakarta%');
                        })->get();        

        $province = array();
        $citiesList = array();

        foreach ($cityDb as $Map)
        {
            if (!isset($province[$Map->province->province_name]))
            {
                $i = count($province);
                $province[$Map->province->province_name] = $i;
            }
            $i = $province[$Map->province->province_name];

            $citiesList[$i]['Province'] = utf8_encode($Map->province->province_name);
            $citiesList[$i]['Cities'][] = $Map->city_name;
        }

        // parsing data
        $data['citiesList'] = $citiesList;
        return view('merchant.form-registration1',$data);
    }

    public function successRegister(Request $request){
        return view('merchant.success-registration');
    }

    public function postRegister(Request $request){
        // create validation   
          
        $rules = [
            'owner_name' => 'required',            
            'email' => 'required|email|unique:tb_users',
            'phone' => 'required|numeric',
            'merchant_name' => 'required|unique:tb_merchant_users',
            'address' => 'required',
            'website' => 'required',
            'picture' => 'image|max:500',
            'city' => 'exists:tb_cities,city_name',
            'detail_address' => '',           
            'syarat' => 'required',
            'days' => 'required',
            'time' => 'required'
        ];

        $messages = [
            'unique'    => "The :attribute has taken kehed",
            'required'    => "The :attribute can't' be blank",               
            'numeric' =>  "The :attribute must numeric only",   
        ];
        
        $this->validate($request,$messages);
        // create parameter
        $params = [];
        $params['merchant_name'] = $request->input('merchant_name');
        $params['owner_name'] = $request->input('owner_name');
        $params['email'] = $request->input('email');
        $params['phone'] = $request->input('phone');
        $params['address'] = $request->input('address');
        $params['detail_address'] = $request->input('detail_address',null);
        $params['city'] = $request->input('city');
        $params['website'] = $request->input('website',null);
        $params['service'] = Array("lastmile");//$request->input('service');
        $timepickup = "";
        if ( $request->input("days") !=null ){
            $timepickup = implode(", ", $request->input("days"));
        }
        if ( $request->input("time") ){
            if (isset($timepickup)){
                $timepickup = $timepickup." : ".$request->input("time");
            }else{
                $timepickup = $request->input("time");
            }
        }
        $params['timepickup'] = $timepickup;//$request->input('service');\

        $picture = $_FILES['picture']['tmp_name'];//$request->file('picture',null);
        if (!empty($picture)){      
            $picture = file_get_contents($picture);
        }
        $params['picture'] = empty($picture) ? null : base64_encode($picture);
        
        $f = fopen(storage_path().'/logs/api/log'.date('Y-m-d').".txt",'a');
        fwrite($f,json_encode($params));
        fclose($f);

        $api = new API();

        $result = $api->merchantRegister($params);
        if (empty($result)){
            $request->session()->flash('error','Failed to Register');
            return redirect()->back()->withInput();
        }
        if ($result->response->code!=200){            
            $removed = array("{", "}", "[","]", '"',".");
            $strmessage = str_replace($removed, "", $result->response->message);
            $strmessage = str_replace(",", ",", $strmessage);
            $request->session()->flash('error',$strmessage);
            return redirect()->back()->withInput();
        }
        return redirect('/merchant/successregister')->with('status', 'Profile updated!');        
    }

    public function activationByEmail($code,$email, Request $request){
        $data = [];
        $data['errorMsg'] = null;
        if (empty($code)){
            $data['errorMsg'] = 'Empty Code';
            return view('merchant.invalid-code',$data);
        }

        $code = explode('-',$code);
        if (empty($code[1])){
            $data['errorMsg'] = 'Empty Codes';
            return view('merchant.invalid-code',$data);
        }
        $activationCode = $code[1];

        $api = new API();
        $result = $api->emailActivation($email,$activationCode);
        if (empty($result)){
            $data['errorMsg'] = 'Failed to Activate';
            return view('merchant.invalid-code',$data);
        }
        if ($result->response->code!=200){
            $data['errorMsg'] = $result->response->message;
            return view('merchant.invalid-code',$data);
        }

        $merchant = \DB::table("tb_merchant_users")->where("email", $email)->first();
        if (isset($merchant)){
            $email = $merchant->email;
            $to = config("config.merchant_email");            
            $merchantName =  $merchant->merchant_name;    
            $url = config("config.dashboardurl");
            \Mail::send('user.emails.merchant-activate', ["data"=>$merchant, 'url'=> $url], function ($m) use ($email, $to, $merchantName) {
                    $m->from('no-reply@popbox.asia', 'PopBox Asia');
                    $m->to($to, $merchantName)->subject("[PopBox] Akun Telah Diaktivasi: $merchantName");
                    $m->bcc(config("config.bcc"));
                });             
        }
        return view('merchant.success-activation');
       
    }
    
    // PopBox Neubodi CSR - Sept 2018
    public function getCSRLanding(){
        $set = 'Malaysia';

        $lockerList = array();
        $lockerMY = array();
        $citiesList = array();

        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        foreach ($lockerList as $index=>$locker){
            $lockerMY[$index]['name'] = $locker->name;
            $lockerMY[$index]['address'] = $locker->address;
            $lockerMY[$index]['address_2'] = $locker->address_2;
            $lockerMY[$index]['city'] =  $locker->district;
            $lockerMY[$index]['province'] =  $locker->province;
            $lockerMY[$index]['latitude'] =  $locker->latitude;
            $lockerMY[$index]['longitude'] =  $locker->longitude;
            $lockerMY[$index]['operational_hours'] = $locker->operational_hours;
            $lockerMY[$index]['locker_id'] = $locker->locker_id;
        }

        foreach ($lockerMY as $item) {
            if(!in_array($item['city'],$citiesList)) $citiesList[] = $item['city'];
        }

        $data['lockerMY'] = $lockerMY;
        $data['citiesList'] = $citiesList;
        
        return view('merchant.csr',$data);
    }

    public function postCSROrder(Request $request){
        $result = new \stdClass;
        $result->isSuccess = false;

        $locker_size = "M";
        $locker_id = $request->input('locker_id');
        $locker = $request->input('locker');

        $notes = "-";
        $item_photo = "-";
        $promo_code = "CSRNEUBODI"; //todo : make sure this active promocode

        $sender_name = $request->input('cust_name');
        $sender_email = $request->input('cust_email');
        $sender_phone = $request->input('cust_phone');
        $sender_used_popbox = $request->input('used_popbox');
        $neubodi_answer = $request->input('heard_neubodi');
        $campaign_answer = $request->input('campaign');

        $form_recipient = "Full Name : " .$sender_name. " 
            <br>Participant Email : " .$sender_email. " 
            <br>Mobile Number : " .$sender_phone. " 
            <br>Have You Used a PopBox Locker Before : " .$sender_used_popbox. " 
            <br>Have You Heard Of Neubodi : " .$neubodi_answer. " 
            <br>How Do You Know About the campaign : " .$campaign_answer. " 
            <br>Drop Locker : " .$locker;

        if (empty($sender_name) || empty($sender_email) || empty($sender_phone) || empty($locker_id)){
            $result->errorMsg = 'Please complete all data before submit';
            return response()->json($result);
        }

        // Post Data CSR to API
        $api = new API();
        $response = $api->csrOrderAPI($locker_size, $locker_id, 
        $notes, $item_photo, $promo_code, $sender_name, 
        $sender_email, $sender_phone, $form_recipient);
        
        if (empty($response)){
            $result->errorMsg = 'Failed submit, please try again.';
            return response()->json($result);
        }

        if ($response->response->code!=200){
            $result->errorMsg = $response->response->message;
            return response()->json($result);
        }
        $result->isSuccess = true;
        return response()->json($result);
    }
    
    public function neuboditc(){
        return view('merchant.neuboditc');
    }
    
    public function neubodithankyou(){
        return view('merchant.neubodithankyou');
    }
    
}