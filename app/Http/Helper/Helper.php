<?php
/**
 * Created by PhpStorm.
 * User: Arief Demiawansyah
 * Date: 10/11/2016
 * Time: 17.51
 */

namespace App\Http\Helper;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use App\Http\Helper\API;

class Helper
{
    /**
     * create Image from physical address
     * @param $filename
     * @return string
     */
    public static function createImg($folder,$filename){
        $server =  $_SERVER['DOCUMENT_ROOT'];
        $tmpPath = $server.'/../../dashboard.popbox.asia/public/'.$folder.'/'.$filename;
        $path = realpath($tmpPath);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = @file_get_contents($path);
        if (empty($data)) return false;
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    public static function createImgLocal($folder,$filename){
         if (!file_exists(public_path().'/img/'.$folder.'/'.$filename)){
            $server =  $_SERVER['DOCUMENT_ROOT'];
            $tmpPath = $server.'/../../dashboard.popbox.asia/public/'.$folder.'/'.$filename;
            $path = realpath($tmpPath);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = @file_get_contents($path);
            if (empty($data)) return false;
            file_put_contents(public_path().'/img/'.$folder.'/'.$filename,$data);
        }       
        $url = asset('img/'.$folder.'/'.$filename);
        return $url;
    }

    /**
     * create Image from URL
     * @param $folder
     * @param $filename
     * @return string
     */
    public static function createImgUrl($folder,$filename){
        $server_dev =  'http://dashbordev.popbox.asia';
        $server_live = 'http://dashboard.popbox.asia';

        $environment = App::environment();
        if ($environment=='production'){
            $server = $server_live;
        } else $server = $server_dev;
       
        $path = "$server/$folder/$filename";
        return $path;
    }

    /**
     * get country based on IP
     * @param null $ip
     * @return mixed
     */
    public static function ipGeoPlugin($ip=null){
        if (!empty($ip)){
            $result = @file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip");
            if (!empty($ip)){
                $result = unserialize($result);
                if (isset($result['geoplugin_countryCode'])){
                    if (!empty($result['geoplugin_countryCode'])) return $result['geoplugin_countryCode'];
                }
            }
        }
    }

    /**
     * get cities from dropdown on tracking
     * @return array
     */
    public static function getCities(){
        $region = Session::get('app.region','id');
        $country = ['id'=>'Indonesia','my'=>'Malaysia'];
        $lockerList = array();
        if (!isset($country[$region])) {
            $set = 'Indonesia';
        } else $set = $country[$region];
        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;

        $citiesList = array();
        foreach ($lockerList as $item) {
            if (!in_array($item->district,$citiesList)){
                $citiesList[] = $item->district;
            }
        }
        return $citiesList;
    }

     public static function getMyCities(){
        $set = "Malaysia";
        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;
        $citiesList = array();
        foreach ($lockerList as $item) {
            if (!in_array($item->district,$citiesList)){
                $citiesList[] = $item->district;
            }
        }
        return $citiesList;
    }
	
	  public static function getIDCities(){
        $set = "Indonesia";
        $locker = Locker::getAllLocker($set);
        if ($locker) $lockerList = $locker;
        $citiesList = array();
        foreach ($lockerList as $item) {
            if (!in_array($item->district,$citiesList)){
                $citiesList[] = $item->district;
            }
        }
        return $citiesList;
    }


    public static function getLockerNearest($long, $lat){
        $api = new API();
        $params = array("latitude" => $lat, "longitude" => $long);
        $lockernearest = $api->nearest($params);
        return $lockernearest;        
    }

    public static function getLockerbyCities($country = ""){        
        $api = new API();
        $lockerBycities = array();
        $lockers = array();
        if ($country=="id"){
            $cities = self::getIDCities();
        }else{
            $cities = self::getMyCities();
        }
        foreach ($cities as $key => $value) {            
            $params = array("city"=> $value);
            $lockerCities = $api->lockerLocation($params);
            $json = json_encode($lockerCities);            
            $lockerCitiesJson = str_replace("\\r\\n", "", $json);
            $lockerCities = json_decode($lockerCitiesJson );
            $lockerBycities[$value] = json_encode($lockerCities->data);            
            foreach ($lockerCities->data as $key1 => $value1) {                
               $lockers[] =  $value1;
            }
        }        
        return array("lockerBycities"=> $lockerBycities, "lockers"=>$lockers);
    }

    public static function truncateText($string,$limit=100, $break='.',$pad='...'){
        // return with no change if string is shorter than $limit
        if(strlen($string) <= $limit) return $string;

        // is $break present between $limit and the end of the string?
        if(false !== ($breakpoint = strpos($string, $break, $limit))) {
            if($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint) . $pad;
            }
        }

        return $string;
    }

    public static function getInstagramToken(){
        $url = "https://api.instagram.com/oauth/authorize/?client_id=070ccbff2ff24f6a87d826b2fa558072&redirect_uri=https://dev.popbox/&response_type=token";
        $result = file_get_contents($url);
        return $result;
    }
}