<?php
/**
 * Created by PhpStorm.
 * User: Arief Demiawansyah
 * Date: 07/11/2016
 * Time: 13.59
 */

namespace App\Http\Helper;


use Illuminate\Support\Facades\App;

class API
{

    
    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = array()){
        static $id;
        if (empty($id)) $id = uniqid();
        $unique = $id;
        if (\Session::has('auth.phone')) {
            $phone = \Session::get('auth.phone');
            $unique = $id.' > '.$phone;
        }

        
        $url =  config('config.api').'/'.$request;
        $param['token'] = config('config.api_token');
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $f = fopen(storage_path().'/logs/api/'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type:application/json'));
        $output = curl_exec($ch);
        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path().'/logs/api/'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        return $output;
    }

    /**
     * Fungsi untuk ambil semua locker list dan location
     * @param array $param
     * @return mixed
     */
    public function lockerLocation($param = array())
    {
        $request = 'locker/location';
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

     /**
     * Fungsi untuk ambil nearest 5km
     * @param array $param
     * @return mixed
     */
    public function nearest($param = array())
    {
        $request = 'nearest/list5';
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Fungsi untuk posting contact us
     * @param $name
     * @param $email
     * @param $phone
     * @param $subject
     * @param $message
     * @param $productId
     * @return mixed
     */
    public function contactUs($name,$email,$phone,$subject,$message,$productId = 3)
    {
        $request =  '/contact/submit';
        $param['product'] = $productId;
        $param['name'] = $name;
        $param['email'] = $email;
        $param['message'] = $message;
        $param['phone'] = $phone;
        $param['subject'] = $subject;
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * fungsi untuk track parcel
     * @param $orderNumber
     * @return mixed
     */
    public function parcelStatus($orderNumber){
        $request = 'parcel/status';
        $param = array();
        $param['order_number'] = $orderNumber;
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * get locker availability
     * @param $lockerName
     * @return mixed
     */
    public function lockerAvailability($lockerName){
        $request = 'locker/availability';
        $param = array();
        $param['locker_name'] = $lockerName;
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }


    /**
     * get locker availability by lockerID
     * @param $locker_id
     * @return mixed
     */
    public function lockerAvailabilityByID($locker_id){
        $request = 'locker/availability';
        $param = array();
        $param['locker_id'] = $locker_id;
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }


    /**
     * Omasiu Order
     * @param $name
     * @param $email
     * @param $phone
     * @param $shoesTotal
     * @param $shoesBrand
     * @param $services
     * @param $locker
     * @return mixed
     */
    public function omaisuOrder($name, $email, $phone, $shoesTotal, $shoesBrand, $services, $locker){
        $request = 'partner/submit';
        $param = array();
        $param['prefix'] = 'OMS';
        $param['cust_name'] = $name;
        $param['cust_email'] = $email;
        $param['cust_phone'] = $phone;
        $param['shoes_total'] = $shoesTotal;
        $param['shoes_brand'] = $shoesBrand;
        $param['service'] = $services;
        $param['locker_name'] = $locker;
        
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    public function vcleanOrder($name, $email, $phone, $shoesTotal, $shoesBrand, $services, $locker){
        $request = 'partner/submit';
        $param = array();
        $param['prefix'] = 'VCS';
        $param['cust_name'] = $name;
        $param['cust_email'] = $email;
        $param['cust_phone'] = $phone;
        $param['shoes_total'] = $shoesTotal;
        $param['shoes_brand'] = $shoesBrand;
        $param['service'] = $services;
        $param['locker_name'] = $locker;

        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    public function tayakaOrder($name, $email, $phone,$services, $locker,$address=null){
        $request = 'partner/submit';
        $param = array();
        $param['prefix'] = 'TYK';
        $param['cust_name'] = $name;
        $param['cust_email'] = $email;
        $param['cust_phone'] = $phone;
        $param['service'] = $services;
        $param['locker_name'] = $locker;
        $param['cust_address'] = $address;

        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * post merchant registration
     * @param $params
     * @return mixed
     */
    public function merchantRegister($params){
        $request = 'merchant/registration';
        $result = $this->cUrl($request,$params);
        $result = json_decode($result);
        return $result;
    }

    /**
     * post email merchant activation
     * @param $email
     * @param $code
     * @return mixed
     */
    public function emailActivation($email,$code){
        $request = 'merchant/activation';
        $param = [];
        $param['email'] = $email;
        $param['activation_code'] = $code;
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    public function pickupOrder($param){
        $request = 'merchant/pickup';        
        $result = $this->cUrl($request,$param);
        $result = json_decode($result);
        return $result;
    }

    private function popsendV2curl($request, $param = array()){
        static $id;
        if (empty($id)) $id = uniqid();

        $unique = $id;
        if (\Session::has('auth.phone')) {
            $phone = \Session::get('auth.phone');
            $unique = $id.' > '.$phone;
        }

        $url =  config('config.api_popsendv2').'/'.$request;
        $param['token'] = config('config.api_popsendv2_token');
        $param['session_id'] = config('config.mypopbox_sessionid');
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";

        // $f = fopen(storage_path().'/logs/api/'.$date.'.log','a');
        // fwrite($f,$msg);
        // fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type:application/json'));
        $output = curl_exec($ch);
        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        
        // $f = fopen(storage_path().'/logs/api/'.$date.'.log','a');
        // fwrite($f,$msg);
        // fclose($f);

        return $output;
    }

    public function csrOrderAPI($locker_size, $locker_id, $notes, 
        $item_photo, $promo_code, $sender_name, $sender_email, 
        $sender_phone, $form_participant){

        $request = 'popsafe/csr-order';
        $param = array();

        $param['locker_size'] = $locker_size;
        $param['locker_id'] = $locker_id;
        $param['notes'] = $notes;
        $param['item_photo'] = $item_photo;
        $param['promo_code'] = $promo_code;
        $param['sender_name'] = $sender_name;
        $param['sender_email'] = $sender_email;
        $param['sender_phone'] = $sender_phone;
        $param['form_participant'] = $form_participant;

        $result = $this->popsendV2curl($request, $param);
        $result = json_decode($result);

        return $result;
    }
}