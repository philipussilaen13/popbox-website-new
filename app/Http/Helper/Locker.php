<?php
/**
 * Created by PhpStorm.
 * User: Arief Demiawansyah
 * Date: 07/11/2016
 * Time: 14.21
 */

namespace App\Http\Helper;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Locker
{
    /**
     * ==============DEPRECATED=====================
     * get locker for Indonesia only
     * @return mixed
     */
    public static function getLockerMap($region='Indonesia')
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $name = "locker-".$region;
        $lockerAPI = Cache::remember($name,120, function() use($region) {
            $response = new \stdClass();
            $response->isSuccess = false;

            $param = array();
            $param['country'] = $region;

            $API = new API();
            $result = $API->lockerLocation($param);
            if ($result->response->code!=200){
                return array();
            }
            return $result->data;
        });

        if (empty($lockerAPI)){
            return $response;
        }
        $response->isSuccess = true;
        $response->data = $lockerAPI;

        return $response;
    }

    /**
     * get all locker based on region
     * @param string $region
     * @return mixed
     */
    public static function getAllLocker($region ='')
    {
        $allLocker = Cache::remember("allLocker-$region",120,function() use($region){
            $data = DB::table('locker_locations')
                ->leftJoin('districts','locker_locations.district_id','=','districts.id')
                ->leftJoin('provinces','districts.province_id','=','provinces.id')
                ->leftJoin('countries','provinces.country_id','=','countries.id')
                ->where('countries.country','=',$region)
                ->where('locker_locations.isPublished','=',1)
                ->select('locker_locations.*','districts.district as district','provinces.province as province','countries.country as country')
                ->get();
            return $data;
        });
        return $allLocker;
    }

    /**
     * ============== Deprecated ==================
     * get gallery based on region
     * @param $region
     * @return array
     */
    public static function getLockerGallery($region){
        $id = array();
        $idImg = array(
            'locker-stsudirman.jpg','locker-gias.jpg','locker-indofoodtower.jpg','locker-lippomallkemang.jpg',
            'locker-moi.jpg','locker-pasfes.jpg','locker-presuniv.jpg','locker-sinarmasland.jpg',
            'locker-grandslipi.jpg'
        );
        $idAddress = array(
            'Locker St. Sudirman','GIAS Locker','Indofood Tower Locker','Lippo Mall Kemang','MOI Locker','PasFes Locker',
            'Pres Univ Locker','Sinarmas Land Locker','Grand Slipi Tower'
        );
        for($i=0;$i<count($idImg);$i++){
            $id[$i]['image'] = $idImg[$i];
            if (array_key_exists($i,$idAddress)) $id[$i]['desc'] = $idAddress[$i];
        }

        $my = array();
        $myImg = array(
            'La_Costa.jpg','Menara_Pinnacle.jpeg','Monash_University.jpeg','Nautica.jpeg',
            'Sunway_Giza.jpeg','Sunway_Pyramid-Lagoon_Entrance.jpeg','Sunway_University.jpeg','Wisma_Sunway.jpeg');
        $myAddress = array(
            'La Costa. Sunway South Quay, Bandar Sunway, 47500 Subang Jaya, Selangor, Malaysia',
            'Menara Pinnacle. Persiaran Lagoon, Bandar Sunway, 47500 Petaling Jaya, Selangor, Malaysia',
            'Monash University. Jalan Lagoon Selatan, Bandar Sunway, 47500 Subang Jaya, Selangor, Malaysia',
            'Nautica. Sunway South Quay, Bandar Sunway, 47500 Subang Jaya, Selangor, Malaysia',
            'Sunway Giza. No. 2, Jalan PJU 5/14, Kota Damansara, 47810 Petaling Jaya, Selangor, Malaysia',
            'Sunway Pyramid-Lagoon Entrance. Sunway Pyramid Shopping Mall, 3, Jalan PJS 11/15, Bandar Sunway, 47500 Petaling Jaya, Selangor, Malaysia',
            'Sunway University. 5, Jalan Universiti, Bandar Sunway, 47500 Subang Jaya, Selangor, Malaysia',
            'Wisma Sunway. 1, Jalan Persiaran Kayangan, Seksyen 9, 40100 Shah Alam, Selangor, Malaysia'
        );
        for($i=0;$i<count($myImg);$i++){
            $my[$i]['image'] = $myImg[$i];
            if (array_key_exists($i,$myAddress)) $my[$i]['desc'] = $myAddress[$i];
        }

        if ($region=='my') return $my;
        else return $id;

    }

    /**
     * ===================== Deprecated =================
     * @param $region
     * @return array
     */
    public static function getLockerSlider($region){
        $idImg = array('img01.jpg','img02.jpg','img03.jpg');
        $idDesc = array(
            'PopBox adalah loker elektronik yang dapat kamu gunakan untuk mengirim barang, mengembalikan barang, ataupun mengambil barang belanjaan online kamu.',
            'PopBox adalah loker elektronik yang dapat kamu gunakan untuk mengirim barang, mengembalikan barang, ataupun mengambil barang belanjaan online kamu.',
            'PopBox adalah loker elektronik yang dapat kamu gunakan untuk mengirim barang, mengembalikan barang, ataupun mengambil barang belanjaan online kamu.'
        );
        $id = array();
        for($i=0;$i<count($idImg);$i++){
            $id[$i]['image'] = $idImg[$i];
            if (array_key_exists($i,$idDesc)) $id[$i]['desc'] = $idDesc[$i];
        }

        $myImg = array('Menara_Pinnacle3.jpeg','img03.jpg','slider-sunway.jpg','Lacosta-slider.jpeg');
        $myDesc = array(
            'PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.',
            'PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.',
            'Now at Bandar Sunway',
            'PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.'
        );
        for($i=0;$i<count($myImg);$i++){
            $my[$i]['image'] = $myImg[$i];
            if (array_key_exists($i,$myDesc)) $my[$i]['desc'] = $myDesc[$i];
        }

        if ($region=='my') return $my;
        else return $id;
    }

    /**
     * get gallery from Db from region
     * @param $region
     * @return array
     */
    public static function getLockerGalleryDb($region){
        $imageList = array();
        $imageGalleryDb = Cache::remember('lockerGallery'.$region, 1440, function() use($region) {
            return  DB::table('tb_website_image')->where('type','gallery')->where('region',$region)->where('status','show')->get();
        });
        foreach ($imageGalleryDb as $img) {
            $tmp = array(
                'image' => $img->image,
                'desc' => $img->description
            );
            $imageList[] = $tmp;
        }
        return $imageList;
    }

    /**
     * get locker image from Db for Slider Index
     * @param $region
     * @return array
     */
    public static function getLockerSliderDb($region){
        $imageList = array();
        $imageSliderDb = Cache::remember('lockerSlider'.$region, 1, function() use($region) {
            return  DB::table('tb_website_image')->whereIn('type',['slider','promo'])->where('status','show')->where('region',$region)->get();
        });
        foreach ($imageSliderDb as $img) {
            $tmp = array(
                'image' => $img->image,
                'desc' => $img->description,
                'type' => $img->type
            );
            $imageList[] = $tmp;
        }
        return $imageList;
    }
}