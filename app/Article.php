<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Article extends Model
{
    protected $table = 'tb_website_articles';
    protected $primaryKey = 'id_article';

    public static function getPromoPage($region='id'){
        $nowDate = date('Y-m-d H:i:s');
        $articlesDb = Article::where('status','publish')
            ->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->where('region',$region)
            ->where('type','=','promo')
            ->paginate(8);
        // test
        return $articlesDb;
    }

    /**
     * get active article
     * @param null $number
     * @return mixed
     */
    public static function getActiveArticle($number=null){
        $nowDate = date('Y-m-d H:i:s');
        $region = Session::get('app.region','id');
        $activeDb = self::where('status','publish')
            ->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->where('region',$region);
        if (!empty($number)){
            $activeDb->take($number);
        }
        $activeDb = $activeDb->get();
        return $activeDb;
    }

    /**
     * get active promo
     * @return mixed
     */
    public static function getActivePromo(){
        $nowDate = date('Y-m-d H:i:s');
        $region = Session::get('app.region','id');
        $activeDb = self::where('status','publish')
            ->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->where('type','promo')
            ->where('region',$region)
            ->orderBy('priority','desc')	
            ->take(5)
            ->get();
        return $activeDb;
    }

    public static function getPastPromo($region=null){
        if (empty($region)) $region = Session::get('app.region','id');
        $nowDate = date('Y-m-d H:i:s');
        $pastPromo = self::where('status','publish')
            ->where('end_date','<',$nowDate)
            ->where('type','promo')
            ->where('region',$region)
            ->get();
        return $pastPromo;
    }

    /**
     * get article type news
     * @return mixed
     */
    public static function getNews(){
        $newsDb = Cache::remember('news', 1440, function() {
            return  self::where('type','news')->where('status','publish')->get();
        });
        return $newsDb;
    }

    /**
     * get testimonial for index page
     * @param string $region
     * @return mixed
     */
    public static function getTestimonial($region = 'id'){
        $data = Cache::remember('testi'.$region, 1440, function() use($region) {
            return  self::where('type','testi')->where('status','publish')->where('region',$region)->orderBy('priority','desc')->get();
        });
        return $data;
    }

    /**
     * get blog data for blog index page where active and sorting by priority
     * @param string $region
     * @return mixed
     */
    public static function getActiveBlog($region = 'id'){
        return self::where('type','blog')->where('status','publish')->where('region',$region)->orderBy('priority','desc')->orderBy('created_date','desc')->simplePaginate(10);
    }

    /**
     * get blog data for blog index page section recent blog/news
     * @param string $region
     * @return mixed
     */
    public static function getRecentBlog($region = 'id'){
        return self::where('type','blog')->where('status','publish')->where('region',$region)->take(5)->get();
    }
}
