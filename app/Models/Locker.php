<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Locker extends Model
{
    // set table
    protected $connection = 'popbox';
    protected $table = 'locker_locations';

     public static function getAllLocker(){
        $allLocker = Cache::remember("allLocker",120,function(){});
            $data = DB::table('locker_locations')
                ->leftJoin('districts','locker_locations.district_id','=','districts.id')
                ->leftJoin('provinces','districts.province_id','=','provinces.id')
                ->leftJoin('countries','provinces.country_id','=','countries.id')
                ->where('countries.country','=','Indonesia')
                ->select('locker_locations.*','districts.district as district','provinces.province as province','countries.country as country')
                ->get();
            return $data;
        return $allLocker;
    }

    /*Relationship*/
    public function services(){
        return $this->hasMany(LockerService::class,'locker_id','locker_id');
    }
}
