<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Sximo;

class Merchantpickup extends Sximo  {
	
	protected $table = 'tb_merchant_pickup';
	protected $primaryKey = 'id_pickup';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_merchant_pickup.*, locker_activities.storetime, locker_activities.taketime,
						case
							when tb_merchant_pickup.status like '%home_delivery%' then 'HOME_DELIVERY'
                            when locker_activities.status like '%in_store%' then 'IN_STORE' 
                            when locker_activities.status like  '%customer_taken%'  then 'CUSTOMER_TAKEN'
                            when locker_activities.status like '%courier_taken%' then 'COURIER_TAKEN'
                            when locker_activities.status is null then 'NEED_STORE'
                    	end as status
				  FROM tb_merchant_pickup 
				  LEFT join locker_activities on tb_merchant_pickup.order_number=locker_activities.barcode  
				  ";
	}	

	public static function queryWhere(  ){
		$merchant_id = \Auth::user()->group_id;		
		// $merchant_id = session()->get('gid');				
		$merchant_query="";

		$sqlmerch = "Select name, description from tb_groups where group_id='".$merchant_id."'";
		$rsmerch = DB::select($sqlmerch);
		$merchant_name = trim($rsmerch[0]->name);
		$posno = trim($rsmerch[0]->description);

		$ckpref = strpos($posno, 'Prefix:');
		$cekuser = strpos($posno, 'Username:');
		$cekmname = strpos($posno, 'Merchantname:');
		
		do {
			if ($ckpref != 0) {
				$pref = substr($posno, strpos($posno, "Prefix:") + 7);   
				$merchant_query = " tb_merchant_pickup.order_number like '".$pref."%' and ";
				break;
			} 

			if ($cekuser != 0) {
				$username = substr($posno, strpos($posno, "Username:") + 9);   
				$merchant_query = " locker_activities.name like '".$username."%' and ";
				break;
			} 

			if ($cekmname != 0) {
				$merchantname = substr($posno, strpos($posno, "Merchantname:") + 13);
				$merchant_query = "tb_merchant_pickup.merchant_name like '".$merchantname."%' and  ";
				break;  
			}
 		} while (false);

		switch ($merchant_id) {
        				case "6":
            				$merchant_query = "merchant_name='Matahari Mall' and ";
            				break;
            			case "7":
            				$merchant_query = "merchant_name='Lazada Indonesia' and ";
            				break;	
            			case "8":
            				$merchant_query = "merchant_name='Asmaraku' and ";
            				break;
            			case "9":
            				$merchant_query = "merchant_name='Zalora Indonesia' and ";
            				break;	
            			case "10":
            				$merchant_query = "merchant_name='Frozen Shop' and ";
            				break;
            			/*case "32":
            				$merchant_query = "merchant_name='Oriflame' and ";
            				break;*/			
		  				default:  
              				$merchant_name = "PopBox";
            			}

		return "  WHERE ".$merchant_query." tb_merchant_pickup.id_pickup IS NOT NULL ";
	
	}
	
	public static function queryGroup(){
		return " ";
	}
	

}
