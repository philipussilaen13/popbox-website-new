<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    // set table
    protected $table = 'tb_cities';

    /*Relationship*/
    public function province(){
        return $this->belongsTo(Province::class,'provinces_id','id');
    }
}
