<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Promo Page Lines
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'Hubungi Kami',
    'sub' => 'Tim pelayanan konsumen PopBox akan senang membantu kamu.',
    'call'=>'Hubungi PopBox',
    'input-name'=>'Nama',
    'input-email'=>'Email',
    'input-hp'=>'No. HP',
    'input-subjek'=>'Subjek',
    'input-message'=>'Pesan',
    'input-btn'=>'Kirim Pesan',
    'modal-title'=>'Terima Kasih',
    'modal-content'=>'Terima Kasih atas pesan anda. Kami akan menghubungi anda segera.',
    // 'contact_address' => 'PT. PopBox Asia Services <br> Jl. Palmerah Utara III No.62 FGH <br>(samping SPBU Palmerah Utara)<br> Jakarta Barat 11480',
    'contact_address' => 'Jl. Palmerah Utara No.90 <br> RT.1/RW.7 Palmerah <br> Jakarta Barat, Jakarta 11480',
    'contact-phone' => '+62 21 22538719',
    'contact-phone2' => '',
    // 'contact-location' => 'PT. PopBox Asia Services <br> Jl. Palmerah Utara III No.62 FGH <br>(samping SPBU Palmerah Utara)<br> Jakarta Barat 11480'
    'contact-location' => 'Jl. Palmerah Utara No.90 <br> RT.1/RW.7 Palmerah <br> Jakarta Barat, Jakarta 11480'
];
