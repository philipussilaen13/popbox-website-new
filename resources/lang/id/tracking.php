<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Promo Page Lines
    |--------------------------------------------------------------------------
    |
    */
    'title' => 'Tracking',
    'sub' => 'Lacak barang kamu',
    'search-awb' => 'Silahkan masukkan nomor AWB kemudian tekan tombol cari',
    'parcel' => 'Nomor Parcel',
    'parcel-btn'=>'Cari'
];
