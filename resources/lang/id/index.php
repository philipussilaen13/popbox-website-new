<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Index Page Lines
    |--------------------------------------------------------------------------
    |
    */
    /* slider-btn
        <a href="https://popsend.popbox.asia/" target="_blank" class="button button-3d button-black button-large button-rounded tright nomargin" style="color: #FFF;"><span>PopSend</span> <i class="icon-angle-right"></i></a>
        <a href="'.config('config.url').'/merchant/register"><div class="button button-3d button-black button-large button-rounded tright nomargin" style="color: #FFF;"><span>Register</span> <i class="icon-angle-right"></i></div></a>
    */

    'tag' => '#PopBoxInAja',
    'tag2'=> '"MUDAH. AMAN. CEPAT. NYAMAN."',
    'tag-btn'=> 'Layanan Kami',
    'whats'=> 'Apa itu PopBox?',
    'whats2'=> 'PopBox adalah loker elektronik yang dapat kamu gunakan untuk mengirim barang, mengembalikan barang, ataupun mengambil barang belanjaan online kamu.',
    'slider-btn'=> '', 
    'services-title'=>'Layanan PopBox',
    'services-sub'=>'Dengan parcel locker kami siap membantu!',
    'services-sub2'=>'LAYANAN LAIN',

    'services-1'=> 'Mengirim / Mengambil Barang',
    // 'services-1-sub'=> 'Gunakan PopBox - parcel locker sebagai tempat pengambilan paket. Jarang di rumah? Malas menunggu kurir lama? Pengiriman jadi lebih cepat, aman, dan mudah. Cari lokasi PopBox terdekat kamu sekarang.',
    'services-1-sub'=> 'Solusi bagi Anda yang ingin menambah titik pengambilan atau kiriman barang. Cocok bagi mitra ecommerce, laundry, kurir, dan lainnya. Ambil barang dengan kode PIN via SMS yang dikirimkan secara otomatis ketika barang tiba di locker',
    'services-2'=>'Pengembalian Barang',
    // 'services-2-sub'=>'PopBox bekerjasama dengan eCommerce untuk pengembalian barang. Konsumen dapat mengembalikan barang dengan cara lebih mudah. Seluruh proses tidak memakan waktu lebih dari 5 detik.',
    'services-2-sub'=>'Berikan pelanggan anda kemudahan dengan menambah titik pengembalian barang via PopBox Locker. Proses cepat, mudah dan dapat di monitor secara real time untuk status pengembalian barang.',
    // 'services-3'=>'COD',
    // 'services-3-sub'=>'Tidak punya kartu kredit? Popbox menawarkan layanan cod (cash on delivery), kamu dapat membayar di kasir pada saat mengambil barang di locker.',
    'services-3'=>'PopSend',
    'services-3-sub'=>'Punya toko online, atau butuh pengiriman barang? Tidak perlu antri atau cari kantor pos lagi, langsung download aplikasi PopBox, pilih loker terdekat dan tim PopBox akan kirim barang kamu ke alamat atau locker tujuan.',
    'services-4'=>'PopSafe',
    'services-4-sub'=>'Titip barang dengan mudah di locker PopBox dengan harga terjangkau. Order dengan aplikasi PopBox, drop barang di locker yang dipilih dan ambil kembali barang dengan menggunakan Kode PIN.',

    'services-5'=>'Media Promosi',
    // 'services-5-sub'=>'PopBox parcel locker dapat digunakan sebagai tempat <strong> advertising spot, product selling and sampling, promosi new product launching,</strong> dan distribusi logistik via PopBox loker.',
    'services-5-sub'=>'Jangkau jutaan orang dengan beriklan di PopBox Locker. Lokasi yang strategis, harga terjangkau dan ratusan titik untuk di branding oleh brand anda.',
    // 'services-6'=>'PopSend',
    // 'services-6-sub'=>'Cara baru mengirim barang via parcel locker. Pilih locker terdekatmu untuk mengirimkan barang dan PopBox akan mengantar paketmu.',
    'services-6'=>'Solusi Pembayaran',
    'services-6-sub'=>'Lakukan pembelian pulsa, pembayaran digital via PopBox Locker. Tersedia berbagai metode pembayaran dari E-money, Yap!, Gopay, Ovo, Ottopay, dan lainnya',
    // 'services-6'=>'PopShop',
    // 'services-6-sub'=>'Pengalaman belanja baru dengan QR Code, beli ratusan produk menarik dan ambil di loker PopBox, dapatkan diskon special.',
    'services-7'=>'On Demand',
    // 'services-7-sub'=>'Melakukan laundri untuk baju dan sepatu kamu menjadi sangat mudah dengan adanya PopBox on Demand',
    'services-7-sub'=>'Bekerjasama dengan mitra-mitra yang membutuhkan titik untuk drop dan pengambilan barang. Cocok untuk mitra laundry baju, sepatu, tas, dan lainnya. Solusi pas untuk ekspansi dengan cepat dan hemat.',

    'location-title'=>'Lokasi PopBox',
    'location-sub'=>'Temukan lokasi PopBox di dekatmu.',
    'location-btn'=>'Temukan loker terdekat',
    'location-search' => 'Cari Loker Terdekat',
    'location-map-btn'=>'Semua Loker',

    'locker-title'=> 'Loker PopBox',
    'locker-sub'=>'Parcel Locker PopBox berada di lokasi strategis seperti mall, perkantoran, apartemen, universitas, stasiun kereta api, mini market, dll.',
    'partner-title'=>'Mitra kami',
    'partner-sub'=>'Bersama toko online favorite, PopBox mencoba memberikan layanan yang terbaik',
    'partner-btn'=> 'Lihat Seluruh Mitra',
    'count-city'=>'KOTA BESAR',
    'count-locker'=>'TITIK LOKER',
    'count-compartment'=>'KOMPARTEMEN',
    'news-title'=>'Telah Diliput Oleh',
    
    'testi-title'=>'Testimoni',
    'testi-btn' => 'Lihat Semua Testimoni',
    'testi-sub'=>'Apa kata mereka',
    'testi-1'=>'Senang sekali bisa bekerjasama dengan PopBox. Dengan adanya e-Locker yang di support oleh PopBox, sangat memudahkan bagi para pelanggan Kami untuk mendapatkan pesanan, terutama di daerah terpencil. Sangat simple dan praktis. Selain itu, Kami sangat puas dengan service yang disediakan oleh PopBox, dan juga inovasi-inovasi yang di hadirkan',
    'testi-2'=>'PopBox memberikan privasi tambahan dan kenyamanan bagi pelanggan kami, yang membedakan AsmaraKu.com dari ecommerce lainnya. Customer kami sangat suka dimana mereka bisa mengambil paketnya sendiri di berbagai lokasi strategis dalam kota',
    'testi-3'=>'Cocok untuk yang sering belanja online tapi jarang di rumah.',
    'testi-4'=>'Sangat gampang, mudah dan praktis',


    'maps-location'=>'Jakarta',
    'maps-zoom'=>'11'
];
