<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Career Page Lines
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'Gabung Bersama Kami',
    'sub' => 'PopBox mencari tim yang ingin membuat karya yang positif dan memberi arti untuk masyarakat.',
];
