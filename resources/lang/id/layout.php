<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Page Lines
    |--------------------------------------------------------------------------
    |
    */
    'tracking'=>'Tracking',
    'home' => 'Beranda',
    'service' => 'Layanan',
    'avaibility-btn' => 'Ketersediaan Loker',
    'location'=>'Lokasi',
    'promo'=>'Promo',
    'merchant'=>'Mitra',
    'tag'=>'Belanja Online? #PopBoxinAja',
    'callus'=>'Hubungi PopBox',
    'our-service'=>'Layanan Kami',
    'contact-url'=>'Kontak',
    'career-url'=>'Karir',
    'faq-url'=>'FAQ',
    'blog-url'=>'Blog',
    'merchant-url'=>'Merchant',
    'partner-url'=>'Partner',
    'terms'=>'Syarat & Ketentuan',
    'privacy'=>'Kebijakan Privasi',
    'city-select' => 'Pilih Kota'
];
