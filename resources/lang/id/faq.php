<?php

return [

    /*
    |--------------------------------------------------------------------------
    | FAQ Page Lines
    |--------------------------------------------------------------------------
    |
    */
    'ambil-btn'=>'Mengambil Barang',
    'return-btn'=>'Pengembalian Barang',

    'apaitu-title' => 'Apa Itu PopBox',
    'apaitu-content'=> 'PopBox adalah sistem loker otomatis dimana kamu bisa mengambil sendiri barang belanjaan kamu kapanpun dan dimanapun.  Ketika barang kamu telah sampai di loker dan siap diambil kamu akan mendapat notifikasi dan kode PIN lewat SMS. PopBox membantu meminimalisi kemungkinan paket rusak dan barang hilang saat proses pengiriman',

    'kenapa-title'=>'Kenapa memakai PopBox?',
    'kenapa-sub-1'=>'Mudah',
    'kenapa-sub-1-content'=> 'Loker PopBox, mempermudah mengambil, mengembalikan, atau mengirim paket kamu. Seluruh proses tidak memakan waktu lebih dari 5 detik. Cukup datang ke loker dan akses menu pada layar loker sesuai layanan yang ingin digunakan.',
    'kenapa-sub-2'=>'Hemat & Cepat',
    'kenapa-sub-2-content' => 'Loker PopBox memungkinkan pengiriman di hari yang sama dengan ongkos kirim yang murah.',
    'kenapa-sub-3'=> 'Aman & Nyaman',
    'kenapa-sub-3-content'=> 'Ambil barang kamu menggunakan kode PIN. Tidak perlu lagi khawatir barang kamu hilang, rusak, atau tertukar karena diawasi CCTV 24 jam. Ambil barang kamu dimanpun dan kapanpun yang kamu mau.',

    'dimana-title'=>'Di mana lokasi PopBox?',
    'dimana-content'=>'Loker PopBox berada di lokasi strategis seperti mall, perkantoran, apartemen, univesitas, stasiun kereta api, mini market, dll. Saat ini PopBox ada di 100 lebih titik lokasi. Untuk mencari loker terdekat buka halaman ini <a href="https://location.popbox.asia/">PopBox Location</a>',

    'ukuran-title'=>'Berapa ukuran masing-masing loker Popbox?',
    'ukuran-content'=>'<ol>
                            <li>XS : 18.5 cm (L) x 19.5 cm (H) x 48 cm (W)</li>
                            <li>S : 10 cm (L) x 34.5 cm (H) x 48 cm (W) - (contoh: dokumen, laptop, handphone, dll)</li>
                            <li>M : 18.5 cm (L) x 34.5 cm (H) x 48 cm (W) - (contoh: kotak sepatu)</li>
                            <li>L : 31 cm (L) x 34.5 cm (H) x 48 cm (W) - (contoh: kardus mie instan)</li>
                        </ol>
                        Dengan maksimum berat adalah 3kg per paket ',

    'asuransi-title'=> 'Apakah barang saya diasuransikan?',
    'asuransi-content'=> 'Kami akan menjamin barang Anda maksimal 1 juta rupiah untuk setiap barang di loker PopBox',

    'aman-title'=> 'Seberapa amannya barang di dalam loker popbox?',
    'aman-content'=> 'Barang kamu sangat aman berada di dalam loker popbox dengan keamanan kode buka PIN dan keamanan kamera CCTV 24 jam/7.',

    'beli-title'=> 'Bagaimana saya mengirim pembelian online saya melalui PopBox?',
    'beli-content'=>'<ol>
                            <li>Belanja di merchant/toko online yang bekerjasama dengan PopBox. Saat ini merchant yang bekerja sama dengan PopBox adalah: MatahariMall, SeroyaMart, Asmaraku.com, FrozenShop.com, SukaOutdoor.com, Lolalo.la, Orori, Best Buy World, dan masih banyak lagi info lengkap silahkan kunjungi <a href="https://popbox.asia/merchant">PopBox Merchant</a></li>
                            <li>Ketika di halaman checkout, pilih PopBox sebagai pengiriman, dan pilih lokasi loker PopBox yang terdekat.</li>
                            <li>Pastikan bahwa nomor telepon yang kamu masukkan sudah benar</li>
                            <li>Ketika barang sudah sampai di loker yang dituju, kamu akan menerima notifikasi sms yang berisi 6 digit kode PIN untuk membuka loker</li>
                        </ol>',

    'status-title'=> 'Bagaimana cara mengetahui barang sudah dikirim ke loker PopBox?',
    'status-content'=> 'Ketika barang sudah sampai di loker yang dituju, kamu akan menerima notifikasi sms yang berisi 6 digit kode PIN untuk membuka loker',

    'lama-title'=> 'Berapa lama waktu yang diberikan jika barang belum diambil?',
    'lama-content'=> 'PopBox memberikan waktu sampai 72 jam (3 hari) untuk mengambil barang di dalam loker. Jika dalam 72 jam barang tersebut tidak diambil maka barang dikembalikan ke penjual.',

    'return-title'=> 'Apa Itu layanan pengembalian barang?',
    'return-content'=> 'Layanan dari PopBox yang dapat membantu kamu untuk melakukan pengembalian barang yang kamu beli di toko online yang bekerjasama degan PopBox.',

    'merchant-return-title'=> 'Toko online apa saja yang bekerjasama untuk pengembalian barang?',
    'merchant-return-content'=> 'Saat ini kamu dapat mengembalikan barang untuk pembelian di MatahariMall, Lazada, dan Zalora',

    'cara-return-title'=> 'Bagaimana cara mengembalikan barang?',
    'cara-return-content'=> '<ol>
                            <li>Kamu harus mengajukan pengembalian barang ke toko online tempat kamu membeli barang itu.</li>
                            <li>Jika sudah mendapatkan kode pengembalian barang, kemas paket kamu dan tempelkan kode pengembalian di luar paket.</li>
                            <li>Datanglah ke loker PopBox dan akses menu “PENGEMBALIAN BARANG”</li>
                            <li>Pilih toko online</li>
                            <li>Scan/masukkan kode pengembalian</li>
                            <li>Masukkan nomor telepon kamu, pastikan datanya adalah data yang benar karena nomor ini akan PopBox gunakan untuk mengirimkan SMS notifikasi</li>
                            <li>Pilih ukuran loker dan letakkan barang kamu</li>
                            <li>Selesai</li>
                        </ol>',

    'biaya-return-title'=> 'Berapa biaya pengembalian barang?',
    'biaya-return-content'=> 'GRATIS! Kamu tidak perlu membayar untuk pengembalian barang lewat PopBox. Kami akan mengantarkan barangmu ke warehouse atau penjual barang kamu.',

    'status-return-title'=> 'Bagaimana tahu status apakah barang sudah sampai ke penjual?',
    'status-return-content'=> 'PopBox akan mengirimkan SMS notifikasi ke nomor yang kamu daftarkan saat meletakkan barang.',

    'apa-cod-title'=> 'Apa itu COD?',
    'apa-cod-content'=> 'Layanan baru dari PopBox, dimana kamu dapat melakukan pembayaran di lokasi loker kemudian dapat mengambil langsung belanjaan kamu di loker PopBox.',

    'loker-cod-title'=> 'Loker PopBox mana yang dapat menjadi tujuan pengambilan barang COD?',
    'loker-cod-content'=> 'Saat ini PopBox bekerjasama dengan Superindo, kamu dapat datang ke Superindo cabang: Mampang, Duren Tiga, Pancoran, dan Basura City.',

    'bayar-cod-title'=> 'Bagaimana cara membayar?',
    'bayar-cod-content'=> 'Kamu dapat datang ke deposit counter Superindo membawa bukti SMS notifikasi dari PopBox. Sebutkan nomor hp dan nomor order kepada kasir yang bertugas, kemudian bayar dengan uang cash. <br>
        Setelah membayar kamu akan dikirimkan kode PIN untuk membuka loker dan mengambil belanjaan kamu.',

    'cara-cod-title'=> 'Bagaimana cara mengambil barang?',
    'cara-cod-content'=> '<ol>
                            <li>Datang ke loker, pilih bahasa yang digunakan.</li>
                            <li>Akses menu “MENGAMBIL BARANG”</li>
                            <li>Masukkan 6 digit kode PIN yang ada di sms notifikasi dari PopBox</li>
                            <li>Loker akan terbuka</li>
                            <li>Ambil barang kamu dan tutup pintu loker</li>
                        </ol>',

    'lama-cod-title'=> 'Berapa lama waktu pembayaran barang cod?',
    'lama-cod-content'=> 'Kamu dapat melakukan pembayaran maksimal 7 hari setelah mendapat notifikasi dari PopBox.',

    'apa-popshop-title'=> 'Apa Itu PopShop?',
    'apa-popshop-content'=> 'Cara belanja baru, kamu dapat memilih produk katalog yang ada di loker PopBox atau di <a href="http://shop.popbox.asia/">shop.popbox.asia</a> dan ambil barang kamu di loker PopBox.',

    'cara-popshop-title'=> 'Bagaimana cara belanja?',
    'cara-popshop-content'=> '<ol>
                            <li>Download aplikasi Uangku atau aplikasi Dompetku dan lakukan TopUp</li>
                            <li>Scan barang di katalog dan lakukan konfirmasi pembayaran</li>
                            <li>Pilih lokasi PopBox terdekat</li>
                            <li>PopBox akan monotifikasi ketika barang sudah sampai lewat SMS dengan dengan kode PIN untuk membuka loker</li>
                            <li>Datang ke loker, akses menu “MENGAMBIL BARANG”</li>
                            <li>Masukkan kode PIN dan ambil barang belanjaan kamu</li>
                            <li>Tutup kembali pintu loker</li>
                        </ol>',

    'apa-popsend-title'=> 'Apa Itu PopSend?',
    'apa-popsend-content'=> 'PopSend adalah metode pengiriman baru, kamu dapat melakukan drop paket di loker terdekat, dan PopBox akan mengantar ke alamat tujuan. Dengan PopSend kamu tidak perlu mengantri atau menunggu kurir pickup paket. <br> Saat ini kami membuka pengiriman dari Jabodetabek ke Jabodetabek. Lokasi PopBox saat ini ada di 100+ lokasi sehingga kamu dapat dengan mudah menemukan lokasi untuk melakukan drop paket.',

    'cara-popsend-title'=> 'Bagaimana cara belanja?',
    'cara-popsend-content'=> '<ol>
                            <li>Buat order via aplikasi PopSend.</li>
                            <li>Kemas paket kamu dengan baik.</li>
                            <li>Cetak invoice PopSend dan tempelkan pada paket atau tulis nomor order dan alamat tujuan pada paket dengan jelas.</li>
                            <li>Datang ke loker PopBox dan buka menu "MENGIRIM BARANG".</li>
                            <li>Scan nomor order atau masukkan manual nomor order (misal : "PLA1234567890")</li>
                            <li>Letakkan barang dan tutup pintu loker</li>
                        </ol>',
    'lama-popsend-title'=> 'Berapa lama paket sampai ke tujuan?',
    'lama-popsend-content'=> 'Layanan PopSend adalah layanan “besok sampai” jadi kurir PopBox akan mengantar paket kamu H+1 setelah kamu meletakkan barang di loker.',
];
