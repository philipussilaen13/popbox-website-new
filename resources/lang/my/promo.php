<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Promo Page Lines
    |--------------------------------------------------------------------------
    |
    */

    'subscribe' => 'Signup to get the latest promo from us and our partners!',
    'subscribe2' => '',
    'subscribe-btn'=> 'Subscribe',
    'input'=>'Input Email'
];
