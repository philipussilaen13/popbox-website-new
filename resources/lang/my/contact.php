<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Promo Page Lines
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'Contact Us',
    'sub' => 'Our customer representative will help you',
    'call'=>'Contact PopBox',
    'input-name'=>'Name',
    'input-email'=>'Email',
    'input-hp'=>'Phone Number',
    'input-subjek'=>'Subject',
    'input-message'=>'Message',
    'input-btn'=>'Send',
    'modal-title'=>'Thank You',
    'modal-content'=>'Thanks for your feedback. We will contact you.',
    'contact_address' => 'Level 1, Sunway Innovation Labs,<br> Menara Sunway, Jalan Lagoon Timur.<br> Bandar Sunway 47500 <br>Selangor Darul Ehsan, Malaysia',
    'contact-phone' => '+60 0111 0606 011',
    'contact-fax' => '+603 5639 8946',
    'contact-mobile' => '+601 2324 6857',
    'contact-location' => 'Menara Sunway'
];
