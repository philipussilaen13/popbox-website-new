<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Promo Page Lines
    |--------------------------------------------------------------------------
    |
    */
    'title' => 'Tracking',
    'sub' => 'Track your shipment',
    'search-awb' => 'Please enter AWB number then click the search button',
    'parcel' => 'Parcel Number',
    'parcel-btn'=>'Search'
];
