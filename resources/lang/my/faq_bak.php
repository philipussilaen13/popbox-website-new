<?php

return [

    /*
    |--------------------------------------------------------------------------
    | FAQ Page Lines
    |--------------------------------------------------------------------------
    |
    */
   
    'ambil-btn'=>'Take Parcel',
    'return-btn'=>'Return Parcel',

    'apaitu-title' => 'What is PopBox?',
    'apaitu-content'=> 'PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently. When parcel you have reached the lockers and ready to be taken you will receive a notification and a PIN code via SMS. PopBox help minimize the possibility of damaged packages and items lost during the delivery process.',

    'kenapa-title'=>'Why use PopBox?',
    'kenapa-sub-1'=>'Easy',
    'kenapa-sub-1-content'=> 'Lockers PopBox, makes it easy to retrieve, restore, or send your package. The whole process should not take more than 5 seconds. Simply come to the lockers and locker access on-screen menus as per the service that you want to use.',
    'kenapa-sub-2'=>'Save & Fast',
    'kenapa-sub-2-content' => 'Lockers PopBox allows delivery on the same day as the postage cost.',
    'kenapa-sub-3'=> 'Safe & Convenient',
    'kenapa-sub-3-content'=> 'Take a parcel use PIN code. No more need to worry about your parcel is lost, damaged, or swapped for CCTV monitored 24 hours. Take your parcel wherever and whenever you want.',

    'dimana-title'=>'Where are PopBox?',
    'dimana-content'=>'PopBox lockers located in strategic locations such as malls, office buildings, apartments, the university, the railway station, mini market, etc. PopBox has in more than 100 locations. To locate the nearest locker open this page <a href="https://location.popbox.asia/">PopBox Location</a>',

    'ukuran-title'=>'What is the size of each locker PopBox?',
    'ukuran-content'=>'<ol>
                            <li>XS : 18.5 cm (P) x 19.5 cm (T) x 48 cm (L)</li>
                            <li>S : 10 cm (P) x 34.5 cm (T) x 48 cm (L) - (contoh: dokumen, laptop, handphone, dll)</li>
                            <li>M : 18.5 cm (P) x 34.5 cm (T) x 48 cm (L) - (contoh: kotak sepatu)</li>
                            <li>L : 31 cm (P) x 34.5 cm (T) x 48 cm (L) - (contoh: kardus mie instan)</li>
                        </ol>
                        Dengan maksimum berat adalah 3kg per paket ',

    'asuransi-title'=> 'Are my parcel insured?',
    'asuransi-content'=> 'We will ensure your items maximum of 285RM for each item in the locker PopBox.',

    'aman-title'=> 'How safe the parcel in the locker PopBox?',
    'aman-content'=> 'Your parcel is very safe in the locker PopBox with PIN unlock code security and CCTV security cameras 24 hours / 7.',

    'beli-title'=> 'How do I send my online purchases through PopBox?',
    'beli-content'=>'<ol>
                            <li>Shopping at merchants / online stores in collaboration with PopBox. The merchant PopBox are: Bonia, Elianto, Laundry on the go, Yan and more information please visit <a href="https://popbox.asia/merchant">PopBox Merchant</a></li>
                            <li>When on the checkout page, select PopBox as delivery option, and select the location nearest PopBox lockers.</li>
                            <li>Make sure that the phone number you entered is correct</li>
                            <li>When the goods arrived in the locker to go to, you will receive an SMS notification containing the 6-digit PIN code to unlock the locker.</li>
                        </ol>',

    'status-title'=> 'How do I know the parcel have been delivered to the locker PopBox?',
    'status-content'=> 'When the parcels arrived in the locker you choose, you will receive an SMS notification containing the 6-digit PIN code to unlock the locker',

    'lama-title'=> 'How long it will be given if parcels have not been taken?',
    'lama-content'=> 'PopBox give time up to 72 hours (3 days) to take the parcels inside the locker. If within 72 hours the parcels are not taken then the parcels are returned to the seller.',

    'return-title'=> 'Apa Itu layanan pengembalian barang?',
    'return-content'=> 'Layanan dari PopBox yang dapat membantu kamu untuk melakukan pengembalian barang yang kamu beli di toko online yang bekerjasama degan PopBox.',

    'merchant-return-title'=> 'The online store anything that cooperate for the return of goods?',
    'merchant-return-content'=> 'You can return the goods purchases in Lazada.',

    'cara-return-title'=> 'How do I return the goods?',
    'cara-return-content'=> '<ol>
                            <li>You must submit return of goods to the online shop where you bought it.</li>
                            <li>If you already have a return code, pack your packages and paste the code returns outside of the package.</li>
                            <li>Come to the locker PopBox and access menu "RETURNS GOODS"</li>
                            <li>Select online store</li>
                            <li>Scan / enter the return code</li>
                            <li>Enter your phone number, make sure the data is the correct data because this number will PopBox use to send SMS notifications</li>
                            <li>Select the size of the locker and put your stuff</li>
                            <li>Finished</li>
                        </ol>',

    'biaya-return-title'=> 'How much cost return of the goods?',
    'biaya-return-content'=> 'FREE! You do not need to pay for the return of goods by PopBox. We will deliver your items to warehouse.',

    'status-return-title'=> 'How to know the status of whether the goods arrived to the seller?',
    'status-return-content'=> 'PopBox will send SMS notifications to the number that you registered when putting items.',

    'apa-cod-title'=> 'Apa itu COD?',
    'apa-cod-content'=> 'Layanan baru dari PopBox, dimana kamu dapat melakukan pembayaran di lokasi loker kemudian dapat mengambil langsung belanjaan kamu di loker PopBox.',

    'loker-cod-title'=> 'Loker PopBox mana yang dapat menjadi tujuan pengambilan barang COD?',
    'loker-cod-content'=> 'Saat ini PopBox bekerjasama dengan Superindo, kamu dapat datang ke Superindo cabang: Mampang, Duren Tiga, Pancoran, dan Basura City.',

    'bayar-cod-title'=> 'Bagaimana cara membayar?',
    'bayar-cod-content'=> 'Kamu dapat datang ke deposit counter Superindo membawa bukti SMS notifikasi dari PopBox. Sebutkan nomor hp dan nomor order kepada kasir yang bertugas, kemudian bayar dengan uang cash. <br>
        Setelah membayar kamu akan dikirimkan kode PIN untuk membuka loker dan mengambil belanjaan kamu.',

    'cara-cod-title'=> 'Bagaimana cara mengambil barang?',
    'cara-cod-content'=> '<ol>
                            <li>Datang ke loker, pilih bahasa yang digunakan.</li>
                            <li>Akses menu “MENGAMBIL BARANG”</li>
                            <li>Masukkan 6 digit kode PIN yang ada di sms notifikasi dari PopBox</li>
                            <li>Loker akan terbuka</li>
                            <li>Ambil barang kamu dan tutup pintu loker</li>
                        </ol>',

    'lama-cod-title'=> 'Berapa lama waktu pembayaran barang cod?',
    'lama-cod-content'=> 'Kamu dapat melakukan pembayaran maksimal 7 hari setelah mendapat notifikasi dari PopBox.',

    'apa-popshop-title'=> 'Apa Itu PopShop?',
    'apa-popshop-content'=> 'Cara belanja baru, kamu dapat memilih produk katalog yang ada di loker PopBox atau di <a href="http://shop.popbox.asia/">shop.popbox.asia</a> dan ambil barang kamu di loker PopBox.',

    'cara-popshop-title'=> 'Bagaimana cara belanja?',
    'cara-popshop-content'=> '<ol>
                            <li>Download aplikasi Uangku atau aplikasi Dompetku dan lakukan TopUp</li>
                            <li>Scan barang di katalog dan lakukan konfirmasi pembayaran</li>
                            <li>Pilih lokasi PopBox terdekat</li>
                            <li>PopBox akan monotifikasi ketika barang sudah sampai lewat SMS dengan dengan kode PIN untuk membuka loker</li>
                            <li>Datang ke loker, akses menu “MENGAMBIL BARANG”</li>
                            <li>Masukkan kode PIN dan ambil barang belanjaan kamu</li>
                            <li>Tutup kembali pintu loker</li>
                        </ol>',

    'apa-popsend-title'=> 'Apa Itu PopSend?',
    'apa-popsend-content'=> 'PopSend adalah metode pengiriman baru, kamu dapat melakukan drop paket di loker terdekat, dan PopBox akan mengantar ke alamat tujuan. Dengan PopSend kamu tidak perlu mengantri atau menunggu kurir pickup paket. <br> Saat ini kami membuka pengiriman dari Jabodetabek ke Jabodetabek. Lokasi PopBox saat ini ada di 100+ lokasi sehingga kamu dapat dengan mudah menemukan lokasi untuk melakukan drop paket.',

    'cara-popsend-title'=> 'Bagaimana cara belanja?',
    'cara-popsend-content'=> '<ol>
                            <li>Buat order via aplikasi PopSend.</li>
                            <li>Kemas paket kamu dengan baik.</li>
                            <li>Cetak invoice PopSend dan tempelkan pada paket atau tulis nomor order dan alamat tujuan pada paket dengan jelas.</li>
                            <li>Datang ke loker PopBox dan buka menu "MENGIRIM BARANG".</li>
                            <li>Scan nomor order atau masukkan manual nomor order (misal : "PLA1234567890")</li>
                            <li>Letakkan barang dan tutup pintu loker</li>
                        </ol>',
    'lama-popsend-title'=> 'Berapa lama paket sampai ke tujuan?',
    'lama-popsend-content'=> 'Layanan PopSend adalah layanan “besok sampai” jadi kurir PopBox akan mengantar paket kamu H+1 setelah kamu meletakkan barang di loker.',
];
