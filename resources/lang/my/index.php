<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Index Page Lines
    |--------------------------------------------------------------------------
    |
    */

    'tag' => '#JustPopBox',
    'tag2'=> '"EASY. SAFE. FAST. CONVENIENT."',
     'tag-btn'=> 'Our Services',
    'whats'=> 'What is PopBox?',
    'whats2'=> 'PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently.',
    'slider-btn'=>'',
    'services-title'=>'PopBox Services',
    'services-sub'=>'We are ready to help with our parcel locker!',
    'services-sub2'=>'OTHER SERVICES',

    'services-1'=> 'First Mile / Last Mile',
    'services-1-sub'=> 'Use Popbox – Parcel Locker as a tool to send and receive parcel. Are you rarely at home? Tired of waiting too long for the courier to arrive? Delivering parcel are now faster, safer, and easier. Find the nearest Popbox parcel Locker available near you now.',
    'services-2'=>'Return Management',
    'services-2-sub'=>'Popbox works with eCommerce in terms of returning parcel. Consumers are able to return parcel in a much easier way than usual. This fast delivery process does not take more than 5 seconds.',
    'services-3'=>'Cash On Pickup ("COP")',
    'services-3-sub'=>'Don’t have credit card? Popbox offers you Cash On Delivery service. You can pay to our cashier while picking up your parcel from our locker. ',

    'services-4'=>'Advertising',
    'services-4-sub'=>'Located at high traffic venues, PopBox lockers are effective in communicating with consumers. Now brands can get their products featured on PopBox. PopBox also works with selective brands on product activation campaign',
    'services-5'=>'PopSend',
    'services-5-sub'=>'A new way for parcel delivery using parcel locker. Pick the nearest parcel locker near you and Popbox will deliver it for you.',
    'services-6'=>'PopShop',
    'services-6-sub'=>'Quirky products at super fast deliveries! Buy hundreds of interesting products and collect it at a PopBox locker near you, Same Day or Next Day!',
    'services-7'=>'On Demand',
    'services-7-sub'=>'Doing your laundry so easy with PopBox on Demand',
    
    'location-title'=>'PopBox Location',
    'location-sub'=>'Find The Nearest Locker',
    'location-btn'=>'Find Nearest Locker',
    'location-search' => 'Find Nearest Locker',
    'location-map-btn'=>'All Locker',

    'locker-title'=> 'PopBox Locker',
    'locker-sub'=>'Our lockers are in strategic locations such as malls, office buildings, apartments, the university, the railway station, mini market, etc.',
    'partner-title'=>'Our Partner',
    'partner-sub'=>'PopBox works together with your favorite eCommerce Merchant',
    'partner-btn'=> 'Check Our Merchant',
    'count-city'=>'CITIES',
    'count-locker'=>'LOCKERS',
    'count-compartment'=>'COMPARTMENTS',
    'news-title'=>'As Seen On',

    'testi-title'=>'Testimonial',
    'testi-btn' => 'See All Testimonials',
    'testi-sub'=>'What They Said About PopBox',
    'testi-1'=>"Very pleased to partner with PopBox. Having Elocker that is supported by PopBox provides convenience for our customers to receive their parcels, especially in remote areas. It's very simple and practical. aside from that, we are very satisfied with service provided by PopBox and also the innovations they created.",
    'testi-2'=>'Popbox provides additional privacy and convenience to our customers which differentiates AsmaraKu.com from other ecommerces. Our customers love the fact that they can pick up the packages themselves in various strategic places around the cities',
    'testi-3'=>'Perfect for someone who loves online shopping but rarely at home.',
    'testi-4'=>"It's very simple, easy and practical.",

    'maps-location'=>'Bandar Sunway',
    'maps-zoom'=>'11'
];
