<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Page Lines
    |--------------------------------------------------------------------------
    |
    */
    'tracking'=>'Tracking',
    'home' => 'Home',
    'service' => 'Services',
    'avaibility-btn' => 'Locker Availability',
    'location'=>'Location',
    'promo'=>'Promo',
    'merchant'=>'Merchant',
    'tag'=>'Online Shopping? #UsePopBox',
    'callus'=>'Contact PopBox',
    'our-service'=>'Our Services',
    'contact-url'=>'Contact Us',
    'career-url'=>'Career',
    'faq-url'=>'FAQ',
    'blog-url'=>'News',
    'merchant-url'=>'Merchant',
    'partner-url'=>'Partner',
    'terms'=>'Terms & Agreement',
    'privacy'=>'Privacy Policy',
    'city-select' => 'Choose City'
];
