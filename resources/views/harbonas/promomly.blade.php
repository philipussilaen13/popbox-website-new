<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Popbox Thematic</title>
<meta name="description" content="PopBox Thematic Display Decoration Competition, register now!">
<meta name="author" content="Popbox Asia">
<meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:title" content="March Promo By PopBox" />
<meta property="og:description" content="PopBox Thematic Display Decoration Competition, register now!" />

<meta property="og:image" content="{{ URL::asset('img/promomly/meta-popboxthematic.jpg')}}" />
 <link rel="icon" type="image/png" href="{{URL::asset('img/favicon.ico')}}">
<link rel="icon" type="image/png" href="{{URL::asset('img/favicon-32x32.png')}}" sizes="32x32" />
{{-- <link rel="icon" type="image/png" href="{{URL::asset('img/favicon-16x16.png')}}" sizes="16x16" /> --}}

<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/style.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/promomly.css')}}">
<meta name="viewport" content="width=device-width,initial-scale=1">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{ elixir('js/jquery.gmap.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
<style type="text/css">    
    </style>
       <script>
            // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            // ga('create', 'UA-72128831-1', 'auto');
            // ga('send', 'pageview');
        </script>
</head>
<body>
	<div class="container">
		

		<div class="row">
			<div class="col-md-12">				
				<img src="{{URL::asset('img/promomly/banner_promo_mly.png')}}" class="img-banner">				
			</div>
		</div>

		<div class="row">			
			<div class="col-md-12 center">		
				<div class="line">		
				</div>
				<!-- <img src="{{URL::asset('img/promomly/line.png')}}" width="95%"> -->
			</div>			
		</div>

		<div class="row">			
			<div class="col-md-12 title-rules">				
				<p>RULES :</p>
			</div>			
		</div>

		<div class="row">			
			<div class="col-md-12 conten-rules">				
				<ol>
					<li>Participant needs to choose one of the themes and develop 2 designs for Popbox <strong>(Lifestyle, E-commerce, and connectivity)</strong></li>
					<li>Participant may work individually or in group <strong>(max: 3 persons)</strong></li>
					<li>
						Participant may use any software to design, but the work must be submitted in <strong>Al (Adobe Illustrator) file & JPEG</strong>
					</li>
					<li>
						Competition date : 20<span class="sth">th</span> March 2017 - 17<span class="sth">th</span> April 2017
					</li>
					<li>
						Participant must submit the work by <strong>17<span class="sth">th</span> April 2017</strong>
					</li>
					<li>
						Please like PopBox’s Facebook page @ PopBox Malaysia to follow on the competition progress
					</li>
					<li>
						Winners will be announced through Facebook page within	 2 weeks' time after the competition ends
					</li>
					<li>Winner prizes :<br/>
						1<span class="sth">st</span> Prize: RM300 cash + Production of Artwork (subject to availability of PopBox)<br/>
						2<span class="sth">nd</span> Prize: RM200 cash + Production of Artwork (subject to availability of PopBox)<br/>
						3<span class="sth">rd</span> Prize: RM100 cash + Production of Artwork (subject to availability of PopBox)<br/>
					</li>
					<li>
						PopBox reserves the rights to use winners' design for production.
					</li>
					<li>
						Participant shall agree to the terms and conditions by PopBox upon the submission of designs to PopBox.
					</li>
				</ol>
			</div>			
		</div>

	
		<div class="row">			
			<div class="col-md-12 center">				
				<a href="https://goo.gl/forms/Q8GiNAUcAr8LvJw62">
					<div class="btn-register">REGISTER NOW</div>
					<!-- <img src="{{URL::asset('img/promomly/btn_register.png')}}" class="btn-register"> -->
				</a>
			</div>			
		</div>

		<div class="row bgyellow">			
			<div class="col-md-12 center">								
				<img src="{{URL::asset('img/promomly/find_us.png')}}" class="img-findus">				
			</div>			
		</div>

		<div class="row bgyellow">			
			<div class="col-md-3 col-xs-6 col-sm-6 center">				
				<a href="https://www.facebook.com/popboxmy" target="_blank">
					<img src="{{URL::asset('img/promomly/fb.png')}}" class="img-socmed">
				</a>
			</div>			
			<div class="col-md-3 col-xs-6 col-sm-6 center">				
				<a href="https://twitter.com/PopBox_my" target="_blank">
					<img src="{{URL::asset('img/promomly/tw.png')}}" class="img-socmed">
				</a>
			</div>			
			<div class="col-md-3 col-xs-6 col-sm-6 center">				
				<a href="https://www.instagram.com/popbox_my/?hl=en" target="_blank">
					<img src="{{URL::asset('img/promomly/ins.png')}}" class="img-socmed">
				</a>
			</div>			
			<div class="col-md-3 col-xs-6 col-sm-6 center">				
				<a href="https://www.youtube.com/watch?v=zaACvMyFqQo" target="_blank">
					<img src="{{URL::asset('img/promomly/youtube.png')}}" class="img-socmed">
				</a>
			</div>			
		</div>

		<div class="row bgyellow">			
			<div class="col-md-12 center">	
				<a href="https://www.popbox.asia">
					<img src="{{URL::asset('img/promomly/link.png')}}" class="img-link">
				</a>
			</div>
		</div>
	</div>

</body>
</html>
