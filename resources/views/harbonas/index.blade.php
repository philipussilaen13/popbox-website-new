<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Harbolnas by PopBox</title>
<meta name="description" content="Harbolnas">
<meta name="author" content="Popbox Asia">
<meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Harbolnas By PopBox" />
<meta property="og:description" content="New way to send your parcel. Locate, select, drop at a PopBox near you and PopSend will deliver. Join PopSend today for the PopBox experience!" />
<meta property="og:image" content="{{ URL::asset('img/meta-img.png')}}" />

<link rel="icon" type="image/png" href="{{URL::asset('img/favicon-32x32.png')}}" sizes="32x32" />
{{-- <link rel="icon" type="image/png" href="{{URL::asset('img/favicon-16x16.png')}}" sizes="16x16" /> --}}

<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/style.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/Harbolnas.css')}}">


<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
  <style type="text/css">
    
    </style>
       <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview');
        </script>
</head>
<body>
	<div class="container">
		<div class="row header">
			<div class="col-md-12">
				<img src="{{ URL::asset('img/Harbolnas/header.png')}}">
			</div>				
		</div>
		<div class="row content">
			<div class="col-md-12">
				<div class="online-partner">ONLINE PARTNERS</div>
			</div>				
		</div>		
		<div class="row group-thumbs">	
			
			<!-- row pertama  -->
				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://shop.popbox.asia')">						
						<img src="{{ URL::asset('img/Harbolnas/img11.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child11.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7">
								<span class="title-thumb">Popshop</span><br/>
								Diskon up <strong>to 80%</strong>
							</div>
						</div>
						<div class="row btn-beli">
							<div class="col-md-12">
								<div>Beli</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/circle.project')">						
						<img src="{{ URL::asset('img/Harbolnas/img12.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child12.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Circle Project</span><br/>
								Diskon <br>
								<span class="title-thumb">Rp 30.000/pcs</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/valecloth.id')">						
						<img src="{{ URL::asset('img/Harbolnas/img13.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child13.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Velecloth.id</span><br/>
								Pembelian minimum <br>
								Rp 200.000 <br/>
								cashback <strong>Rp 25.0000</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/mono_project')">						
						<img src="{{ URL::asset('img/Harbolnas/img14.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child14.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7">
								<span class="title-thumb">Mono</span><br/>
								Diskon <strong>12% +</strong><br/>
								<strong>Rp 50.000</strong> kirim ke<br/>
								Popbox
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/teamo_kidswear')">						
						<img src="{{ URL::asset('img/Harbolnas/img15.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child15.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Teamo Kidswear</span><br/>
								Pembelian minimum <br/>
								Rp 50.000<br/>
								cashback Rp 25.000
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

			
			

			<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/granova.jakbar')">						
						<img src="{{ URL::asset('img/Harbolnas/img21.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child21.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Granova</span><br/>
								Pembelian <strong>Rp. 300.000</strong> <br/>
								<strong>Free</strong> 1 granova 150 gr<br/>
								Pembelian <strong>Rp. 200.000</strong><br/>								
								<strong>Free 1</strong> granova 100 gr
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/wooly.craft')">
						<img src="{{ URL::asset('img/Harbolnas/img22.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child22.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7">			
								<span class="title-thumb">Wooly.Craft</span><br/>
								Buy 1 get 20% off, buy<br/>
								2 get 1 free
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/simpleskullshop')">						
						<img src="{{ URL::asset('img/Harbolnas/img23.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child23.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7">
								<span class="title-thumb">Simpleskullshop</span><br/>
								Beli 2 <strong>Diskon 12%</strong>									
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/dblackcase_store')">						
						<img src="{{ URL::asset('img/Harbolnas/img24.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child24.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7">
								<span class="title-thumb">Dblackcase</span><br/>
								Beli 2 Diskon 12%
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/bubblewings.go')">						
						<img src="{{ URL::asset('img/Harbolnas/img25.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child25.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Bubblewings</span><br/>
								Pembelian minimum<br/>
								Rp 100.000<br/>
								cashback <strong>Rp 25.000</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>
					
		
			<div class="thumbs col-xs-6 .col-sm-6" onclick="onLink('https://www.instagram.com/relisaistiatma')">
					<div class="row thumb ">						
						<img src="{{ URL::asset('img/Harbolnas/img31.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child31.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">RI Storev</span><br/>
								Pembelian minimum<br/>
								Rp 250.000<br/>
								cashback <strong>Rp 50.000</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>
				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/somesheahshop')">						
						<img src="{{ URL::asset('img/Harbolnas/img32.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child32.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Somesheah Shop</span><br/>
								Pembelian minimum<br/>
								Rp 300.000<br/>
								cashback <strong>Rp 30.000</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/kukistyle')">						
						<img src="{{ URL::asset('img/Harbolnas/img33.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child33.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">KUKI</span><br/>
								Pembelian minimum<br/>
								Rp 150.000<br/>
								cashback <strong>Rp 25.000</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/kaycollection')">						
						<img src="{{ URL::asset('img/Harbolnas/img34.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child34.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7">
								<span class="title-thumb">keycollection</span><br/>
								Pembelian minimum<br/>
								Rp 250.000<br/>
								<strong>Diskon 20%</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/tatuisjakarta')">						
						<img src="{{ URL::asset('img/Harbolnas/img35.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child35.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7">
								<span class="title-thumb">Tatuis Jakarta</span><br/>
								Buy 1 Get 1 <strong>FREE</strong>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>			
				
		
			
			<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/koskakimu')">								
						<img src="{{ URL::asset('img/Harbolnas/img41.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child41.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Koskakimu</span><br/>
								Pembelian minimum<br/>
								Rp 35.000<br/>
								cashback <strong>Rp 5.000</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/f8indonesia')">												
						<img src="{{ URL::asset('img/Harbolnas/img42.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child42.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Fashion 8</span><br/>
								Pembelian minimum<br/>
								Rp 150.000<br/>
								cashback <strong>Rp 20.000</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/elpaellashawl')">					
						<img src="{{ URL::asset('img/Harbolnas/img43.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child43.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">Elpaella</span><br/>
								Tissue case Special price<br/>
								Rp 35.000<br/>	
								Cheaper by buying 3pcs<br/>
								<strong>Rp 100.000</strong>
								syal <strong>Buy 4 Get 1 Free</strong>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>

				<div class="thumbs col-xs-6 .col-sm-6">
					<div class="row thumb" onclick="onLink('https://www.instagram.com/calciofigures')">						
						<img src="{{ URL::asset('img/Harbolnas/img44.png')}}" class="img-thumb">
						<div class="row thumb-child">
							<div class="col-md-5 col-xs-5 .col-sm-5">
								<img src="{{ URL::asset('img/Harbolnas/child44.png')}}">
							</div>
							<div class="col-md-7 col-xs-7 .col-sm-7 line09">
								<span class="title-thumb">CalcioFigure</span><br/>
								Pembelian minimum<br/>
								Rp 100.000<br/>	
								Diskon <strong>Rp 25.000</strong>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="btn-beli">Beli</div>								
							</div>
						</div>
					</div>
				</div>
			
			</div>		
		
		<div class="row howto top-pakai">	
			<div class="col-md-12">
				<div class="howtowuse">CARA PAKAI POPBOX</div>
			</div>
		</div>
		<div class="row howto">				
			<div>
				<div class="col-md-3">
					<div class="cls-img-howto"><img src="{{ URL::asset('img/Harbolnas/howto1.png')}}"></div>
					<div class="howto-desc">
						Belanja Online,<br/>
						pilih/ketik PopBox<br/>
						di alamat
					</div>					
				</div>
				<div class="col-md-3">
					<div class="cls-img-howto"><img src="{{ URL::asset('img/Harbolnas/howto2.png')}}"></div>		
					<div class="howto-desc">
						Pilih lokasi,<br/>
						PopBox<br/>
						Terdekatmu
					</div>			
				</div>
				<div class="col-md-3">
					<div class="cls-img-howto"><img src="{{ URL::asset('img/Harbolnas/howto3.png')}}"></div>	
					<div class="howto-desc">
						Barang sampai<br/>
						Kamu akan terima<br/>
						SMS untuk kode<br/>
						buka loker
					</div>				
				</div>
				<div class="col-md-3">
					<div class="cls-img-howto"><img src="{{ URL::asset('img/Harbolnas/howto4.png')}}"></div>			
					<div class="howto-desc">
						Ambil paketmu<br/>
						dan enjoy!
					</div>	
				</div>
			</div>
		</div>		
		<div class="row deliver">	
			<div class="col-md-12">
				<div class="deliver-title">DIMANA SAJA LOKASI LOKER POPBOX?</div>
			</div>
			<div class="col-md-12">
				<div class="deliver-desc">Pobox Sudah berada di lebih 120 titik di Jabodetabek, silahkan cari yang terdekat dengan lokasi kamu</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="map_wrapper">
			        <div id="map_canvas" class="mapping"></div>
			    </div> 
			</div>
		</div>
		<div class="row choose-locker">	
			<div class="col-md-5">
				<div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select City <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="city" style="width:100%;">
                    	@foreach ($lockerBycities as $key => $value)
                        	<li>{{$key}}</li>
                        @endforeach
                    </ul>
                </div>
			</div>
			<div class="col-md-5">
				<div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    	Choose Locker <span class="caret"></span>
                    </button>
                  	<ul class="dropdown-menu" id="loker" style="width:100%;">
                   	</ul>
                </div>
			</div>		
			<div class="col-md-2">	
				<button type="button" class="btn btn-danger">Cari</button>
			</div>
		</div>

		<div class="row aplikasi">	
			<div class="col-md-5 col-xs-12 .col-sm-12 sub-aplikasi">
				<div class="aplikasi-note">
					Download Aplikasinya<br/>
					Dapatkan<br/>
					FREE CREDIT<br/>
					<span id="price">Rp 50.000</span>
				</div>
				<div class="row img-download">
					<div class="col-md-6 col-xs-6 .col-sm-6 popbox-img"><img src="{{ URL::asset('img/Harbolnas/popbox_box.png')}}"></div>
					<div class="col-md-6 col-xs-6 .col-sm-6 popbox-play">
						<a href="https://play.google.com/store/apps/details?id=asia.popbox.app">
							<img src="{{ URL::asset('img/Harbolnas/googleplay.png')}}">
						</a>
					</div>
				</div>
				
			</div>
			<div class="col-md-7 col-xs-12 .col-sm-12 sub-aplikasi">
				<div>
					<img src="{{ URL::asset('img/Harbolnas/applikasi2.png')}}">
				</div>
			</div>
		</div>

		<div class="row syarat">	
			<div class="col-md-12">
				<div class="syarat-title"><span>*</span> Syarat & Ketentuan Berlaku</div>
			</div>
		</div>
		<div class="row syarat">	
			<div class="col-md-4 icon-contact-three">
				<span class="syarat-img"><img src="{{ URL::asset('img/Harbolnas/icon-bottom1.png')}}"></span>021-29022537
			</div>
			<div class="col-md-4  icon-contact-three">
				<a href="mailto:Info@popbox.com"><span class="syarat-img"><img src="{{ URL::asset('img/Harbolnas/icon-bottom2.png')}}"></span>Info@popbox.asia</a>
			</div>
			<div class="col-md-4  icon-contact-three">
				<a href="https://www.facebook.com/pboxasia"><span class="syarat-img"><img src="{{ URL::asset('img/Harbolnas/icon-bottom3.png')}}"></span>Popbox Asia</a>
			</div>
		</div>
		<div class="row syarat group-syarat-four">	
			<div class="col-md-4 icon-contact">
				<a href="https://instagram.com/popbox_asia"><span class="syarat-img"><img src="{{ URL::asset('img/Harbolnas/icon-bottom4.png')}}"></span>popbox_asia</a>
			</div>
			<!-- <div class="col-md-3  icon-contact">
				<a href=""><span class="syarat-img"><img src="{{ URL::asset('img/Harbolnas/icon-bottom5.png')}}"></span>@popboxasia</a>
			</div> -->
			<div class="col-md-4  icon-contact">
				<a href="https://twitter.com/popbox_asia">
					<span class="syarat-img"><img src="{{ URL::asset('img/Harbolnas/icon-bottom6.png')}}"></span>PopBox_Asia
				</a>
			</div>
			<div class="col-md-4  icon-contact">
				<a href="https://www.youtube.com/channel/UCYGHLg4Xdc_IHD5DHdDIRTA">
					<span class="syarat-img"><img src="{{ URL::asset('img/Harbolnas/icon-bottom7.png')}}"></span>Popbox_Asia
				</a>
			</div>
		</div>
	</div>
	<div class="row footer">	
		<div class="col-md-12">
		</div>
	</div>
	<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBf2v8vfQazUaI2egWcoYvwK7ljsAPh6Uk&libraries=places&callback=initialize"async defer></script>
	<script type="text/javascript">
		var lockerByCities = new Array();
		<?php foreach ($lockerBycities as $key => $value): ?>
			    lockerByCities['<?php echo $key ?>']= <?php echo ($value); ?>;
		<?php endforeach ?>

		var arrLockers = new Array();
		<?php foreach ($lockers as $key => $value): ?>			
			    arrLockers['<?php echo $value->name ?>']= <?php echo (json_encode($value)); ?>;
		<?php endforeach ?>

		var lockername = "";

		$(document).ready(function(){
			$('#city li').on('click', function(){
		        $(this).parents(".btn-group").find('.btn').html($(this).text()+" <span class=\"caret\"></span>");
		        $("#loker li").parents(".btn-group").find('.btn').html("Pilih Loker <span class=\"caret\"></span>");
		        var  str_html ='';		        	       
		        $.each(lockerByCities[$(this).text()], function( index, value ){		        	
		            str_html +='<li>'+ value.name+'</li>';		            
		        });
		        $("#loker").parent().removeClass('hide');
        		$("#loker").html(str_html);
			});

			 $(document).on('click', '#loker li', function(){
		        $(this).parents(".btn-group").find('.btn').html($(this).text()+" <span class=\"caret\"></span>");
		        lockername = $(this).text();
		      
		    });

			 $('.btn-danger').on('click', function(){
			 	if (lockername!=''){
				 	var obj = arrLockers[lockername];		        
				 	var lockerMarkers = new Array();
			        var lockerInfoWindowContent = new Array()
			        lockerInfoWindowContent.push(["'" + obj.address + "'"]);
			        lockerMarkers.push(["'" + obj.name + "'", obj.latitude, obj.longitude]);		       
			        test(lockerMarkers, lockerInfoWindowContent, false);
		    	}
			 });

		});

		 function initialize() {
		    // digunakan untuk empty maps default lat lang
		    // var bounds = new google.maps.LatLngBounds(); 
		    // var myLatlng = {lat: -6.20, lng: 106.84};
		    // var map = new google.maps.Map(document.getElementById('map_canvas'), {
		    //   zoom: 11,
		    //   center: myLatlng
		    // });
		    
		    var markers =  new Array(); // or "var valueToPush = new Object();" which is the same
		    var infoWindowContent = new Array();    
		    <?php foreach ($lockers as $value) { ?>       
		       console.log("<?php echo $value->name; ?>");
		       var content = '<div><strong><?php echo $value->name; ?></strong></div><div><?php echo $value->address; ?></div>';
		       infoWindowContent.push(["'" + content + "'"]);
		       markers.push(["'<?php echo $value->name; ?>'", <?php echo $value->latitude; ?>, <?php echo $value->longitude; ?>]);      
		    <?php } ?>       
		    test(markers, infoWindowContent, true);   
 		}
  
  		function test(markers, infoWindowContent, isFirst){      
		    // Multiple Markers
		    var bounds = new google.maps.LatLngBounds();    
		    var mapOptions = {
		        mapTypeId: 'roadmap'
		    };
		                    
		    // Display a map on the page

		    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		    map.setTilt(45);
		        
		    // Display multiple markers on a map
		    var infoWindow = new google.maps.InfoWindow(), marker, i;
		    
		    // Loop through our array of markers & place each one on the map  
		   
		    var url_img = '{{ asset('img/map/marker-locker.png') }}';
		    var image = {
		      url: url_img,
		      origin: new google.maps.Point(0, 0),
		      anchor: new google.maps.Point(17, 34),
		      scaledSize: new google.maps.Size(20, 33)
		    };
		   
		    for( i = 0; i < markers.length; i++ ) {            
		        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);        
		        bounds.extend(position);
		        if (markers[i][0]=="current_location"){                  
		            marker = new google.maps.Marker({
		                position: position,
		                map: map,                
		                title: "Lokasi anda disini"
		            });      
		            infoWindow.setContent(infoWindowContent[i][0]);
		            infoWindow.open(map, marker);
		        }else{
		            marker = new google.maps.Marker({
		                position: position,
		                map: map,
		                icon: image,
		                title: markers[i][0]
		            });
		        }
		        
		        // Allow each marker to have an info window    
		        google.maps.event.addListener(marker, 'click', (function(marker, i) {
		            return function() {
		                infoWindow.setContent(infoWindowContent[i][0]);
		                infoWindow.open(map, marker);
		                console.log(infoWindowContent[i][0]);
		            }
		        })(marker, i));

		        // Automatically center the map fitting all markers on the screen
		        map.fitBounds(bounds);
		    }

		    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
		    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
		        if (isFirst){
		          this.setZoom(11);
		          this.setCenter(new google.maps.LatLng(-6.21, 106.81));
		        }else{
		          this.setZoom(13);
		        }
		        google.maps.event.removeListener(boundsListener);
		    });


		    
		}

		function onLink(link){
			window.location.href = link;
		}
	</script>	 

</body>
</html>
