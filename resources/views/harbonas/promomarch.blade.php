<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Maret Bebas Ongkir</title>
<meta name="description" content="Belanja di merchant kesukaanmu dan rasakan pengalaman menarik menggunakan loker PopBox!">
<meta name="author" content="Popbox Asia">
<meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Promo By PopBox" />
<meta property="og:description" content="BUY online, SELECT and SEND to your nearest PopBox! Experience the convenience of shopping online and awesome deals from our merchants!" />
<meta property="og:image" content="{{ URL::asset('img/promomarch/meta.jpg')}}" />
 <link rel="icon" type="image/png" href="{{URL::asset('img/favicon.ico')}}">
<link rel="icon" type="image/png" href="{{URL::asset('img/favicon-32x32.png')}}" sizes="32x32" />
{{-- <link rel="icon" type="image/png" href="{{URL::asset('img/favicon-16x16.png')}}" sizes="16x16" /> --}}

<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/bootstrap.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/style.css')}}">
<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/promomarch.css')}}">
<meta name="viewport" content="width=device-width,initial-scale=1">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="{{ elixir('js/jquery.gmap.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
<style type="text/css">
    </style>
       <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview');
        </script>
        
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '494571000752305');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=494571000752305&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->   
        
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 banner">			
				<!-- <a href="https://www.popbox.asia"> -->
					<img src="{{URL::asset('img/promomarch/top-banner.png')}}">	
				</a>
			</div>			
		</div>
		<div class="row">
			<div class="col-md-6 col-xs-12 .col-sm-12 pad-menuright">	
				<div class="top-menu">
					<ul>
						<li>MARET</li>
						<li>BEBAS ONGKIR</li>
					</ul>
				</div>
			</div>
			<div class="col-md-6  col-xs-12 .col-sm-12 top-menu">							
				<div >
					<ul>
						<li><a href="javascript:void(0)" onclick="to_position('.link-cara')">CARA</a></li>
						<li><a href="javascript:void(0)" onclick="to_position('.link-merchant')">MERCHANT</a></li>
						<li><a href="javascript:void(0)" onclick="to_position('.link-lokasi')">LOKASI</a></li>				
						<li><a href="javascript:void(0)" onclick="to_position('.link-contact')">KONTAK</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 title-howto link-cara">
				<img src="{{URL::asset('img/promomarch/how-to-label.png')}}" >
			</div>
		</div>

		<div class="row">
			<div class="col-md-3 how-to-detail">				
				<img src="{{URL::asset('img/promomarch/how-to1.png')}}" >
			</div>
			<div class="col-md-3 how-to-detail">				
				<img src="{{URL::asset('img/promomarch/how-to3.png')}}" >
				
			</div>
			<div class="col-md-3 how-to-detail">				
				<img src="{{URL::asset('img/promomarch/how-to4.png')}}" >				
			</div>
			<div class="col-md-3 how-to-detail">				
				<img src="{{URL::asset('img/promomarch/how-to2.png')}}" >
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 title-merchant">				
				<img src="{{URL::asset('img/promomarch/daftar-merchant.png')}}">
			</div>
		</div>

		<div class="row link-merchant">
			<div class="col-md-12">
				<div class="category link-category">
					<img src="{{URL::asset('img/promomarch/fashion-cat.png')}}">
				</div>
			</div>
		</div>

		<div class="row row-category1">
			<div class="five-landing">
				<a href="https://www.frozenshop.com/">
					<img src="{{URL::asset('img/promomarch/fashion1.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="http://www.les-riches.com/">
						<img src="{{URL::asset('img/promomarch/fashion2.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="https://www.orori.com/">
						<img src="{{URL::asset('img/promomarch/fashion3.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="http://www.sissae.com/home">
					<img src="{{URL::asset('img/promomarch/fashion4.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="https://www.instagram.com/simpleskullshop/">
					<img src="{{URL::asset('img/promomarch/fashion5.png')}}">
				</a>
			</div>
			<div class="more-fashion">
				<div class="five-landing">
					<a href="https://www.instagram.com/elpaellashawl/">
						<img src="{{URL::asset('img/promomarch/fashion6.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/bubblewings.go/">
						<img src="{{URL::asset('img/promomarch/fashion7.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/_ristore/">
						<img src="{{URL::asset('img/promomarch/fashion8.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/valecloth.id/">
						<img src="{{URL::asset('img/promomarch/fashion9.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/circle.project/">
						<img src="{{URL::asset('img/promomarch/fashion10.png')}}">
					</a>
				</div>

				<div class="five-landing">
					<a href="https://www.instagram.com/mono_project/">
						<img src="{{URL::asset('img/promomarch/fashion11.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/emma.butik/">
						<img src="{{URL::asset('img/promomarch/fashion12.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/kal.idn/?hl=en">
						<img src="{{URL::asset('img/promomarch/fashion13.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="http://www.kuki-style.com/">
						<img src="{{URL::asset('img/promomarch/fashion14.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://shopggiztresco.com/">
						<img src="{{URL::asset('img/promomarch/fashion15.png')}}">
					</a>
				</div>

				<div class="five-landing">
					<a href="https://www.instagram.com/mvra.id/">
						<img src="{{URL::asset('img/promomarch/fashion16.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/skycloud_id/">
						<img src="{{URL::asset('img/promomarch/fashion17.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.kimofficial.id/">
						<img src="{{URL::asset('img/promomarch/fashion18.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/dmasempo/">
						<img src="{{URL::asset('img/promomarch/fashion19.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/mp_product/">
						<img src="{{URL::asset('img/promomarch/fashion20.png')}}">
					</a>
				</div>

				<div class="five-landing">
					<a href="https://www.instagram.com/njonja_kebaja/">
						<img src="{{URL::asset('img/promomarch/fashion21.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/sciorta_boutique/">
						<img src="{{URL::asset('img/promomarch/fashion22.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/asilla.fashion/">
						<img src="{{URL::asset('img/promomarch/fashion23.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/sheeza_id/">
						<img src="{{URL::asset('img/promomarch/fashion24.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/cedahhijab/">
						<img src="{{URL::asset('img/promomarch/fashion25.png')}}">
					</a>
				</div>

				<div class="five-landing">
					<a href="https://www.instagram.com/nubersupply/">
						<img src="{{URL::asset('img/promomarch/fashion26.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/somesheahshop/">
						<img src="{{URL::asset('img/promomarch/fashion27.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/clutchicstore/">
						<img src="{{URL::asset('img/promomarch/fashion28.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/nappamilano/">
						<img src="{{URL::asset('img/promomarch/fashion29.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/eghastore/">
						<img src="{{URL::asset('img/promomarch/fashion30.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="ttps://www.instagram.com/eunoia_ig/?hl=en">
						<img src="{{URL::asset('img/promomarch/fashion31.png')}}">
					</a>
				</div>
			</div>
		</div>

		<div class="row fashion-all">			
			<div class="col-md-12 daftar-merchant">
				<a href="javascript:void(0)" onclick="onShowAllFashion()">
						<img src="{{URL::asset('img/promomarch/show-all.png')}}">
				</a>
			</div>					
		</div>

		<div class="row fashion-less">
			<div class="col-md-12 daftar-merchant">
				<a href="javascript:void(0)" onclick="onShowLessFashion()"><img src="{{URL::asset('img/promomarch/show-less.png')}}"></a>
			</div>					
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="category">
					<img src="{{URL::asset('img/promomarch/kids.png')}}">
				</div>
			</div>
		</div>

		<div class="row row-category1">
			<div class="five-landing">
				<a href="https://www.instagram.com/candybabygift/?hl=en">
					<img src="{{URL::asset('img/promomarch/kids1.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="http://www.hoppipolla.id/">
					<img src="{{URL::asset('img/promomarch/kids2.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="https://www.instagram.com/xiao_in82/">
					<img src="{{URL::asset('img/promomarch/kids3.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="https://www.instagram.com/theona.tata/">
					<img src="{{URL::asset('img/promomarch/kids4.png')}}">
				</a>
			</div>
			<div class="five-landing">
				<a href="https://www.instagram.com/teensnkids/">
					<img src="{{URL::asset('img/promomarch/kids5.png')}}">
				</a>
			</div>

			<div class="more-kids">
				<div class="five-landing">
					<a href="https://www.instagram.com/tokodnd/">
						<img src="{{URL::asset('img/promomarch/kids6.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/kokogibran/">
						<img src="{{URL::asset('img/promomarch/kids7.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="#">
						<img src="{{URL::asset('img/promomarch/kids8.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/arathelle_whs/">
						<img src="{{URL::asset('img/promomarch/kids9.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="http://www.pojokanak.com/">
						<img src="{{URL::asset('img/promomarch/kids10.png')}}">
					</a>
				</div>

				<div class="five-landing">
					<a href="https://shopee.co.id/babyshopgrosir">
						<img src="{{URL::asset('img/promomarch/kids11.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/rainbowhampers/">
						<img src="{{URL::asset('img/promomarch/kids12.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/varoapparel/">
						<img src="{{URL::asset('img/promomarch/kids13.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/zoetoys/">
						<img src="{{URL::asset('img/promomarch/kids14.png')}}">
					</a>
				</div>
				<div class="five-landing">
					<a href="https://www.instagram.com/zoetoys/">
						<img src="{{URL::asset('img/promomarch/kids15.png')}}">
					</a>
				</div>
			</div>
		</div>

		<div class="row kids-all">			
			<div class="col-md-12 daftar-merchant">
				<a href="javascript:void(0)" onclick="onShowAllKids()"><img src="{{URL::asset('img/promomarch/show-all.png')}}"></a>
			</div>					
		</div>

		<div class="row kids-less">			
			<div class="col-md-12 daftar-merchant">
				<a href="javascript:void(0)" onclick="onShowLessKids()"><img src="{{URL::asset('img/promomarch/show-less.png')}}"></a>
			</div>					
		</div>

		
		<div class="row two-category">			
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-12 ">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/home.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://www.tayakalaundry.com/">
								<img src="{{URL::asset('img/promomarch/home1.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://www.taptopick.co/">
								<img src="{{URL::asset('img/promomarch/home2.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://omaisu.id/">
								<img src="{{URL::asset('img/promomarch/home3.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://laundrysepatumu.com/">
								<img src="{{URL::asset('img/promomarch/home4.png')}}">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2">
			</div>
			<div class="col-md-5">								
				<div class="row">
					<div class="col-md-12">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/sport.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://sukaoutdoor.com/">
								<img src="{{URL::asset('img/promomarch/sport1.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.mitre.co.id/">
								<img src="{{URL::asset('img/promomarch/sport2.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://www.asports.id/">
								<img src="{{URL::asset('img/promomarch/sport3.png')}}">
							</a>
						</div>
					</div>
				</div>


			</div>					
		</div>

		<div class="row two-category">			
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-12 col-xs-6 .col-sm-6 ">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/health.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.instagram.com/kaycollection/">
								<img src="{{URL::asset('img/promomarch/health1.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://www.priskila.co.id/">
								<img src="{{URL::asset('img/promomarch/health2.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.instagram.com/prettyeonnie/">
								<img src="{{URL::asset('img/promomarch/health3.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://perfectbeauty.id/">
								<img src="{{URL::asset('img/promomarch/health4.png')}}">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2">
			</div>
			<div class="col-md-5">								
				<div class="row">
					<div class="col-md-12">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/all-cat.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.asmaraku.com/">
								<img src="{{URL::asset('img/promomarch/all-cat1.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://wearinasia.com/">
								<img src="{{URL::asset('img/promomarch/all-cat2.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://indoshopper.com/">
								<img src="{{URL::asset('img/promomarch/all-cat3.png')}}">
							</a>
						</div>
					</div>
				</div>
			</div>					
		</div>
		<div class="row two-category">			
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-12 ">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/gadget.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://www.klik4it.com/">
								<img src="{{URL::asset('img/promomarch/gadget1.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.instagram.com/dblackcase_store/">
								<img src="{{URL::asset('img/promomarch/gadget2.png')}}">
							</a>
						</div>
					</div>					
				</div>
			</div>
			<div class="col-md-2">
			</div>
			<div class="col-md-5">								
				<div class="row">
					<div class="col-md-12">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/hobby.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="http://www.diaboliquetoys.com/">
								<img src="{{URL::asset('img/promomarch/hobby1.png')}}">
							</a>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.facebook.com/zhobbycollection/">
								<img src="{{URL::asset('img/promomarch/hobby2.png')}}">
							</a>
						</div>
					</div>					
				</div>
			</div>					
		</div>
		<div class="row two-category">			
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-12 ">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/food.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.instagram.com/granolavanjava/">
								<img src="{{URL::asset('img/promomarch/food1.png')}}">
							</a>
						</div>
					</div>					
				</div>
			</div>
			<div class="col-md-2">
			</div>
			<div class="col-md-5">			
				<div class="row">
					<div class="col-md-12">
						<div class="category1">
							<img src="{{URL::asset('img/promomarch/living.png')}}">
						</div>
					</div>
				</div>
				<div class="row row-lading">
					<div class="col-md-6 col-xs-6 .col-sm-6 two-landing">
						<div>
							<a href="https://www.facebook.com/tupperware.unique">
								<img src="{{URL::asset('img/promomarch/living1.png')}}">
							</a>
						</div>
					</div>					
				</div>
			</div>					
		</div>
	
		<div class="row">	
			<div class="col-md-12 title-loker">
				<img src="{{URL::asset('img/promomarch/loker-loc-title.png')}}"/>
			</div>					
		</div>
		
		<div class="row back-white drop-filter link-lokasi">	
			<div class="col-md-6 col-xs-12 .col-sm-12 tright">
				<div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select City <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" id="city" style="width:100%;">
                    	@foreach ($lockerBycities as $key => $value)
                        	<li>{{$key}}</li>
                        @endforeach
                    </ul>
                </div>
			</div>
			<div class="col-md-6 col-xs-12 .col-sm-12">
				<div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    	Choose Locker <span class="caret"></span>
                    </button>
                  	<ul class="dropdown-menu" id="loker" style="width:100%;">
                   	</ul>
                </div>
			</div>					
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="map_wrapper">
			        <div id="map_canvas" class="mapping"></div>
			    </div> 
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="sub-footer">
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="footer">
					<img src="{{URL::asset('img/promomarch/title-contactus.png')}}" class="img-title"/>
				</div>
			</div>
		</div>
		<div class="row row-footer">
			<div class="col-md-12 footer">
				<div class="tree-landing-footer">
					<img src="{{URL::asset('img/promomarch/footer1.png')}}"/>
				</div>
				<div class="tree-landing-footer">
					<img src="{{URL::asset('img/promomarch/footer2.png')}}" width="60%" />
				</div>
				<div class="tree-landing-footer">
					<img src="{{URL::asset('img/promomarch/footer3.png')}}" width="60%"/>
				</div>				
			</div>
		</div>	
		<div class="row row-footer link-contact">
			@if (count($errors) > 0)
                <div class="alert alert-danger">
                   <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                    
            @if (session('message'))
                <div class="alert alert-info">{{ session('message') }}</div>
            @endif
		</div>
		  <form method="POST" action="http://popbox.dev/submitpromomarch" accept-charset="UTF-8" class="form-horizontal">
                 {{ csrf_field() }}
				<div class="row row-footer">
					<div class="col-md-4 field-sort">
						<input class="form-control" placeholder="nama" name="nama" type="text">						
					</div>
					<div class="col-md-4 field-sort">
						<input class="form-control" placeholder="email" name="email" type="text">							
					</div>			
					<div class="col-md-4 field-sort">
						<input class="form-control" placeholder="phone" name="phone" type="text">							
					</div>			
				</div>	
				<div class="row row-footer">
					<div class="col-md-12">
						<input class="form-control" placeholder="subject" name="subject" type="text">								
					</div>			
				</div>		
				<div class="row row-footer">
					<div class="col-md-12">
						<textarea class="form-control" placeholder="message" name="message" cols="30" rows="5"></textarea>
						<!-- <textarea class="form-control" placeholder="message" name="message"></textarea> -->
					</div>			
				</div>		
				<!-- <div class="row row-footer">
					<div class="col-md-12">
                        <div class="g-recaptcha" data-sitekey="{{config('config.capcha.key')}}"></div>
					</div>                                 
                </div> -->
				<div class="row row-footer">
					<div class="col-md-12 col-btn-kirim-psn">
						<input type="submit" class="btn btn-kirim-psn" value="Kirim pesan">
					</div>			
				</div>	
				<div class="row row-socmed">
					<div class="col-md-4 center">	
						<a href="https://www.facebook.com/pboxasia">
							<img src="http://popbox.dev/img/promomarch/fb.png">		
						</a>
					</div>			
					<div class="col-md-4 center">				
						<a href="https://twitter.com/popbox_asia">
							<img src="http://popbox.dev/img/promomarch/tw.png">		
						</a>
					</div>			
					<div class="col-md-4 center">
						<a href="https://instagram.com/popbox_asia">
							<img src="http://popbox.dev/img/promomarch/ins.png">		
						</a>
					</div>			
				</div>		
		 </form>
	</div>
	
	<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBf2v8vfQazUaI2egWcoYvwK7ljsAPh6Uk&libraries=places&callback=initialize"async defer></script>
	<script type="text/javascript">
		var lockerList = {!! json_encode($lockerJkt) !!};
		var lockerByCities = new Array();
		<?php foreach ($lockerBycities as $key => $value): ?>
			    lockerByCities['<?php echo $key ?>']= <?php echo ($value); ?>;
		<?php endforeach ?>

		var arrLockers = new Array();
		<?php foreach ($lockers as $key => $value): ?>			
			    arrLockers['<?php echo $value->name ?>']= <?php echo (json_encode($value)); ?>;
		<?php endforeach ?>
		var lockername = "";
		var newLoc = false;

		$(document).ready(function(){
			$('#city li').on('click', function(){
		        $(this).parents(".btn-group").find('.btn').html($(this).text()+" <span class=\"caret\"></span>");
		        $("#loker li").parents(".btn-group").find('.btn').html("Pilih Loker <span class=\"caret\"></span>");
		        var  str_html ='';		        	       
		        $.each(lockerByCities[$(this).text()], function( index, value ){		        	
		            str_html +='<li>'+ value.name+'</li>';
		        });
		        $("#loker").parent().removeClass('hide');
        		$("#loker").html(str_html);
			});

			 $(document).on('click', '#loker li', function(){			 	
		        $(this).parents(".btn-group").find('.btn').html($(this).text()+" <span class=\"caret\"></span>");
		        lockername = $(this).text();
		        var obj = arrLockers[lockername];		        
				var lockerMarkers = new Array();
			    var lockerInfoWindowContent = new Array()
			    lockerInfoWindowContent.push(["'" + obj.address + "'"]);
			    lockerMarkers.push(["'" + obj.name + "'", obj.latitude, obj.longitude]);		       
			    test(lockerMarkers, lockerInfoWindowContent, false);		      
		    });

			
		});

		 function initialize() {
		    // digunakan untuk empty maps default lat lang
		    // var bounds = new google.maps.LatLngBounds(); 
		    // var myLatlng = {lat: -6.20, lng: 106.84};
		    // var map = new google.maps.Map(document.getElementById('map_canvas'), {
		    //   zoom: 11,
		    //   center: myLatlng
		    // });
		    
		    var markers =  new Array(); // or "var valueToPush = new Object();" which is the same
		    var infoWindowContent = new Array();    
		    <?php foreach ($lockers as $value) { ?>       
		       console.log("<?php echo $value->name; ?>");
		       var content = '<div><strong><?php echo $value->name; ?></strong></div><div><?php echo $value->address; ?></div>';
		       infoWindowContent.push(["'" + content + "'"]);
		       markers.push(["'<?php echo $value->name; ?>'", <?php echo $value->latitude; ?>, <?php echo $value->longitude; ?>]);      
		    <?php } ?>       
		    test(markers, infoWindowContent, true);   
 		}

 		function to_position(divid){
		     $('html, body').animate({scrollTop:$(divid).position().top - 50 }, 'slow');
		 }

		 function onShowAllFashion(){
		 	$(".more-fashion").show();		 	
		 	$(".fashion-all").hide();
		 	$(".fashion-less").show();
		 }

		 function onShowLessFashion(){
		 	$(".more-fashion").hide();		 	
		 	$(".fashion-all").show();
		 	$(".fashion-less").hide();
		 }

		 function onShowAllKids(){
		 	$(".more-kids").show();
		 	$(".kids-all").hide()
		 	$(".kids-less").show();
		 }

		 function onShowLessKids(){
		 	$(".more-kids").hide();
		 	$(".kids-all").show()
		 	$(".kids-less").hide();
		 }

 	

        function showPosition(position) {		    
		    var domain = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
		    var fulurl = domain + "/cnydeals?location=1";
		    $.ajax({
			  	method: "GET",
			  	url: fulurl,
			  	data: { long: position.coords.latitude, lat: position.coords.longitude }
			  	//data: { long: "101.602733", lat: "3.072519" }
			}).done(function( result ) {			
				var markers =  new Array(); // or "var valueToPush = new Object();" which is the same
		    	var infoWindowContent = new Array();    	
			    $.each(result.data, function( index, value ) {			    	
			    	var content = '<div><strong>' + value.name + '</strong></div><div>'+ value.address + '</div>';
		       		infoWindowContent.push(["'" + content + "'"]);
		       		markers.push(["'" + value.name + "'", value.latitude, value.longitude]);      
			    });
			    test(markers, infoWindowContent, true);   
			});
		}
  
  		function test(markers, infoWindowContent, isFirst){      
		    // Multiple Markers
		    var bounds = new google.maps.LatLngBounds();    
		    var mapOptions = {
		        mapTypeId: 'roadmap'
		    };
		                    
		    // Display a map on the page

		    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		    map.setTilt(45);
		        
		    // Display multiple markers on a map
		    var infoWindow = new google.maps.InfoWindow(), marker, i;
		    
		    // Loop through our array of markers & place each one on the map  
		   
		    var url_img = '{{ asset('img/map/marker-locker.png') }}';
		    var image = {
		      url: url_img,
		      origin: new google.maps.Point(0, 0),
		      anchor: new google.maps.Point(17, 34),
		      scaledSize: new google.maps.Size(20, 33)
		    };
		   
		    for( i = 0; i < markers.length; i++ ) {            
		        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);        
		        bounds.extend(position);
		        if (markers[i][0]=="current_location"){                  
		            marker = new google.maps.Marker({
		                position: position,
		                map: map,                
		                title: "Lokasi anda disini"
		            });      
		            infoWindow.setContent(infoWindowContent[i][0]);
		            infoWindow.open(map, marker);
		        }else{
		            marker = new google.maps.Marker({
		                position: position,
		                map: map,
		                icon: image,
		                title: markers[i][0]
		            });
		        }
		        
		        // Allow each marker to have an info window    
		        google.maps.event.addListener(marker, 'click', (function(marker, i) {
		            return function() {
		                infoWindow.setContent(infoWindowContent[i][0]);
		                infoWindow.open(map, marker);
		                console.log(infoWindowContent[i][0]);
		            }
		        })(marker, i));

		        // Automatically center the map fitting all markers on the screen
		        map.fitBounds(bounds);
		    }

		    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
		    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
		        if (isFirst){
		          this.setZoom(11);
		          this.setCenter(new google.maps.LatLng(-6.21, 106.81));
		        }else{
		          this.setZoom(13);
		        }
		        google.maps.event.removeListener(boundsListener);
		    });
		    
		}

		function onLink(link){
			window.location.href = link;
		}
	</script>	 

</body>
</html>
