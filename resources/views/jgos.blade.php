@extends('layout.main-article')
@section('content')
    <link rel="icon" type="image/png" href="img/favicon.ico">
    <style type="text/css">
    	@media only screen and (min-width:480px){
    		.merchant{
    			margin-right: 0px; 
    			width: 33.3333%; 
    		}
    	}
    	.card{
	        width: 100%;
	        min-height: 400px;
	        padding: 15px 15px;
	        border: 1px solid #DDDDDD;
	        border-radius: 10px;
	        margin-top: 30px;
	        text-align: center;
	        background: #fff;
	   }

	   .landing-page .section-popbox .card{
	        width: 100%;
	        min-height: 340px;
	        padding: 10px 0 0px 0;
	        border: 1px solid #DDDDDD;
	        border-radius: 0px;
	        margin-top: 30px;
	        text-align: center;
	        background: #FFFFFF;
	   }

	   .landing-page .section-benefit .card{
	        width: 100%;
	        min-height: 340px;
	        padding: 10px 0 0px 0;
	        border-radius: 0px;
	        margin-top: 30px;
	        text-align: center;
	   }

	   .card .icon i{
	        font-size: 63px;
	        color: #9a9a9a;
	        width: 85px;
	        height: 85px;
	        line-height: 80px;
	        text-align: center;
	        border-radius: 50%;
	        margin: 0 auto;
	        margin-top: 20px;
	   }

	    .card.card-blue{
	      border-color: #0C87CC;
	    }
	    .card.card-blue .icon i{
	      color:#00bbff;
	    }

	    .landing-page .section-popbox .card.card-white{
	      border-color: #FFFFFF;
	    }
	    .landing-page .section-popbox .card.card-blue{
	      border-color: #0C87CC;
	      background-color: #0C87CC;
	      color: #ffffff;
	    }
	    .card.card-white .icon i{
	      color:#00bbff;
	    }

	    .card.card-green{
	      border-color: #BDF8C0;
	    }
	    .card.card-green .icon i{
	      color:#3ABE41;
	    }
	    .card.card-orange{ 
	      border-color: #FAD9CD;
	    }
	    .card.card-orange .icon i{
	      color:#ff967f;
	    }
	     .card.card-red{ 
	      border-color: #FFBFC3;
	    }
	    .card.card-red .icon i{
	      color:#E01724;
	    }
	    .card  h4{
	        font-weight: 300;
	        font-size: 18px;
	        margin: 20px 0 15px;
	        color: #000;
	   }

	   .card  p{
	        font-size: 14px;
	        line-height: 22px;
	        font-weight: 400;
	        color: #000;
	   }
        .gMap-controls{
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 25px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            width: 30%;
        }

        .gMap-controls:focus{
            box-shadow: 0 0 5px rgba(12, 135, 204, 1);
        }

        #pac-input{
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
        }

    </style>
    <section id="content">
	    <div class="content-wrap">
			<div class="container clearfix">
				<div class="nobottommargin">
					<div class="col_full clearfix" align="center">
						<img src="{{ url('img/return/jgos.jpg') }}" class="img-responsive"><br><br>
						<h2>Syarat dan Ketentuan :</h2>
						<h4>
							*Promo berlaku mulai 24-30 Agustus 2018<br>
*Free Credit dapat di gunakan untuk pengguna baru yang download Aplikasi Popbox<br>
*Masukkan refferal code saat sign up/buat akun<br>
*Credit berlaku selama 180 hari (6 bulan) setelah promo di gunakan<br>
*Promo tidak dapat digabung dengan promo lainnya, dan tidak berlaku kelipatan <br>
						</h4>
					</div>
				</div>
			</div>
		</div>
    </section>
@stop

@section('js')
@stop
