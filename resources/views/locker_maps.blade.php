@extends('layout.main-locker')
@section('content')
    <div id="nearest_locker" style="height: 800px;" class="gmap"></div>
    <div id="selected_locker" style="display:none;"></div>
@stop

@section('js')
    <script type="text/javascript" src="https://hpneo.github.io/gmaps/gmaps.js"></script>
    <script type="text/javascript">
        var lockerList = {!! json_encode($lockerList) !!};
    </script>
	<script type="text/javascript">
        var map;
        $(document).ready(function(){

            @if(empty($city))
	     map = new GMaps({
                div: '#nearest_locker',
		lat: {{ $lat }},
  		lng: {{ $long }},
		zoom: {{ $zoom }},
		scrollwheel: false,
            });
	    
	    @else

	    map = new GMaps({
                div: '#nearest_locker',
		lat: {{ $lat }},
  		lng: {{ $long }},
		zoom: {{ $zoom }},
		scrollwheel: false,
            });

	    GMaps.geocode({
          	address: '{{ $city }}',
  		callback: function(results, status) {
    			if (status == 'OK') {
      				var latlng = results[0].geometry.location;
      				map.setCenter(latlng.lat(), latlng.lng());
    			}
  		}
       	    });

	    @endif


	   var markers = Array();

            $.each(lockerList, function (key, value) {
                var html = '<div align="center">';
                html += '<strong style="color:#FF6300;font-size:15px;">' + value.name + '</strong><br>';
                html += '<strong>' + value.address + '</strong><br>';
                html += value.address_2 + '<br>';
                html += value.operational_hours;
                html += '</div>';
                var iconMarker = '{{ asset('img/map/marker-locker.png') }}';

                var locker = {
                    lat: value.latitude,
                    lng: value.longitude,
		    icon: iconMarker,
                    infoWindow: {
  			content: html
		    },                    
		    click: function(e){
			console.log(value.name);
			$('#selected_locker').html(value.name);                        
                    }
                };
		map.addMarker(locker);


            });

        });
	</script>
@stop
