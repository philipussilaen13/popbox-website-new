<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta charset="utf-8">
    <?php
        if (Route::getCurrentRoute()->getName()!=null){
            if (Route::getCurrentRoute()->getName()=="promopulsa"){ ?>
                <meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
                <meta property="og:type" content="article" />
                <meta property="og:title" content="Promo Pulsa By PopBox" />
                <meta property="og:description" content="Isi pulsa mudah tanpa biaya admin via loker PopBox! Gaperlu khawatir kehabisan pulsa. Cukup datang ke loker PopBox terdekatmu dan tap e-money mu!" />
                <meta property="og:image" content="{{ URL::asset('img/promopulsa/banner.png')}}" />

            <?php } else if (Route::getCurrentRoute()->getName()=="ramadhan"){ ?>
                <meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
                <meta property="og:type" content="article" />
                <meta property="og:title" content="Promo Ramadhan By PopBox" />
                <meta property="og:description" content="Menyambut bulan Ramadhan. PopBox bagi-bagi kejutan! FREE ONGKIR sejabodetabek selama periode." />
                <meta property="og:image" content="{{ URL::asset('img/ramadhan/banner.png')}}" />

            <?php }
        }
    ?>
    <title>PopBox - Parcel Locker Indonesia {{Route::getCurrentRoute()->getName()}}</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia Team" />
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('img/favico.ico') }}">
    <link rel="icon" type="image/png" href="img/favicon.ico">
    <!-- Stylesheets
	============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/dark.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/flag-icon.min.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M48GKGL');</script>
<!-- End Google Tag Manager -->

     <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview');
        </script>
    @yield('meta')
    @yield('css')
</head>

<body class="stretched">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M48GKGL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">
            <div id="header-wrap">
                <div class="container clearfix">
                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="{{ url('/') }}" class="standard-logo" data-dark-logo="{{ asset('img/logo/logo-dark.png') }}"><img src="{{ asset('img/logo/logo.png') }}" alt="PopBOX Logo"></a>
                        <a href="{{ url('/') }}" class="retina-logo" data-dark-logo="{{ asset('img/logo/logo-dark@2x.png') }}"><img src="{{ asset('img/logo/logo@2x.png') }}" alt="PopBOX Logo"></a>
                    </div>
                    <!-- #logo end -->
                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">
                    	<ul>
                            <li>
                                <a href="#" data-href="#section-nextgen">
                                    @php 
                                        $locale = \Session::get('app.region','id');
                                    @endphp
                                    <span class="flag-icon flag-icon-{{ $locale }} flag-icon-squared" style="display: inline-flex"></span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('language') }}/id" data-href="#section-nextgen">
                                            <div><span class="flag-icon flag-icon-id flag-icon-squared" style="display: inline-flex;"></span> Indonesia</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('language') }}/my" data-href="#section-stunning-graphics">
                                            <div><span class="flag-icon flag-icon-my flag-icon-squared" style="display: inline-flex;"></span> Malaysia</div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                    		{{-- <li>
                    			<a href="{{ url('career') }}">
                    				<div>{{ trans('layout.career-url') }}</div>
                    			</a>
                    		</li> --}}
                    		<li>
                    			<a href="{{ url('contact') }}">
                    				<div>{{ trans('layout.contact-url') }}</div>
                    			</a>
                    		</li>
                            @if(\Auth::check())
                                <li>
                                    <a href="/user/logout">
                                        <div>logout</div>
                                    </a>
                                </li>
                            @endif
                    	</ul>
                    </nav>
                    <!-- #primary-menu end -->
                </div>
            </div>
        </header>
        <!-- #header end -->
        @yield('content')
        
        @include('layout.footer')
    </div>
    <!-- #wrapper end -->
    <!-- Go To Top
	============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>
    <!-- External JavaScripts
	============================================= -->
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
    <!-- Footer Scripts
	============================================= -->
    <script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools --> 
    {{-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5823e82fc2a9cb90"></script>  --}}
    <script type="text/javascript" src="{{ elixir('js/validation.js') }}"></script>
    @yield('js')
</body>

</html>
