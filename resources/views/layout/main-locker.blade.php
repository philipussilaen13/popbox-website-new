<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>PopBox</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="PopBox Asia Team" />
        <meta charset="utf-8" />
    </head>
    <body style="margin:0;">
            @yield('content')
            <script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijkBYFlPmwsLEAfvljffdrnhM7EStYF4&libraries=places"></script>
            @yield('js')
    </body>
</html>

