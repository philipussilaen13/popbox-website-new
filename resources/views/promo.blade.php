@extends('layout.main-article')
@section('content')
<section id="slider" class="boxed-slider">
    <div class="container clearfix">
        <div class="fslider" data-easing="easeInQuad">
            <div class="flexslider">
                <div class="slider-wrap">
                    @foreach ($activePromo as $promo)
                    <div class="slide" data-thumb="images/slider/boxed/thumbs/2.jpg">
                        <a href="{{ url('article') }}/{{ date('Ymd',strtotime($promo->created_date)) }}/{{ $promo->short_title }}/{{ $promo->id_article }}">
                            <img src="{{\App\Http\Helper\Helper::createImgLocal('article',$promo->image)}}" alt="Slide 2">
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <h3>Active Promo</h3>
            <hr>
            <div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">
                 @if (!empty($activePromo))
                    @foreach ($activePromo as $article)
                        <div class="entry clearfix">
                            <div class="entry-image">
                                <a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}" target="_blank"><img class="image_fade" src="{{\App\Http\Helper\Helper::createImgLocal('article',$article->image)}}" alt="Loker PopBox di PRI"></a>
                            </div>
                            <div class="entry-title">
                                <h2><a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}" target="_blank">{{ $article->title }}</a></h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i>{{ date('j M Y',strtotime($article->created_date)) }}</li>
                                <li><a href="#"><i class="icon-comments"></i>{{ ucfirst($article->type) }}</a></li>
                                <li><a href="#"><i class="icon-film"></i></a></li>
                            </ul>
                            <div class="entry-content">
                                <a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}" class="more-link">Read More</a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <h3>Archive</h3>
            <hr>
            <div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">
                 @if (!empty($pastPromo))
                    @foreach ($pastPromo as $article)
                        <div class="entry clearfix">
                            <div class="entry-image">
                                <a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}" target="_blank"><img class="image_fade" src="{{\App\Http\Helper\Helper::createImgLocal('article',$article->image)}}" alt="Loker PopBox di PRI"></a>
                            </div>
                            <div class="entry-title">
                                <h2><a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}" target="_blank">{{ $article->title }}</a></h2>
                            </div>
                            <ul class="entry-meta clearfix">
                                <li><i class="icon-calendar3"></i>{{ date('j M Y',strtotime($article->created_date)) }}</li>
                                <li><a href="#"><i class="icon-comments"></i>{{ ucfirst($article->type) }}</a></li>
                                <li><a href="#"><i class="icon-film"></i></a></li>
                            </ul>
                            <div class="entry-content">
                                <a href="{{ url('article') }}/{{ date('Ymd',strtotime($article->created_date)) }}/{{ $article->short_title }}/{{ $article->id_article }}" class="more-link">Read More</a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div style="background-color: #B6E9E0; margin: 30px 0; padding: 30px; width: 100%;">
            <div class="heading-block center">
                <h3>{{ trans('promo.subscribe') }} <span>{{ trans('promo.subscribe2') }}</span></h3>
            </div>
            <div class="subscribe-widget">
                <div class="widget-subscribe-form-result"></div>
                <form id="widget-subscribe-form2" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                    <div class="input-group input-group-lg divcenter" style="max-width:600px;">
                        <span class="input-group-addon"><i class="icon-email2"></i></span>
                        <input type="email" name="widget-subscribe-form-email" class="form-control required email" placeholder="{{ trans('promo.input') }}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">{{ trans('promo.subscribe-btn') }}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop