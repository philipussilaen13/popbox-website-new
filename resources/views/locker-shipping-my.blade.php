<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>PopBox - Locker Shipping</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia Team" />
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    {{-- Standard --}}
    <meta name="description" content="Get your online purchase shipped to a locker near you today! Collection at your convenience 24/7">
    <meta name="keywords" content="PopBox,Parcel,Locker,Loker,Kirim Barang,Logistik,Logistic">
    {{-- Schema.org for G+ --}}
<!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="PopBox Asia">
    <meta itemprop="description" content="Get your online purchase shipped to a locker near you today! Collection at your convenience 24/7">
    <meta itemprop="image" content="{{ asset('img/metaimage-popbox.jpg') }}">
    {{-- Twitter Card data --}}
    <meta name="twitter:card" content="PopBox is an automated parcel locker that allows you to send, receive, and return a parcel conveniently">
    <meta name="twitter:site" content="@PopBox_Asia">
    <meta name="twitter:title" content="PopBox - Parcel Collection Made Easy">
    <meta name="twitter:description" content="Get your online purchase shipped to a locker near you today! Collection at your convenience 24/7">
    <meta name="twitter:creator" content="@PopBox_Asia">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset('img/metaimage-popbox.jpg') }}">
    {{-- OpenGraph --}}
    <meta property="og:title" content="PopBox - Parcel Collection Made Easy">
    <meta property="og:image" content="{{ asset('img/metaimage-popbox.jpg') }}">
    <meta property="og:description" content="Get your online purchase shipped to a locker near you today! Collection at your convenience 24/7">
    <meta property="og:url" content="{{ url('/') }}" />

    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}">

    <style type="text/css">
        .step{
            color: #ef5448
        }
        .list-group.vertical-steps{
            padding-left:10px;
        }
        .list-group.vertical-steps .list-group-item{
            border:none;
            border-left:3px solid #ece5dd;
            box-sizing:border-box;
            border-radius:0;
            counter-increment: step-counter;
            padding-left:20px;
            padding-right:0px;
            padding-bottom:20px;
            padding-top:0px;
        }
        .list-group.vertical-steps .list-group-item.active{
            background-color:transparent;
            color:inherit;
        }
        .list-group.vertical-steps .list-group-item:last-child{
            border-left:3px solid transparent;
            padding-bottom:0;
        }
        .list-group.vertical-steps .list-group-item::before {
            border-radius: 50%;
            background-color:#ece5dd;
            color:#555;
            content: counter(step-counter);
            display:inline-block;
            float:left;
            height:25px;
            line-height:25px;
            margin-left:-35px;
            text-align:center;
            width:25px;
        }
        .list-group.vertical-steps .list-group-item span,
        .list-group.vertical-steps .list-group-item a{
            display:block;
            overflow:hidden;
            padding-top:2px;
        }
        /*Active/ Completed States*/
        .list-group.vertical-steps .list-group-item.active::before{
            background-color:#0052c2;
            color:#fff;
        }
        .list-group.vertical-steps .list-group-item.completed{
            border-left:3px solid #0052c2;
        }
        .list-group.vertical-steps .list-group-item.completed::before{
            background-color:#0052c2;
            color:#fff;
        }
        .list-group.vertical-steps .list-group-item.completed:last-child{
            border-left:3px solid transparent;
        }
    </style>
</head>
<body>
<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="center">
                    <img src="{{ asset('img/logo-PopBox-my.png') }}" style="width: 50%">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3 align="center">
                        Pick your own parcel via PopBox Locker. <br> You will get PIN Code when your parcel has been arrived.
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ol class="list-group vertical-steps">
                <li class="list-group-item">
                    <h4><span class="step">Choose nearest parcel</span></h4>
                    <div class="row">
                        <form id="availabilityForm">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h4> </h4>
                                <select class="form-control" name="cityName">
                                    <option value="">Select City</option>
                                    @foreach ($citiesList as $element)
                                        <option value="{{ $element }}">{{ $element }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h4> </h4>
                                <select class="form-control" name="lockerName">
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="row hide" id="boxAlamat">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h4>Address</h4>
                            <textarea class="form-control" rows="5" readonly="true" id="textAlamat"></textarea>
                            <br>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h4>Operational Hours<span id="operational_hours"></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <h4><span class="step">Copy and paste the locker address to shipping address field and continue the transaction</span></h4>
                    <button class="btn btn-info clip hide" type="button" data-clipboard-target="#textAlamat" id="salinBtn">Copy Address</button>
                </li>
                <li class="list-group-item">
                    <h4>
                        <span class="step">When the parcel arrived</span>
                    </h4>
                    <span>Go to locker and take the parcel via "Parcel Pick Up" menu</span>
                </li>
            </ol>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/clipboard.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        // initialize clipboard
        new Clipboard('.clip');

        // city on change
        $('select[name=cityName]').on('change', function(event) {
            /* Act on the event */
            $('#errorMsg').addClass('hide');
            $('select[name=lockerName]').html('');
            $.ajax({
                url: '{{ url('lockerList-my') }}',
                type: 'POST',
                dataType: 'json',
                data: $('#availabilityForm').serialize(),
                success: function(data){
                    if (data.isSuccess==true) {
                        $('#contactform').find('.form-process').fadeOut();
                        var lockerList = data.data;
                        var option = '';
                        $.each(lockerList, function(index, val) {
                            /* iterate through array or object */
                            var address = val.address;
                            var address_2 = val.address_2;
                            var district = val.district+', '+val.province;
                            var operational_hours = val.operational_hours;
                            var zip_code = val.zip_code;
                            option += "<option value='"+val.name+"' address='"+address+"' address_2='"+address_2+"' district='"+district+"' operate='"+operational_hours+"' zip='"+zip_code+"'>"+val.name+"</option>"
                        });
                        $('select[name=lockerName]').html(option);
                        $('#lockerDiv').removeClass('hide');
                        $('select[name=lockerName]').trigger('change');
                    } else {
                        $('#contactform').find('.form-process').fadeOut();
                        $('#errorMsg').html(data.errorMsg);
                        $('#errorMsg').removeClass('hide');
                    }
                }
            })
                .done(function() {
                    $('#availabilityForm').find('.form-process').fadeOut();
                    console.log("success");
                })
                .fail(function() {
                    $('#availabilityForm').find('.form-process').fadeOut();
                    console.log("error");
                })
                .always(function() {
                    $('#availabilityForm').find('.form-process').fadeOut();
                    console.log("complete");
                });
        });

        //locker on change
        $('select[name=lockerName]').on('change', function(event) {
            /* Act on the event */
            $('#boxAlamat').addClass('hide');
            $('#salinBtn').addClass('hide');

            var locker = $(this).find(':selected');
            var address = locker.attr('address');
            var address_2 = locker.attr('address_2');
            var district = locker.attr('district');
            var operate = locker.attr('operate');
            var name = locker.val();
            var zip_code = locker.attr('zip');

            // create textarea value
            var textarea = name+"\n"+address+"\n"+address_2 ;
            $('#textAlamat').val(textarea);
            $('#operational_hours').html(operate);

            $('#boxAlamat').removeClass('hide');
            $('#salinBtn').removeClass('hide');
        });
    });
</script>
</body>
</html>