		<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hi Admin, </p>
		<p> New Merchant Pickup Request from {{ $data->merchant_name}} on {{ date("Y-m-d H:i:s") }}   </p>
		<p>			
			Merchant info :<br/>
		
			Owner Name : {{ $data->owner_name }}<br />
			Contact No : {{ $data->phone }}<br />
			Pickup Time : {{ $data->time_pickup }}<br /><br />

			Order info :<br/>
			Order No : {{ $pickup["order_number"] }} <br />			
			Merchant Name : {{ $data->merchant_name }}<br />
			Pickup Location : {{ $pickup["pickup_location"] }}<br />
			Popbox Location : {{ $pickup["popbox_location"] }} <br/>
			Customer phone :  {{ $pickup["customer_phone"] }} <br/>
			Customer email :   {{ $pickup["customer_email"] }} <br/>
		</p>		
	</body>
</html>