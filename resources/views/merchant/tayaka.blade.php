<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia" />
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.ico') }}">

     {{-- Standard --}}
    <meta name="description" content="Layanan baru Tayaka, Anda dapat meletakkan dan mengambil laundri di loker PopBox. Cuci setrika kiloan, bagi Anda yang penuh kesibukan, sering berpergian, ataupun perlu mencuci dalam kuantitas banyak.">
    <meta name="keywords" content="PopBox,Parcel,Locker,Loker,Kirim Barang,Logistik,Logistic">
    {{-- Schema.org for G+ --}}
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="PopBox On Demand - Tayaka">
    <meta itemprop="description" content="Layanan baru Tayaka, Anda dapat meletakkan dan mengambil laundri di loker PopBox. Cuci setrika kiloan, bagi Anda yang penuh kesibukan, sering berpergian, ataupun perlu mencuci dalam kuantitas banyak.">
    <meta itemprop="image" content="{{ asset('img/tayaka/tayaka-meta-data.png') }}">
    {{-- Twitter Card data --}}
    <meta name="twitter:card" content="Layanan baru Tayaka, Anda dapat meletakkan dan mengambil laundri di loker PopBox. Cuci setrika kiloan, bagi Anda yang penuh kesibukan, sering berpergian, ataupun perlu mencuci dalam kuantitas banyak.">
    <meta name="twitter:site" content="@PopBox_Asia">
    <meta name="twitter:title" content="PopBox On Demand - Tayaka">
    <meta name="twitter:description" content="Layanan baru Tayaka, Anda dapat meletakkan dan mengambil laundri di loker PopBox. Cuci setrika kiloan, bagi Anda yang penuh kesibukan, sering berpergian, ataupun perlu mencuci dalam kuantitas banyak.">
    <meta name="twitter:creator" content="@PopBox_Asia">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset('img/tayaka/tayaka-meta-data.png') }}">
    {{-- OpenGraph --}}
    <meta property="og:title" content="PopBox On Demand - Tayaka">
    <meta property="og:image" content="{{ asset('img/tayaka/tayaka-meta-data.png') }}">
    <meta property="og:description" content="Layanan baru Tayaka, Anda dapat meletakkan dan mengambil laundri di loker PopBox. Cuci setrika kiloan, bagi Anda yang penuh kesibukan, sering berpergian, ataupun perlu mencuci dalam kuantitas banyak.">
    <meta property="og:url" content="{{ url('tayaka') }}" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/dark.css') }}">
    <!-- Agency Demo Specific Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/demos/agency/agency.css') }}" type="text/css" />
    <!-- / -->
    <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/animate.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/colors.php?color=67B4DB" type="text/css" />
    <!-- Document Title
    ============================================= -->
    <title>Tayaka | PopBox</title>
</head>

<body class="stretched">
    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
        <header id="header" class="sticky-style-2">
            <div class="container clearfix">
                <!-- Logo
                ============================================= -->
                <div id="logo" class="divcenter">
                    <div class="row" width="100%">
                        <!--  <a href="http://popbox.asia" class="standard-logo"><img class="divcenter" src="img/logo-header.png" alt="Canvas Logo">
                        </a> -->
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <a href="http://popbox.asia"><img src="{{ asset('img/tayaka/logo-popbox.png') }}" alt="PopBox Logo">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="http://www.tayakalaundry.com/"><img src="{{ asset('img/tayaka/logo-tayaka.png') }}" alt="Tayaka Logo"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- <a href="index-agency.html" class="retina-logo"><img class="divcenter" src="img/logo-header@2x.png" alt="Canvas Logo"></a> -->
                </div>
                <!-- #logo end -->
            </div>
            <div id="header-wrap">
                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu" class="style-2 center" style="background-color: #67B4DB">
                    <div class="container clearfix">
                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                        <ul>
                            <li>
                                <a href="#">
                                    <div>Beranda</div>
                                </a>
                            </li>
                            <li>
                                <a href="#howitworks">
                                    <div>Cara</div>
                                </a>
                            </li>
                            <li>
                                <a href="#layanan">
                                    <div>Layanan</div>
                                </a>
                            </li>
                            <li>
                                <a href="#content">
                                    <div>Order</div>
                                </a>
                            </li>
                            <li>
                                <a href="#contactus">
                                    <div>Hubungi Kami</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- #primary-menu end -->
            </div>
        </header>
        <!-- #header end -->
        <section id="slider" class="boxed-slider">
            <div class="container clearfix">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-items="1" data-loop="true" data-nav="true" data-autoplay="5000" data-animate-in="fadeIn" data-animate-out="fadeOut" data-speed="800">
                    <a href="#"><img src="{{ asset('img/tayaka/slider01.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/tayaka/slider02.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/tayaka/slider03.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/tayaka/slider04.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/tayaka/slider05.jpg') }}" alt="Slider"></a>
                </div>
                <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
            </div>
        </section>
        <!-- Content
        ============================================= -->
        <section id="howitworks" >
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h3>Telah Hadir Layanan Tayaka & PopBox</h3>
                        <span>
                        Letakkan dan ambil laundry Anda di loker PopBox.<br>
                        <strong>Bagaimana caranya?</strong>

                        </span>
                    </div>
                    <div class="col_half">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <a class="i-circled i-bordered i-alt divcenter">1</a>
                            <h3>MASUKKAN DATA PESANAN<span class="subtitle">Lengkapi data pesanan Anda <span><a href="#layanan">di sini</a>. <br>Anda akan mendapatkan <strong>SMS notifikasi</strong> pemesanan yang berisi nomor order untuk membuka loker.</span></h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>

                            <a class="i-circled i-bordered i-alt divcenter">2</a>
                            <h3>LETAKKAN LAUNDRY<span class="subtitle">
                            Datang ke loker PopBox, pilih menu <strong>"LAUNDRY"</strong>, kemudian masukkan nomor order. Letakkan laundry ke loker dan tutup pintu loker.</span></h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>

                        </div>
                    </div>

                    <div class="col_half col_last">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">

                            <a class="i-circled i-bordered i-alt divcenter">3</a>
                            <h3>PROSES LAUNDRY
                                <span class="subtitle">Kurir Tayaka akan mengambil laundry Anda dan melakukan proses pencucian. Proses pencucian maksimal 3 hari kerja.</span>
                                <span class="subtitle"><strong>Untuk pembayaran akan dihubungi oleh Tayaka Laundry</strong></span>
                            </h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>

                             <a class="i-circled i-bordered i-alt divcenter">4</a>
                            <h3>PENGAMBILAN LAUNDRY<span class="subtitle">Kami akan mengirimkan notifikasi setelah laundry Anda selesai. Silakan datang ke loker dan buka menu <strong>"MENGAMBIL BARANG"</strong>, kemudian masukkan PIN, dan ambil pakaian Anda.</span></h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                   
                    <div class="divider divider-center nobottommargin"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- CONTENT
        ============================================= -->
        <section id="layanan">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="container center clearfix">
                        <div class="heading-block center">
                            <h3>CUCI SETRIKA KILOAN <span>Tayaka</span></h3>
                        </div>
                    </div>
                    <div class="col_full">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <h3><span class="subtitle">CUCI SETRIKA KILOAN
                            Bagi Anda yang penuh kesibukan, sering berpergian, ataupun perlu mencuci dalam kuantitas banyak. <br>TAYAKA menyediakan <strong>Jasa Cuci Setrika Kiloan</strong>. <br><br>Meski Kiloan, Anda dapat meminta untuk dilakukan pemisahan pakaian bilamana ada pakaian Anda yang berisiko terhadap kelunturan atau kerusakan bahan dengan melakukan penandaan pada pakaian Anda sehingga keamanan pakaian Anda lebih terjaga (tanpa biaya tambahan).</span></h3>
                            <h2>Harga: <span>Rp8.500/kg</span></h2>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="container clearfix">
                <div class="postcontent nobottommargin divcenter">
                    <h3 class="center">Masukkan Pesanan</h3>
                    <div class="contact-widget">
                        <div class="contact-form-result"></div>
                       <form class="nobottommargin" id="formSubmit" name="formSubmit">
                            {{ csrf_field() }}
                            <div class="form-process"></div>
                            <div class="col_one_third">
                                <label for="template-contactform-name">Nama <small>*</small></label>
                                <input type="text" id="cust_name" name="cust_name" value="" class="sm-form-control required" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-email">Email <small>*</small></label>
                                <input type="email" id="cust_email" name="cust_email" value="" class="required email sm-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="template-contactform-phone">No. Telp</label>
                                <input type="text" id="cust_phone" name="cust_phone" value="" class="sm-form-control" />
                            </div>
                            <div class="clear"></div>
                            <div>
                                <label for="template-contactform-service">Pilih Layanan</label>
                                <select id="service" name="service" class="sm-form-control" style="height:38px">
                                    <option value="drop-pick">Drop dan Pick up di loker PopBox</option>
                                    <option value="drop">Drop di loker PopBox - Antar ke Rumah</option>
                                    <option value="pick">Antar ke Tayaka - Pick up di Loker PopBox</option>
                                                       
                                </select>
                                <div class="clear"></div>
                            </div>

                            <div>
                                <label for="template-contactform-service">Pilih Loker PopBox</label>
                                <select id="locker" name="locker" class="sm-form-control" style="height: 38px">
                                    @foreach ($lockerJkt as $element)
                                        <option value="{{ $element['name'] }}">{{ $element['name'] }}</option>
                                    @endforeach
                                </select>

                            </div>
                             <div>
                                <div class="clear"></div>
                                <label for="template-contactform-message"><a href="https://location.popbox.asia" target="_blank"> LIHAT LOKASI TERDEKAT DI SINI</a></label>
                            </div>
                            <div class="clear"></div> <br>
                            <div class="col_three_third">
                                <label>Address</label>
                                <textarea id="locker_address" class="sm-form-control" rows="3" disabled>
                                    
                                </textarea>
                            </div>
                            <div class="clear"></div>
                            <div class="col_full hidden" id="div-address">
                                <label for="template-contactform-message">Alamat Pengiriman<small>*</small></label>
                                <textarea class="sm-form-control" id="address" name="address" rows="6" cols="30"></textarea>
                            </div>
                            <div>
                                <p>
                                    <h4>Syarat dan Ketentuan:</h4> - Biaya pembatalan dikenakan Rp6.000,
                                    <br> - Minimal laundry 4 kg, kurang dari 4kg tetap akan dihitung 4kg
                                    <br> - Pembayaran harus sudah dilunasi sebelum laundry bersih di pick up
                                    <br> - Jika baju customer luntur atau rusak karena sifat bahan dan tidak diinfokan sebelumnya akan menjadi tanggung jawab customer.
                                    <br> - Estimasi waktu laundry maksimal adalah 3 hari kerja
                                    <br>
                                </p>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="snk" value="snk"> &nbsp; Anda setuju dengan syarat dan ketentuan yang berlaku.
                            </div>
                            <div style="margin-top: 25px;">
                                <button class="button button-3d divcenter" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
        </section>
        <!-- CONTENT
        ============================================= -->
        <!-- Content
        ============================================= -->
        <section id="contactus" class="divcenter">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="clear"></div>
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                    <div class="heading-block center">
                        <h3>Hubungi Kami</h3>
                        <span>Untuk pertanyaan, saran, serta info dari kami, silakan hubungi tim layanan pelanggan kami </span>
                    </div>
                    <div class="col_half center">
                        <h5>TAYAKA</h5> Jl. Mekar Utama No. 26
                        <br>Mekar Wangi - Bandung,
                        <br>50237
                        <br><a href="http://www.tayakalaundry.com/">http://www.tayakalaundry.com/</a>
                        <br>
                        <br>0225234047
                        <br><a href="mailto:tayaka.laundry@gmail.com">info@tayakalaundry.com</a>
                    </div>
                    <div class="col_half col_last center">
                        <h5>PT POPBOX ASIA SERVICES</h5> Grand Slipi Tower Unit 21J
                        <br> Jl. S. Parman kav 22-24
                        <br> Jakarta 11480
                        <br> <a href="www.popbox.asia">www.popbox.asia</a>
                        <br>
                        <br>+62 21 2902 2537/8
                        <br><a href="mailto:info@popbox.asia">info@popbox.asia</a>
                    </div>
                    <!-- Postcontent
                    ============================================= -->
                    <div class="postcontent nobottommargin divcenter">
                        <div class="contact-widget">
                            <div class="contact-form-result"></div>
                            <div class="clear"></div>
                            <form class="nobottommargin" id="contactform" name="contactform">
                                <div class="form-process"></div>
                                <div class="col_one_third">
                                    <label for="template-contactform-name">Name<small>*</small></label>
                                    <input type="text" id="name" name="name" value="" class="sm-form-control required" />
                                </div>
                                <div class="col_one_third">
                                    <label for="template-contactform-email">Email<small>*</small></label>
                                    <input type="email" id="email" name="email" value="" class="required email sm-form-control" />
                                </div>
                                <div class="col_one_third col_last">
                                    <label for="template-contactform-phone">Phone Number<small>*</small></label>
                                    <input type="text" id="phone" name="phone" value="" class="sm-form-control required" />
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="template-contactform-subject">Subject<small>*</small></label>
                                    <input type="text" id="subject" name="subject" value="" class="required sm-form-control" />
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="template-contactform-message">Message<small>*</small></label>
                                    <textarea class="required sm-form-control" id="message" name="message" rows="4" cols="30"></textarea>
                                </div>
                                <div class="col_full">
                                     <div class="g-recaptcha" data-sitekey="6LerZAsUAAAAAKGr1poO2iTXLlvM_tE6OJYmlle4" style="display: inline-block;"></div>
                                </div>
                                <div class="col_last">
                                    <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- .postcontent end -->
                </div>
            </div>
        </section>
        <div class="modal fade" id="modalSubscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Contact</h4>
                    </div>
                    <div class="modal-body">
                       Thanks for your feedback. We will contact you soon.
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalSubmit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Tayaka Order</h4>
                    </div>
                    <div class="modal-body">
                       Success order 
                    </div>
                </div>
            </div>
        </div>
        <!-- #content end -->
        <!-- Footer
        ============================================= -->
        <footer id="footer">
            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">
                <div class="container clearfix">
                    <div class="col_half">
                        Copyrights &copy; 2017 All Rights Reserved by PopBox Asia Services.
                    </div>
                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="https://www.facebook.com/pboxasia" class="social-icon si-small si-light si-rounded si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="https://twitter.com/popbox_asia" class="social-icon si-small si-light si-rounded si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/popbox_asia/" class="social-icon si-small si-light si-rounded si-instagram">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <!-- #copyrights end -->
        </footer>
        <!-- #footer end -->
        <!-- #wrapper end -->
        <div id="gotoTop" class="icon-angle-up"></div>
        <!-- External JavaScripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!-- Footer Scripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/validation.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $("#processTabs").tabs({
                    show: {
                        effect: "fade",
                        duration: 400
                    }
                });
                $(".tab-linker").click(function() {
                    $("#processTabs").tabs("option", "active", $(this).attr('rel') - 1);
                    return false;
                });
            });
        </script>
        <script type="text/javascript">
            var lockerList = {!! json_encode($lockerJkt) !!};
            $(document).ready(function() {
                // select service on change
                $('select[name=service]').on('change', function(event) {
                    //get value
                    var value = $(this).val();
                    if (value=='drop') {
                        $('#div-address').removeClass('hidden');
                    } else $('#div-address').addClass('hidden');
                });

                $('select[name=locker]').on('change', function(event) {
                    var name = $(this).val();
                    var selectedLocker = null;
                    for (var i in lockerList) {
                        if(lockerList[i].name==name){
                            selectedLocker = lockerList[i];
                        } 
                    }
                    if (selectedLocker!= null) {
                        var address = selectedLocker.address;
                        address += "\n"+selectedLocker.address_2;
                        address += "\n"+selectedLocker.operational_hours;

                        $('textarea[id=locker_address]').val(address);
                    }
                });
                $('select[name=locker]').trigger('change');

                // form omaisu submit
                $('#formSubmit').on('submit', function(event) {
                    event.preventDefault();
                    var form = $(this).serialize();
                    var rules = Array();
                    rules['cust_name'] = 'required|min:4';
                    rules['cust_email'] = 'required|min:4|email';
                    rules['cust_phone'] = 'required|min:4|numeric';
                    rules['services'] = 'required';
                    rules['locker'] = 'required';
                    
                    var service = $('select[name=service]').val();
                    if (service=='drop') {
                        rules['address'] = 'required';
                    }

                    var isValid = validate($(this),rules);
                    if (isValid==false) {
                         return;
                    }
                    $.ajax({
                        url: "{{ url('tayaka') }}",
                        data: $('#formSubmit').serialize(),
                        dataType:'json',
                        type: 'post',
                        async: true,
                        success: function(data){
                            if (data.isSuccess==true) {
                                // Show Modal
                                //hideLoading('#subscribe', 'load');
                                $('#formSubmit').find('.form-process').fadeOut();
                                $('#modalSubmit ').modal('show');
                                setTimeout(function() {
                                    location.reload();
                                }, 3000);
                            } else {
                                $('#formSubmit').find('.form-process').fadeOut();
                                //hideLoading('#subscribe', 'load');
                                alert(data.errorMsg)
                            }
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            $('#formSubmit').find('.form-process').fadeOut();
                        }
                    }).fail(function(){
                        //hideLoading('#subscribe', 'load');
                        $('#formSubmit').find('.form-process').fadeOut();
                        alert('Failed To Submit');
                        return;
                    });
                });

                // form contact submit
                $('#contactform').submit(function(event) {
                    event.preventDefault();
                    var rules = Array();
                    rules['name'] = 'required';
                    rules['email'] = 'required|email';
                    rules['message'] = 'required';
                    var isValid = validate($(this),rules);
                    if (isValid==false) {
                        return;
                    }
                    $.ajax({
                        url: "{{ url('contact') }}",
                        data: $('#contactform').serialize(),
                        dataType:'json',
                        type: 'post',
                        async: true,
                        success: function(data){
                            if (data.isSuccess==true) {
                                // Show Modal
                                //hideLoading('#subscribe', 'load');
                                $('#contactform').find('.form-process').fadeOut();
                                $('#modalSubscribe').modal('show');
                                $('input[name="email"]').val('');
                                $('input[name="name"]').val('');
                                $('input[name="message"]').val('');
                                $('input[name="subject"]').val('');

                            } else {
                                $('#contactform').find('.form-process').fadeOut();
                                //hideLoading('#subscribe', 'load');
                                alert(data.errorMsg)
                            }
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            $('#contactform').find('.form-process').fadeOut();
                        }
                    }).fail(function(){
                        //hideLoading('#subscribe', 'load');
                        $('#contactform').find('.form-process').fadeOut();
                        alert('Failed To Submit');
                        return;
                    });
                });
            });
        </script>
</body>

</html>
