<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="PopBox Asia">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.ico') }}">
    <meta name="description" content="PopBox CSR Neubodi">
    <meta name="keywords" content="PopBox,Parcel,Locker,Loker,Kirim Barang,Logistik,Logistic, neubodi, csr popbox">
    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/dark.css') }}">
    <!-- Agency Demo Specific Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/demos/agency/agency.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/animate.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" >
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/colors.php?color=00A3C7" type="text/css">
    <!-- Document Title
    ============================================= -->
    <title>Sunway PopBox - Neubodi</title>
</head>

<body class="stretched">
    <!-- Document Wrapper ============================================= -->
    <div id="wrapper" class="clearfix">
        <section id="slider" class="boxed-slider">
            <div class="container clearfix">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-items="1" data-loop="true" data-nav="true" data-autoplay="5000" data-animate-in="fadeIn" data-animate-out="fadeOut" data-speed="800">
                    <a href="#"><img src="{{ asset('img/merchant/neubodi_header.png') }}" alt="PopBox Neubodi"></a>
                </div>
            </div>
        </section>

         <!-- #Body -->
         <section id="slider" class="boxed-slider">
            <div class="container clearfix">
            <img src="img/merchant/body_trans.png" alt="Neubodi Body Image"> 
            </div>
        </section>

    <!-- #footer -->
    <section id="slider" class="boxed-slider">
        <div class="container clearfix">
        <img src="img/merchant/neubodi_footer.png" alt="Neubodi Footer Image"> 
            <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
        </div>
    </section>
    
        <!-- #content end -->
        <!-- Content
        ============================================= -->

        <section id="content">
            <div class="container clearfix">
                <div class="postcontent nobottommargin divcenter">
                    <h3 class="center">Participant Form</h3>
                    <div class="contact-widget">
                        <div class="contact-form-result"></div>
                        <form class="nobottommargin" id="formSubmit" name="formSubmit">
                            {{ csrf_field() }}
                            <div class="form-process"></div>
                            <div class="col_one_third">
                                <label for="template-contactform-name">Full Name <small>*</small></label>
                                <input type="text" id="cust_name" name="cust_name" value="" class="sm-form-control required" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-email">Email <small>*</small></label>
                                <input type="email" id="cust_email" name="cust_email" value="" class="required email sm-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="template-contactform-phone">Mobile Number <small>*</small></label>
                                <input type="text" id="cust_phone" name="cust_phone" value="" class="sm-form-control" />
                            </div>

                            <div class="col_three_third">
                            <label for="services">Have you used a PopBox locker before </label>
                                <select id="used_popbox" name="used_popbox" class="sm-form-control" style="height: 38px">
                                    <option value="">Select Answer</option>
                                    <option value="YES_USED">YES</option>
                                    <option value="NO_USED">NO</option>
                                </select>
                                <small><strong id="alert-services" style="color:#F9474F"></strong></small>
                            </div>
                            <br>

                            <div class="col_three_third">
                            <label for="services">Have you heard of Neubodi <small>*</small> </label>
                                <select id="heard_neubodi" name="heard_neubodi" class="sm-form-control" style="height: 38px">
                                    <option value="">Select Answer</option>
                                    <option value="YES_HEARD">YES</option>
                                    <option value="NO_HEARD">NO</option>
                                </select>
                                <small><strong id="alert-services" style="color:#F9474F"></strong></small>
                            </div>
                            <br>

                            <div class="col_three_third">
                            <label for="template-contactform-name">How do you know about the campaign? <small>*</small></label>
                                <input type="text" id="campaign" name="campaign" value="" class="sm-form-control required" />
                            </div>
                            <br>
                            
                            <div class="col_three_third">
                                <label for="city">Select City</label>
                                <select id="city" name="city" class="sm-form-control" style="height: 38px">
                                    @foreach ($citiesList as $element)
                                        <option value="{{ $element }}">{{ $element }}</option>
                                    @endforeach
                                </select>
                            </div><br>
                            <div class="clear"></div>

                            <div class="col_three_third">
                                <label for="template-contactform-service" >Select locker </label>
                                <select id="locker" name="locker" class="sm-form-control" style="height: 38px">
                                </select>
                            </div>
                            <div class="clear"></div> <br>

                            <div class="col_three_third">
                                <label>Address</label>
                                <textarea id="locker_address" class="sm-form-control" rows="4" disabled></textarea>
                                <textarea name="locker_id" id="locker_id" class="sm-form-control" rows="0" style="display:none;"></textarea>
                            </div>
                            <div class="clear"></div>

                            <div class="form-group">
                                <p>
                                    <!-- <h4>Term & Conditions</h4>  -->
                                    <h4><a href="/merchant/neuboditc" target="_blank">Term & Conditions</a></h4>
                                </p>
                                <input type="checkbox" name="snk" value="snk"> By entering into this campaign, entrants agree to be bound by these Terms and Conditions
together.
                            </div>
                            <div style="margin-top: 25px;">
                                <button class="button button-3d divcenter" 
                                    type="submit" 
                                    id="template-contactform-submit" 
                                    name="template-contactform-submit" 
                                    value="submit">SUBMIT DATA</button>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
        </section>

        <footer id="footer">
            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">
                <div class="container clearfix">
                    <div class="col_half">
                        Copyrights &copy; 2018 All Rights Reserved by PopBox Asia Services.
                    </div>
                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="https://www.facebook.com/pboxasia" class="social-icon si-small si-light si-rounded si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="https://twitter.com/popbox_asia" class="social-icon si-small si-light si-rounded si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/popbox_asia/" class="social-icon si-small si-light si-rounded si-instagram">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <!-- #copyrights end -->
        </footer>
        <!-- #footer end -->
        <!-- #wrapper end -->
        <!-- Go To Top
    ============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>
        <!-- External JavaScripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!-- Footer Scripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/validation.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $("#processTabs").tabs({
                    show: {
                        effect: "fade",
                        duration: 1000
                    }
                });
                $(".tab-linker").click(function() {
                    $("#processTabs").tabs("option", "active", $(this).attr('rel') - 1);
                    return false;
                });
            });
        </script>
        <script type="text/javascript">
            var lockerList = {!! json_encode($lockerMY) !!};
            // City on change
            $('select[name=city]').on('change', function(event) {
                var city = $(this).val();
                var item = [];

                for (var i in lockerList) {
                    if(lockerList[i].city==city){
                        item.push(lockerList[i]);
                    } 
                }
                var option ='';
                for (var i = 0; i < item.length; i++) {
                    option+="<option value='"+item[i].name+"'>"+item[i].name+"</option>"
                }
                $('select[name=locker]').html(option);
                $('select[name=locker]').trigger('change');
            });

            $('select[name=locker]').on('change', function(event) {
                var name = $(this).val();
                var selectedLocker = null;
                for (var i in lockerList) {
                    if(lockerList[i].name==name){
                        selectedLocker = lockerList[i];
                    } 
                }
                
                if (selectedLocker!= null) {
                    var address = selectedLocker.address;
                    address += "\n"+selectedLocker.address_2;
                    address += "\n"+selectedLocker.operational_hours;

                    $('textarea[id=locker_address]').val(address);
                    $('textarea[id=locker_id]').val(selectedLocker.locker_id);
                }
            });

            $(document).ready(function() {
                $('select[name=city]').trigger('change');
                $('#formSubmit').on('submit', function(event) {
                    event.preventDefault();
                    var form = $(this).serialize();
                    var rules = Array();

                    rules['cust_name'] = 'required|min:4';
                    rules['cust_email'] = 'required|min:4|email';
                    rules['cust_phone'] = 'required|min:8|numeric';
                    rules['locker_id'] = 'required';
                    rules['campaign'] = 'required|min:5';

                    var isValid = validate($(this),rules);
                    if (isValid==false) {
                         return;
                    }

                    console.log('param : ', $('#formSubmit').serialize());

                    $.ajax({
                        url: "{{ url('csrsubmit') }}",
                        data: $('#formSubmit').serialize(),
                        dataType:'json',
                        type: 'POST',
                        async: true,
                        success: function(data){
                            console.log('data : ', data);
                            if (data.isSuccess==true) {
                                // $('#formSubmit').find('.form-process').fadeOut();
                                window.location.href = "{{ url('merchant/neubodithankyou') }}";

                                //$('#modalSubmit ').modal('show');
                                //setTimeout(function() {location.reload();}, 3000);
                            } else {
                                $('#formSubmit').find('.form-process').fadeOut();
                                alert(data.errorMsg)
                            }
                        },
                        error: function(error){
                            console.log('error : ', error);
                            $('#formSubmit').find('.form-process').fadeOut();
                        }
                    }).fail(function(){
                        $('#formSubmit').find('.form-process').fadeOut();
                        alert('Failed to submit your request');
                        return;
                    });
                });
            });
        </script>
</body>

</html>
