<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia" />
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.ico') }}">

     {{-- Standard --}}
    <meta name="description" content="PopBox will deliver your shoes and OMAISU will save your unforgettable stories with your shoes.">
    <meta name="keywords" content="PopBox,Parcel,Locker,Loker,Kirim Barang,Logistik,Logistic">
    {{-- Schema.org for G+ --}}
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="PopBox On Demand - Omaisu">
    <meta itemprop="description" content="PopBox will deliver your shoes and OMAISU will save your unforgettable stories with your shoes.">
    <meta itemprop="image" content="{{ asset('img/omaisu/omaisu-meta-data.png') }}">
    {{-- Twitter Card data --}}
    <meta name="twitter:card" content="PopBox will deliver your shoes and OMAISU will save your unforgettable stories with your shoes.">
    <meta name="twitter:site" content="@PopBox_Asia">
    <meta name="twitter:title" content="PopBox - Parcel Collection Made Easy">
    <meta name="twitter:description" content="PopBox will deliver your shoes and OMAISU will save your unforgettable stories with your shoes.">
    <meta name="twitter:creator" content="@PopBox_Asia">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset('img/omaisu/omaisu-meta-data.png') }}">
    {{-- OpenGraph --}}
    <meta property="og:title" content="PopBox On Demand - Omaisu">
    <meta property="og:image" content="{{ asset('img/omaisu/omaisu-meta-data.png') }}">
    <meta property="og:description" content="PopBox will deliver your shoes and OMAISU will save your unforgettable stories with your shoes.">
    <meta property="og:url" content="{{ url('omaisu') }}" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/dark.css') }}">
    <!-- Agency Demo Specific Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/demos/agency/agency.css') }}" type="text/css" />
    <!-- / -->
    <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/animate.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/colors.php?color=c0bb62" type="text/css" />
    <!-- Document Title
    ============================================= -->
    <title>Omaisu | PopBox</title>
</head>

<body class="stretched">
    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
        <header id="header" class="sticky-style-2">
            <div class="container clearfix">
                <!-- Logo
                ============================================= -->
                <div id="logo" class="divcenter">
                    <div class="row" width="100%">
                        <!--  <a href="http://popbox.asia" class="standard-logo"><img class="divcenter" src="img/logo-header.png" alt="Canvas Logo">
                        </a> -->
                        <table>
                            <tbody>
                                <tr>
                                    <td><a href="{{ url('/') }}"><img src="{{ asset('img/omaisu/logo-header.png') }}"></a></td>
                                    {{-- <td>
                                        <a href="{{ url('/') }}"><img src="{{ asset('img/omaisu/logo-popbox.png') }}" alt="PopBox Logo">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="http://omaisu.id"><img src="{{ asset('img/omaisu/logo-omaisu.png') }}" alt="Omaisu Logo"></a>
                                    </td> --}}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- <a href="index-agency.html" class="retina-logo"><img class="divcenter" src="img/logo-header@2x.png" alt="Canvas Logo"></a> -->
                </div>
                <!-- #logo end -->
            </div>
            <div id="header-wrap">
                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu" class="style-2 center" style="background-color: #FFD500">
                    <div class="container clearfix">
                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                        <ul>
                            <li class="current">
                                <a href="{{ url('omaisu') }}">
                                    <div>Home</div>
                                </a>
                            </li>
                            <li>
                                <a href="#howitworks">
                                    <div>How It Works</div>
                                </a>
                            </li>
                            <li>
                                <a href="#treatment">
                                    <div>Treatment</div>
                                </a>
                            </li>
                            <li>
                                <a href="#content">
                                    <div>Create Order</div>
                                </a>
                            </li>
                            <li>
                                <a href="#beforeafter">
                                    <div>Before-After</div>
                                </a>
                            </li>
                            <li>
                                <a href="#contactus">
                                    <div>Contact Us</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- #primary-menu end -->
            </div>
        </header>
        <!-- #header end -->
        <section id="slider" class="boxed-slider">
            <div class="container clearfix">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-items="1" data-loop="true" data-nav="true" data-autoplay="5000" data-animate-in="fadeIn" data-animate-out="fadeOut" data-speed="800">
                    <a href="#"><img src="{{ asset('img/omaisu/slider02.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/omaisu/01.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/omaisu/02.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/omaisu/03.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/omaisu/04.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/omaisu/05.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/omaisu/06.jpg') }}" alt="Slider"></a>
                </div>
                <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
            </div>
        </section>
        <!-- Content
        ============================================= -->
        <section id="howitworks"">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h3>How It Works</h3>
                        <span>Your shoe has to be treated as an investment.</span>
                    </div>
                    <!-- <h3 class="center">How It Works</h3> -->
                    <div id="processTabs">
                        <ul class="process-steps bottommargin clearfix">
                            <li>
                                <a href="#ptab1" class="i-circled i-bordered i-alt divcenter">1</a>
                                <h5>CREATE ORDER & DO PAYMENT</h5>
                            </li>
                            <li>
                                <a href="#ptab2" class="i-circled i-bordered i-alt divcenter">2</a>
                                <h5>DROP TO LOCKER</h5>
                            </li>
                            <li>
                                <a href="#ptab3" class="i-circled i-bordered i-alt divcenter">3</a>
                                <h5>CLEANING PROCESS</h5>
                            </li>
                            <li>
                                <a href="#ptab4" class="i-circled i-bordered i-alt divcenter">4</a>
                                <h5>TAKE THE CLEAN SHOES</h5>
                            </li>
                        </ul>
                        <div>
                            <div id="ptab1" class="col_full">
                                <div class="col_half">
                                    <center><img src="{{ asset('img/omaisu/how01.jpg') }}" class="center" width="90%"></p>
                                    </center>
                                </div>
                                <div class="col_half col_last">
                                    <h4>Submit Your Order<br>
                                    <span class="subtitle">+ Fill <a href="#content">this form</a><br> 
                                    + Choose PopBox Locker near you<br>
                                    + You will receive SMS order contains order number</span></h4>
                                    <h4>
                                        Do Payment <br>
                                        <span class="subtitle">
                                            + Omaisu admin will contact you and send the invoice <br>
                                            + Transfer your payment to <strong>BNI Syariah, KC Benhil / Sensasi Sepatu Indonesia / 988 998 774</strong> <br>
                                            + Send your transfer confirmation to <a href="mailto:omaisubenhil@gmail.com">omaisubenhil@gmail.com </a> 
                                        </span>
                                    </h4>
                                </div>
                            </div>

                            <div id="ptab2" class="col_full">
                                <div class="col_half">
                                    <center><img src="{{ asset('img/omaisu/how02.jpg') }}" class="center" width="90%"></p>
                                    </center>
                                </div>
                                <div class="col_half col_last">
                                    <h4>WRAP YOUR SHOES<br>
                                    <span class="subtitle">+ Stuff your shoes with newspaper roll to keep them in shape.<br> 
                                    + Wrap it with plastic or box.<br>
                                    + Make sure it wrapped well. <br>
                                    + Label your shoe box with order number</span></h4>
                                    <h4>DROP AT POPBOX LOCKER<br>
                                    <span class="subtitle">+ Go to PopBox locker<br> 
                                    + Open "LAUNDRY" menu<br> 
                                    + Input your order number to open the locker<br>
                                    + Drop the shoes<br>
                                    + Close the locker</span></h4>
                                </div>
                            </div>

                            <div id="ptab3" class="col_full">
                                <div class="col_half">
                                    <center><img src="{{ asset('img/omaisu/how03.jpg') }}" class="center" width="90%"></p>
                                    </center>
                                </div>
                                <div class="col_half col_last">
                                    <h4>Omaisu will clean your shoes with love<br> 
                                    <span class="subtitle">
                                    + Cleaning process takes 4-5 days<br>
                                    + OMAISU will send you "before and after treatment" photo as a guarantee about your shoe condition, so we will give you some information - if there is any damages, 
                                    </span></h4>
                                </div>
                            </div>

                            <div id="ptab4" class="col_full">
                                <div class="col_half">
                                    <center><img src="{{ asset('img/omaisu/how04.jpg') }}" class="center" width="90%"></p>
                                    </center>
                                </div>
                                <div class="col_half col_last">
                                    <h4>SMS NOTIFICATION<br>
                                    <span class="subtitle">+ We will notify you once your clean shoes has been ready.<br> 
                                    + The SMS mentions about the PIN CODE and expiry time<br></h4>
                                    
                                    <h4>TAKE YOUR SHOES<br>
                                    <span class="subtitle">+ Go to PopBox locker<br> 
                                    + Open "PARCEL PICKUP" menu,<br> 
                                    + Input your PIN CODE<br>
                                    + Take your shoes<br>
                                    + Close the loker</span></h4>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- CONTENT
        ============================================= -->
        <section id="treatment">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h3 class="center">Our <span>Treatment</span></h3>
                        <span>We give attention to every detail materials</span>
                        <span>[Leather, suede, nubuck, bludru, canvas, denim, nylon, etc]</span>
                    </div>
                   
		 	<div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#content" value="CLEANING TREATMENT"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>1. </span>CLEANING TREATMENT
                                <span class="subtitle">Basic Cleaning for your Shoes (especially in Outside)</span>
                                <span class="subtitle"><strong>Available For Material Canvas, Jeans, Nylon.</strong></span>
                            </h3>
                            <h3><span>69 K (Standard)  &  89 K (Boots)</span></h3>
                        </div>
                    </div>
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#content" value="SIGNATURE SPA TREATMENT"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>2. </span>SIGNATURE SPA TREATMENT
                                <span class="subtitle">Detail Cleaning for your Shoes (Deeply Cleaning for Outside & Inside)</span>
                                <span class="subtitle"><strong>Available For Material Canvas, Jeans, Nylon</strong></span>
                            </h3>
                            <h3><span>89 K (Standard)  &  109 K (Boots)</span></h3>
                        </div>
                    </div>
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#content" value="CLEANING TREATMENT"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>3. </span>CLEANING TREATMENT
                                <span class="subtitle">Basic Cleaning for your Shoes (especially in Outside)</span>
                                <span class="subtitle"><strong>Available for Material Full Grain Leather, Suede, Nubuck, Vegtan</strong></span>
                            </h3>
                            <h3><span>79 K (Standard)  &  109 K (Boots)</span></h3>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col_one_third nobottommargin">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#content" value="SIGNATURE SPA TREATMENT"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>4. </span>SIGNATURE SPA TREATMENT
                                <span class="subtitle">Detail Cleaning for your Shoes (Deeply Cleaning for Outside & Inside)</span>
                                <span class="subtitle"><strong>Available for Material Full Grain Leather, Suede, Nubuck, Vegtan</strong></span>
                            </h3>
                            <h3><span>99 K (Standard)  &  119 K (Boots)</span></h3>
                        </div>
                    </div>
                    <div class="col_one_third nobottommargin">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#content" value="PROTECTION TREATMENT"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>5. </span>PROTECTION TREATMENT
                                <span class="subtitle">Total Protection for your shoes from water, mud, sun, & dust</span>
                                <span class="subtitle"><strong>Mostly for all shoe material</strong></span>
                            </h3>
                            <h3><span>150 K (Standard) & 200 K (Boots)</span></h3>
                        </div>
                    </div>
                    <div class="col_one_third nobottommargin col_last">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#content" value="RIDING BOOTS TREATMENT"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>6. </span>RIDING BOOTS TREATMENT
                                <span class="subtitle">Detail cleaning for your Riding Boots</span>
                                <span class="subtitle"><strong>Available for all Material, especially Full Grain Leather, Suede, Nubuck, Vegtan</strong></span>
                            </h3>
                            <h3><span>199 K</span></h3>
                        </div>
                    </div>                    
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="container clearfix">
                <div class="postcontent nobottommargin divcenter">
                    <h3 class="center">Create <span>Omaisu</span> Order</h3>
                    <div class="contact-widget">
                        <div class="contact-form-result"></div>
                        <form class="nobottommargin" id="formSubmit" name="formSubmit">
                            {{ csrf_field() }}
                            <div class="form-process"></div>
                            <div class="col_one_third">
                                <label for="template-contactform-name">Name <small>*</small></label>
                                <input type="text" id="cust_name" name="cust_name" value="" class="sm-form-control required" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-email">Email <small>*</small></label>
                                <input type="email" id="cust_email" name="cust_email" value="" class="required email sm-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="template-contactform-phone">Phone</label>
                                <input type="text" id="cust_phone" name="cust_phone" value="" class="sm-form-control" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-subject">Number Of Shoes <small>*</small></label>
                                <input type="text" id="shoes_total" name="shoes_total" value="" class="required sm-form-control" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-subject">Shoe Brand<small>*</small></label>
                                <input type="text" id="shoes_brand" name="shoes_brand" value="" class="required sm-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="services">Services</label>
                                <select id="services" name="services" class="sm-form-control" style="height: 38px">
                                    <option value="">-- Select Service --</option>
                                    <option value="CLEANING TREATMENT">CLEANING TREATMENT</option>
                                    <option value="SIGNATURE SPA TREATMENT">SIGNATURE SPA TREATMENT</option>
                                    <option value="CLEANING TREATMENT">CLEANING TREATMENT</option>
                                    <option value="SIGNATURE SPA TREATMENT">SIGNATURE SPA TREATMENT</option>
                                    <option value="PROTECTION TREATMENT">PROTECTION TREATMENT</option>
                                    <option value="RIDING BOOTS TREATMENT">RIDING BOOTS TREATMENT</option>
                                </select>
                                <small><strong id="alert-services" style="color:#F9474F"></strong></small>
                            </div>
                            <div class="clear"></div>
                            <div class="col_three_third">
                                <label for="city">Select City</label>
                                <select id="city" name="city" class="sm-form-control" style="height: 38px">
                                    @foreach ($citiesList as $element)
                                        <option value="{{ $element }}">{{ $element }}</option>
                                    @endforeach
                                </select>
                            </div><br>
                            <div class="clear"></div>
                            <div class="col_three_third">
                                <label for="template-contactform-service" >Select Locker</label>
                                <select id="locker" name="locker" class="sm-form-control" style="height: 38px">
                                </select>
                                <small>Look Nearest Locker <a href="https://www.popbox.asia/#nearest_locker" target="_blank">Here</a></small>
                            </div>
                            <div class="clear"></div> <br>
                            <div class="col_three_third">
                                <label>Address</label>
                                <textarea id="locker_address" class="sm-form-control" rows="3" disabled>
                                    
                                </textarea>
                            </div>
                            <div class="clear"></div>

                            <div class="form-group">
                                <p>
                                    <h4>Terms & Conditions for Customers:</h4> 
                                    1. OMAISU will send you "before and after treatment" photo as a guarantee about your shoe condition, so we will give you some informations - if there is any damages,
                                    <br> 2. If you don't pick your shoes up after 3 days of notice, you need to collect your shoes at Omaisu's store
                                </p>
                                <input type="checkbox" name="snk" value="snk"> &nbsp; By clicking "Submit Order", you agree to the Terms and Conditions.
                            </div>
                            <div style="margin-top: 25px;">
                                <button class="button button-3d divcenter" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">SUBMIT ORDER</button>
                            </div>
                        </form>
                    </div>
                </div>
        </section>
        <!-- CONTENT
        ============================================= -->
        <section id="beforeafter">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                    <div class="heading-block center">
                        <h3>Some of our project</h3>
                        <span>Awesome Works that We've present to. Proudly.!</span>
                    </div>
                    <div id="portfolio" class="portfolio grid-container portfolio-5 clearfix">
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/after01.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/before01.jp') }}g" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/omaisu/after01.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                    <a href="{{ asset('img/omaisu/before01.jpg') }}" class="hidden" data-lightbox="gallery-item"></a>
                                </div>
                            </div>
                            <!--    <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/after02.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/before02.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/omaisu/after02.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                    <a href="{{ asset('img/omaisu/before02.jpg') }}" class="hidden" data-lightbox="gallery-item"></a>
                                    <a href="portfolio-single-gallery.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <!--  <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/after03.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/before03.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/omaisu/after03.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                    <a href="{{ asset('img/omaisu/before03.jpg') }}" class="hidden" data-lightbox="gallery-item"></a>
                                </div>
                            </div>
                            <!--  <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/after04.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/before04.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/omaisu/after04.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                    <a href="{{ asset('img/omaisu/before04.jpg') }}" class="hidden" data-lightbox="gallery-item"></a>
                                </div>
                            </div>
                            <!-- <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/after05.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/omaisu/before05.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/omaisu/after05.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                    <a href="{{ asset('img/omaisu/before05.jpg') }}" class="hidden" data-lightbox="gallery-item"></a>
                                    <a href="portfolio-single-gallery.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- #portfolio end -->
                    <div class="clear"></div>
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- Content
        ============================================= -->
        <section id="contactus" class="divcenter">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h3>We love to hear from you.</h3>
                        <span>Get support by phone or email. In the meantime, peep our social wall to see what we're up to.</span>
                    </div>
                    <div class="col_half center">
                        <h5>OMAISU</h5> Bendungan Hilir Raya No. 27
                        <br>Tanah Abang, Jakarta Pusat
                        <br>10210
                        <br><a href="http://omaisu.id/">omaisu.id</a>
                        <br>
                        <br>02129543455
                        <br><a href="mailto:my@omaisu.id">my@omaisu.id</a>
                    </div>
                    <div class="col_half col_last center">
                        <h5>PT POPBOX ASIA SERVICES</h5> Grand Slipi Tower Unit 18J
                        <br> Jl. S. Parman kav 22-24
                        <br> Jakarta 11480
                        <br> <a href="www.popbox.asia">www.popbox.asia</a>
                        <br>
                        <br>+62 21 2902 2537/8
                        <br><a href="mailto:info@popbox.asia">info@popbox.asia</a>
                    </div>
                    <!-- Postcontent
                    ============================================= -->
                    <div class="postcontent nobottommargin divcenter">
                        <div class="contact-widget">
                            <div class="contact-form-result"></div>
                            <div class="clear"></div>
                            <form class="nobottommargin" id="contactform" name="contactform">
                                <div class="form-process"></div>
                                <div class="col_one_third">
                                    <label for="template-contactform-name">Name<small>*</small></label>
                                    <input type="text" id="name" name="name" value="" class="sm-form-control required" />
                                </div>
                                <div class="col_one_third">
                                    <label for="template-contactform-email">Email<small>*</small></label>
                                    <input type="email" id="email" name="email" value="" class="required email sm-form-control" />
                                </div>
                                <div class="col_one_third col_last">
                                    <label for="template-contactform-phone">Phone Number<small>*</small></label>
                                    <input type="text" id="phone" name="phone" value="" class="sm-form-control required" />
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="template-contactform-subject">Subject<small>*</small></label>
                                    <input type="text" id="subject" name="subject" value="" class="required sm-form-control" />
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="template-contactform-message">Message<small>*</small></label>
                                    <textarea class="required sm-form-control" id="message" name="message" rows="4" cols="30"></textarea>
                                </div>
                                <div class="col_full">
                                     <div class="g-recaptcha" data-sitekey="6LerZAsUAAAAAKGr1poO2iTXLlvM_tE6OJYmlle4" style="display: inline-block;"></div>
                                </div>
                                <div class="col_last">
                                    <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- .postcontent end -->
                </div>
            </div>
        </section>
        <div class="modal fade" id="modalSubscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Contact</h4>
                    </div>
                    <div class="modal-body">
                       Thanks for your feedback. We will contact you soon.
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalSubmit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Omaisu Order</h4>
                    </div>
                    <div class="modal-body">
                       Success order 
                    </div>
                </div>
            </div>
        </div>
        <!-- #content end -->
        <!-- Footer
        ============================================= -->
        <footer id="footer">
            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">
                <div class="container clearfix">
                    <div class="col_half">
                        Copyrights &copy; 2017 All Rights Reserved by PopBox Asia Services.
                    </div>
                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="https://www.facebook.com/pboxasia" class="social-icon si-small si-light si-rounded si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="https://twitter.com/popbox_asia" class="social-icon si-small si-light si-rounded si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/popbox_asia/" class="social-icon si-small si-light si-rounded si-instagram">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <!-- #copyrights end -->
        </footer>
        <!-- #footer end -->
        <!-- #wrapper end -->
        <!-- Go To Top
    ============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>
        <!-- External JavaScripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!-- Footer Scripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/validation.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $("#processTabs").tabs({
                    show: {
                        effect: "fade",
                        duration: 400
                    }
                });
                $(".tab-linker").click(function() {
                    $("#processTabs").tabs("option", "active", $(this).attr('rel') - 1);
                    return false;
                });
            });
        </script>
        <script type="text/javascript">
            var lockerList = {!! json_encode($lockerJkt) !!};
            // City on change
            $('select[name=city]').on('change', function(event) {
                var city = $(this).val();
                var item = [];

                for (var i in lockerList) {
                    if(lockerList[i].city==city){
                        item.push(lockerList[i]);
                    } 
                }
                var option ='';
                for (var i = 0; i < item.length; i++) {
                    option+="<option value='"+item[i].name+"'>"+item[i].name+"</option>"
                }
                $('select[name=locker]').html(option);
                $('select[name=locker]').trigger('change');
            });

            $('select[name=locker]').on('change', function(event) {
                var name = $(this).val();
                var selectedLocker = null;
                for (var i in lockerList) {
                    if(lockerList[i].name==name){
                        selectedLocker = lockerList[i];
                    } 
                }
                if (selectedLocker!= null) {
                    var address = selectedLocker.address;
                    address += "\n"+selectedLocker.address_2;
                    address += "\n"+selectedLocker.operational_hours;

                    $('textarea[id=locker_address]').val(address);
                }
            });

            $(document).ready(function() {
                $('select[name=city]').trigger('change');
                $('a[href="#content"]').on('click', function(event) {
                    var value = $(this).attr('value');
                    $('select[name=services]').val(value);
                });

                // form omaisu submit
                $('#formSubmit').on('submit', function(event) {
                    event.preventDefault();
                    var form = $(this).serialize();
                    var rules = Array();
                    rules['cust_name'] = 'required|min:4';
                    rules['cust_email'] = 'required|min:4|email';
                    rules['cust_phone'] = 'required|min:4|numeric';
                    rules['shoes_total'] = 'required|numeric';
                    rules['shoes_brand'] = 'required';
                    rules['services'] = 'required';
                    rules['locker'] = 'required';

                    var isValid = validate($(this),rules);
                    if (isValid==false) {
                         return;
                    }
                    $.ajax({
                        url: "{{ url('omaisu') }}",
                        data: $('#formSubmit').serialize(),
                        dataType:'json',
                        type: 'post',
                        async: true,
                        success: function(data){
                            if (data.isSuccess==true) {
                                // Show Modal
                                //hideLoading('#subscribe', 'load');
                                $('#formSubmit').find('.form-process').fadeOut();
                                $('#modalSubmit ').modal('show');
                                setTimeout(function() {
                                    location.reload();
                                }, 3000);
                            } else {
                                $('#formSubmit').find('.form-process').fadeOut();
                                //hideLoading('#subscribe', 'load');
                                alert(data.errorMsg)
                            }
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            $('#formSubmit').find('.form-process').fadeOut();
                        }
                    }).fail(function(){
                        //hideLoading('#subscribe', 'load');
                        $('#formSubmit').find('.form-process').fadeOut();
                        alert('Failed To Submit');
                        return;
                    });
                });

                // form contact submit
                $('#contactform').submit(function(event) {
                    event.preventDefault();
                    var rules = Array();
                    rules['name'] = 'required';
                    rules['email'] = 'required|email';
                    rules['message'] = 'required';
                    var isValid = validate($(this),rules);
                    if (isValid==false) {
                        return;
                    }
                    $.ajax({
                        url: "{{ url('contact') }}",
                        data: $('#contactform').serialize(),
                        dataType:'json',
                        type: 'post',
                        async: true,
                        success: function(data){
                            if (data.isSuccess==true) {
                                // Show Modal
                                //hideLoading('#subscribe', 'load');
                                $('#contactform').find('.form-process').fadeOut();
                                $('#modalSubscribe').modal('show');
                                $('input[name="email"]').val('');
                                $('input[name="name"]').val('');
                                $('input[name="message"]').val('');
                                $('input[name="subject"]').val('');

                            } else {
                                $('#contactform').find('.form-process').fadeOut();
                                //hideLoading('#subscribe', 'load');
                                alert(data.errorMsg)
                            }
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            $('#contactform').find('.form-process').fadeOut();
                        }
                    }).fail(function(){
                        //hideLoading('#subscribe', 'load');
                        $('#contactform').find('.form-process').fadeOut();
                        alert('Failed To Submit');
                        return;
                    });
                });
            });
        </script>
</body>

</html>
