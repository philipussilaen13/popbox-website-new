@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
 <style type="text/css">
     .active{
        border: 1px solid #1abc9c !important;
    }

    #section-welcome{
    	height: auto;
    }


 </style>
 <section id="slider" class="boxed-slider">
            <div class="container clearfix">
                <img src="{{ asset('img/merchant/banner-merchant-howto.jpg') }}" alt="Image">
            </div>
 </section>
 	<br>
 <section id="section-welcome">
    <div class="container clearfix">        
        <div class="col_full">
            <div class="center">
                <h3>Panduan untuk membuat manual order</h3>
            </div>
            <div align="left">
                <h5>A. Manual Order Tuliskan nama toko/merchant.</h5>
            </div>
            <div align="left">
                <h5>1. Tuliskan customer phone.</h5>
            </div>
            <div align="left">
                <h5>
                    2. Pilih pickup locker location.
                </h5>
            </div>
            <div align="left">
                <h5>3. Pilih alamat Penjemputan atau pickup location.</h5>
            </div>
            <div align="left">
                <h5>4. Tuliskan customer email.</h5>
            </div>
            <div align="left">
                <h5>5. Tuliskan detail item</h5>
            </div>
            <div align="left">
                <h5>6. Tuliskan Price</h5>
            </div>
            <div align="left">
                <h5>7. Tekan submit</h5>
            </div>
        </div>

        <div class="col_full">
            <div class="center">
                <h3>Panduan lengkap untuk manual order dengan upload dari excel</h3>
            </div>
            <div align="left">
                <h5>1. Download template email.</h5>
            </div>
            <div align="left">
                <h5>
                    2. Lengkapi informasi template sesuai judul kolom. (jangan sampe merubah format template) dan save
                </h5>
            </div>
            <div align="left">
                <h5>3. Klik upload untuk dan pilih file excel yang kita buat sebelumnya.</h5>
            </div>
            <div align="left">
                <h5>5. Tekan submit upload.</h5>
            </div>            
            <div align="left">
                <h5>6. Akan keluar informasi berhasi atau gagalnya upload dari excel.</h5>
            </div>            
        </div> 
    </div>
 </section>
      

        
@stop

