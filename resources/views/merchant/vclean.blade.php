<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia" />
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.ico') }}">

     {{-- StKamurd --}}
    <meta name="description" content="The Right Choose to Clean Your Shoes">
    <meta name="keywords" content="PopBox,Parcel,Locker,Loker,Kirim Barang,Logistik,Logistic">
    {{-- Schema.org for G+ --}}
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="PopBox On Demand - Vclean Shoes">
    <meta itemprop="description" content="The Right Choose to Clean Your Shoes">
    <meta itemprop="image" content="{{ asset('img/omaisu/omaisu-meta-data.png') }}">
    {{-- Twitter Card data --}}
    <meta name="twitter:card" content="The Right Choose to Clean Your Shoes">
    <meta name="twitter:site" content="@PopBox_Asia">
    <meta name="twitter:title" content="PopBox - Parcel Collection Made Easy">
    <meta name="twitter:description" content="The Right Choose to Clean Your Shoes">
    <meta name="twitter:creator" content="@PopBox_Asia">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset('img/omaisu/omaisu-meta-data.png') }}">
    {{-- OpenGraph --}}
    <meta property="og:title" content="PopBox On Demand - Vclean Shoes">
    <meta property="og:image" content="{{ asset('img/omaisu/omaisu-meta-data.png') }}">
    <meta property="og:description" content="The Right Choose to Clean Your Shoes">
    <meta property="og:url" content="{{ url('omaisu') }}" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/dark.css') }}">
    <!-- Agency Demo Specific Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/demos/agency/agency.css') }}" type="text/css" />
    <!-- / -->
    <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/animate.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/colors.php?color=00A3C7" type="text/css" />
    <!-- Document Title
    ============================================= -->
    <title>VCLEANSHOES | PopBox</title>
</head>

<body class="stretched">
    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
        <header id="header" class="sticky-style-2">
            <div class="container clearfix">
                <!-- Logo
                ============================================= -->
                <div id="logo" class="divcenter">
                    <div class="row" width="100%">
                        <!--  <a href="http://popbox.asia" class="stKamurd-logo"><img class="divcenter" src="img/logo-header.png" alt="Canvas Logo">
                        </a> -->
                        <table>
                            <tbody>
                                <tr>
                                    <td><a href="{{ url('/') }}"><img src="{{ asset('img/vcleanshoes/logo-header.png') }}"></a></td>
                                    {{-- <td>
                                        <a href="{{ url('/') }}"><img src="{{ asset('img/omaisu/logo-popbox.png') }}" alt="PopBox Logo">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="http://omaisu.id"><img src="{{ asset('img/omaisu/logo-omaisu.png') }}" alt="Omaisu Logo"></a>
                                    </td> --}}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- <a href="index-agency.html" class="retina-logo"><img class="divcenter" src="img/logo-header@2x.png" alt="Canvas Logo"></a> -->
                </div>
                <!-- #logo end -->
            </div>
            <div id="header-wrap">
                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu" class="style-2 center" style="background-color: #00A3C7;color: #fff">
                    <div class="container clearfix">
                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                        <ul>
                            <li>
                                <a href="#">
                                    <div>Beranda</div>
                                </a>
                            </li>
                            <li>
                                <a href="#howitworks">
                                    <div>Cara</div>
                                </a>
                            </li>
                            <li>
                                <a href="#treatment">
                                    <div>Layanan</div>
                                </a>
                            </li>
                            <li>
                                <a href="#content">
                                    <div>Order</div>
                                </a>
                            </li>
                            <li>
                                <a href="#beforeafter">
                                    <div>Hasil</div>
                                </a>
                            </li>
                            <li>
                                <a href="#contactus">
                                    <div>Hubungi Kami</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- #primary-menu end -->
            </div>
        </header>
        <!-- #header end -->
        <section id="slider" class="boxed-slider">
            <div class="container clearfix">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-items="1" data-loop="true" data-nav="true" data-autoplay="5000" data-animate-in="fadeIn" data-animate-out="fadeOut" data-speed="800">
                    <a href="#"><img src="{{ asset('img/vcleanshoes/slide1.jpg') }}" alt="Slider"></a>
                    <a href="#"><img src="{{ asset('img/vcleanshoes/slide2.jpg') }}" alt="Slider"></a>

                </div>
                <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
            </div>
        </section>
        <!-- Content
        ============================================= -->
        <section id="howitworks" >
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h3>Telah Hadir Layanan VCleanShoes & PopBox</h3>
                        <span>
                        Letakkan dan ambil sepatu kamu di loker PopBox.<br>
                        <strong>Bagaimana caranya?</strong>

                        </span>
                    </div>
                    <div class="col_half">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <a class="i-circled i-bordered i-alt divcenter">1</a>
                            <h3>PESAN DAN BAYAR<span class="subtitle">Lengkapi data pesananmu <span><a href="#content">di sini</a>. <br>Kamu akan mendapatkan <strong>SMS dan Email notifikasi</strong> pemesanan yang berisi nomor order untuk membuka loker. <br> Lakukan pembayaran ke <strong>BCA 4129018060 a/n Rio Nugraha</strong> </span></h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>

                            <a class="i-circled i-bordered i-alt divcenter">2</a>
                            <h3>LETAKKAN SEPATU<span class="subtitle">
                            Datang ke loker PopBox yang sudah kamu pilih, pilih menu <strong>"LAUNDRY"</strong>, kemudian masukkan nomor order. Letakkan Sepatumu ke loker dan tutup pintu loker.</span></h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>

                        </div>
                    </div>

                    <div class="col_half col_last">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">

                            <a class="i-circled i-bordered i-alt divcenter">3</a>
                            <h3>PROSES PENCUCIAN SEPATU
                                <span class="subtitle">VCleanShoes akan memproses sepatu kamu sesuai dengan layanan yang kamu pilih.</span>
                                <span class="subtitle">Proses pencucian 2-4 hari kerja.</span>
                            </h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>

                             <a class="i-circled i-bordered i-alt divcenter">4</a>
                            <h3>PENGAMBILAN SEPATU<span class="subtitle">Kami akan mengirimkan notifikasi setelah sepatu kamu selesai. Silakan datang ke loker dan buka menu <strong>"MENGAMBIL BARANG"</strong>, kemudian masukkan PIN, dan ambil sepatu mu.</span></h3>
                            <!-- <div class="divider divider-short divider-center"><i class="icon-circle"></i></div> -->
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                   
                    <div class="divider divider-center nobottommargin"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- CONTENT
        ============================================= -->
        <section id="treatment">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h3 class="center">Layanan <span>Kami</span></h3>
                        {{-- <span>We give attention to every detail materials</span> --}}
                        {{-- <span>[Leather, suede, nubuck, bludru, canvas, denim, nylon, etc]</span> --}}
                    </div>
                   
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>1. </span>FAST CLEAN
                                <span class="subtitle">Fast Clean adalah layanan treatment bagian luar sepatu</span>
                                {{-- <span class="subtitle"><strong>(Outsole, Mid Sole & Upper)</strong></span> --}}
                            </h3>
                            <h2><span>40 k</span></h2>
                        </div>
                    </div>
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>2. </span>FRESH CLEAN
                                <span class="subtitle">Fresh Clean adalah layanan Laundry Sepatu yang bergaransi selama 1 minggu.</span>
                                <span class="subtitle">2x treatment yang diberikan, proses lebih mendalam dari Fast Clean. Free 1x cuci jika kotor kembali.</span>
                            </h3>
                            <h2><span>55 k</span></h2>
                        </div>
                    </div>
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>3. </span>DEEP CLEAN
                                <span class="subtitle">Deep Clean adalah layanan Laundry Sepatu yang kami tawarkan untuk membersihkan seluruh sepatu Kamu.</span>
                                <span class="subtitle">Mulai dari bagian atas, bawah, kiri, kanan, serta bagian dalam agar sepatu menjadi baru kembali. Layanan ini bergaransi selama 1 Bulan</span>
                            </h3>
                            <h2><span>100 k</span></h2>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>4. </span>CLEAN PACKAGE 
                                <span class="subtitle">Clean Package adalah layanan paket Laundry Sepatu yang kami tawarkan seperti hal nya Deep Clean</span>
                                <span class="subtitle">Clean Package merupakan paket 1 Bulan Laundry Sepatu. Selama 1 bulan, kamu bebas kapan pun dan berapa kali pun kamu mau mencuci sepatu mu.</span>
                            </h3>
                            <h2><span>200 k</span></h2>
                        </div>
                    </div>
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>5. </span>STUDENT PACKAGE
                                <span class="subtitle">Fresh Clean, Deep Clean</span>
                                {{-- <span class="subtitle"><strong>(All Side on Your Shoes)</strong></span> --}}
                            </h3>
                            <h2><span>35k, 50k</span></h2>
                        </div>
                    </div>
                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>6. </span>BABY SHOES
                                <span class="subtitle">Rawat sepatu baby mu agar selalu terjaga kebersihan nya.</span>
                                {{-- <span class="subtitle"><strong>(All Side on Your Shoes)</strong></span> --}}
                            </h3>
                            <h2><span>20 k</span></h2>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>7. </span>WHITENING SOLE
                                <span class="subtitle">Sol Sepatu mu menjadi kuning? Ingin menghilangkan nya? Serahkan pada kami. Kami bisa mengembalikan sepatu yang menguning menjadi putih kembali. Garansi 1 tahun</span>
                                {{-- <span class="subtitle"><strong>(Detail Side on your Boots)</strong></span> --}}
                            </h3>
                            <h2><span>100 k</span></h2>
                        </div>
                    </div>
                    <div class="col_one_third">
                        <div class="feature-box fbox-center fbox-outline fbox-effect nobottomborder">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-ok i-alt"></i></a>
                            </div>
                            <h3>
                                <span>7. </span>Repaint
                                <span class="subtitle">Warna sepatu mu pudar? Di repaint aja. Bisa custom juga buat yg mau custom silahkan contact kami.</span>
                                {{-- <span class="subtitle"><strong>(Detail Side on your Boots)</strong></span> --}}
                            </h3>
                            <h2><span>125 k, 150k</span></h2>
                        </div>
                    </div>

                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="container clearfix">
                <div class="postcontent nobottommargin divcenter">
                    <h3 class="center">Masukkan Pesanan</h3>
                    <div class="contact-widget">
                        <div class="contact-form-result"></div>
                        <form class="nobottommargin" id="formSubmit" name="formSubmit">
                            {{ csrf_field() }}
                            <div class="form-process"></div>
                            <div class="col_one_third">
                                <label for="template-contactform-name">Nama <small>*</small></label>
                                <input type="text" id="cust_name" name="cust_name" value="" class="sm-form-control required" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-email">Email <small>*</small></label>
                                <input type="email" id="cust_email" name="cust_email" value="" class="required email sm-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="template-contactform-phone">No. Handphone</label>
                                <input type="text" id="cust_phone" name="cust_phone" value="" class="sm-form-control" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-subject">Jumlah Sepatu <small>*</small></label>
                                <input type="text" id="shoes_total" name="shoes_total" value="" class="required sm-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="services">Services</label>
                                <select id="services" name="services" class="sm-form-control" style="height: 38px">
                                    <option value="">-- Select Service --</option>
                                    <option value="FAST CLEAN">FAST CLEAN</option>
                                    <option value="FRESH CLEAN">FRESH CLEAN</option>
                                    <option value="DEEP CLEAN">DEEP CLEAN</option>
                                    <option value="CLEAN PACKAGE">CLEAN PACKAGE</option>
                                    <option value="STUDENT FRESH CLEAN">STUDENT FRESH CLEAN</option>
                                    <option value="STUDENT DEEP CLEAN">STUDENT DEEP CLEAN</option>
                                    <option value="BABY SHOES">BABY SHOES</option>
                                    <option value="WHITENING SOLE">WHITENING SOLE</option>
                                    <option value="REPAINT">REPAINT</option>
                                </select>
                                <small><strong id="alert-services" style="color:#F9474F"></strong></small>
                            </div>
                            <div class="clear"></div>
                            <div class="col_three_third">
                                <label for="city">Pilih Kota</label>
                                <select id="city" name="city" class="sm-form-control" style="height: 38px">
                                    @foreach ($citiesList as $element)
                                        <option value="{{ $element }}">{{ $element }}</option>
                                    @endforeach
                                </select>
                            </div><br>
                            <div class="clear"></div>
                            <div class="col_three_third">
                                <label for="template-contactform-service" >Pilih Loker</label>
                                <select id="locker" name="locker" class="sm-form-control" style="height: 38px">
                                </select>
                                <small>Cari Locker Terdekat <a href="https://www.popbox.asia/#nearest_locker" target="_blank">disini</a></small>
                            </div>
                            <div class="clear"></div> <br>
                            <div class="col_three_third">
                                <label>Alamat</label>
                                <textarea id="locker_address" class="sm-form-control" rows="3" disabled>
                                    
                                </textarea>
                            </div>
                            <div class="clear"></div>

                            <div class="form-group">
                                <p>
                                    <h4>Syarat dan Ketentuan Konsumen:</h4> 
                                    1. Gratis biaya pengiriman hanya berlaku untuk 3 pasang sepatu. 
                                    <br> 2. Sepatu akan diproses setelah konsumen melakukan pembayaran melalui transfer bank.
                                    <br> 3. Harga berlaku untuk wilayah Jabodetabek.
                                </p>
                                <input type="checkbox" name="snk" value="snk"> &nbsp; By clicking "Submit Order", you agree to the Terms and Conditions.
                            </div>
                            <div style="margin-top: 25px;">
                                <button class="button button-3d divcenter" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
        </section>
        <!-- CONTENT
        ============================================= -->
        <section id="beforeafter">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                    <div class="heading-block center">
                        <h3>Kenakan, Jangan Disimpan</h3>
                        <span>Biar kami yang membersihkan</span>
                    </div>
                    <div id="portfolio" class="portfolio grid-container portfolio-4 clearfix">
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/vcleanshoes/img 1.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/vcleanshoes/img 1.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                </div>
                            </div>
                            <!--    <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/vcleanshoes/img 2.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/vcleanshoes/img 2.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                </div>
                            </div>
                            <!--    <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/vcleanshoes/img 3.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/vcleanshoes/img 3.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                </div>
                            </div>
                            <!--    <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                        <article class="portfolio-item pf-icons pf-illustrations">
                            <div class="portfolio-image">
                                <div class="fslider" data-arrows="false" data-speed="400" data-pause="10000">
                                    <div class="flexslider">
                                        <div class="slider-wrap">
                                            <div class="slide">
                                                <a href="portfolio-single-gallery.html"><img src="{{ asset('img/vcleanshoes/img 4.jpg') }}" alt="Morning Dew"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-overlay" data-lightbox="gallery">
                                    <a href="{{ asset('img/vcleanshoes/img 4.jpg') }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                </div>
                            </div>
                            <!--    <div class="portfolio-desc">
                                <h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
                                <span><a href="#"><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
                            </div> -->
                        </article>
                    </div>
                    <!-- #portfolio end -->
                    <div class="clear"></div>
                    <div class="divider divider-center"><i class="icon-chevron-down"></i></div>
                </div>
            </div>
        </section>
        <!-- #content end -->
        <!-- Content
        ============================================= -->
        <section id="contactus" class="divcenter">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="heading-block center">
                        <h3>HUBUNGI KAMI.</h3>
                        <span>Untuk pertanyaan, saran, serta info dari kami, silakan hubungi tim layanan pelanggan kami</span>
                    </div>
                    <div class="col_half center">
                        <h5>Vclean Shoes</h5> Jl. Cempaka Putih Raya Blok B RT 007 / RW 008
                        <br>( Samping SEVEL Cempaka Putih Raya ) 
                        <br>Cempaka Putih, Jakarta Pusat
                        <br>10510
                        <br><a href="http://laundrysepatumu.com/">laundrysepatumu.com</a>
                        <br>
                        <br>0838 7484 5890
                        <br><a href="mailto:vcleanshoes@gmail.com">vcleanshoes@gmail.com</a>
                    </div>
                    <div class="col_half col_last center">
                        <h5>PT POPBOX ASIA SERVICES</h5> Grand Slipi Tower Unit 18J
                        <br> Jl. S. Parman kav 22-24
                        <br> Jakarta 11480
                        <br> <a href="www.popbox.asia">www.popbox.asia</a>
                        <br>
                        <br>+62 21 2902 2537/8
                        <br><a href="mailto:info@popbox.asia">info@popbox.asia</a>
                    </div>
                    <!-- Postcontent
                    ============================================= -->
                    <div class="postcontent nobottommargin divcenter">
                        <div class="contact-widget">
                            <div class="contact-form-result"></div>
                            <div class="clear"></div>
                            <form class="nobottommargin" id="contactform" name="contactform">
                                <div class="form-process"></div>
                                <div class="col_one_third">
                                    <label for="template-contactform-name">Nama<small>*</small></label>
                                    <input type="text" id="name" name="name" value="" class="sm-form-control required" />
                                </div>
                                <div class="col_one_third">
                                    <label for="template-contactform-email">Email<small>*</small></label>
                                    <input type="email" id="email" name="email" value="" class="required email sm-form-control" />
                                </div>
                                <div class="col_one_third col_last">
                                    <label for="template-contactform-phone">No. Handphone<small>*</small></label>
                                    <input type="text" id="phone" name="phone" value="" class="sm-form-control required" />
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="template-contactform-subject">Subjek<small>*</small></label>
                                    <input type="text" id="subject" name="subject" value="" class="required sm-form-control" />
                                </div>
                                <div class="clear"></div>
                                <div class="col_full">
                                    <label for="template-contactform-message">Pesan<small>*</small></label>
                                    <textarea class="required sm-form-control" id="message" name="message" rows="4" cols="30"></textarea>
                                </div>
                                <div class="col_full">
                                     <div class="g-recaptcha" data-sitekey="6LerZAsUAAAAAKGr1poO2iTXLlvM_tE6OJYmlle4" style="display: inline-block;"></div>
                                </div>
                                <div class="col_last">
                                    <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- .postcontent end -->
                </div>
            </div>
        </section>
        <div class="modal fade" id="modalSubscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Contact</h4>
                    </div>
                    <div class="modal-body">
                       Thanks for your feedback. We will contact you soon.
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalSubmit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Vclean Shoes Order</h4>
                    </div>
                    <div class="modal-body">
                       Success order 
                    </div>
                </div>
            </div>
        </div>
        <!-- #content end -->
        <!-- Footer
        ============================================= -->
        <footer id="footer">
            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">
                <div class="container clearfix">
                    <div class="col_half">
                        Copyrights &copy; 2017 All Rights Reserved by PopBox Asia Services.
                    </div>
                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="https://www.facebook.com/pboxasia" class="social-icon si-small si-light si-rounded si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="https://twitter.com/popbox_asia" class="social-icon si-small si-light si-rounded si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/popbox_asia/" class="social-icon si-small si-light si-rounded si-instagram">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <!-- #copyrights end -->
        </footer>
        <!-- #footer end -->
        <!-- #wrapper end -->
        <!-- Go To Top
    ============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>
        <!-- External JavaScripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!-- Footer Scripts
    ============================================= -->
        <script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/validation.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $("#processTabs").tabs({
                    show: {
                        effect: "fade",
                        duration: 400
                    }
                });
                $(".tab-linker").click(function() {
                    $("#processTabs").tabs("option", "active", $(this).attr('rel') - 1);
                    return false;
                });
            });
        </script>
        <script type="text/javascript">
            var lockerList = {!! json_encode($lockerJkt) !!};
            // City on change
            $('select[name=city]').on('change', function(event) {
                var city = $(this).val();
                var item = [];

                for (var i in lockerList) {
                    if(lockerList[i].city==city){
                        item.push(lockerList[i]);
                    } 
                }
                var option ='';
                for (var i = 0; i < item.length; i++) {
                    option+="<option value='"+item[i].name+"'>"+item[i].name+"</option>"
                }
                $('select[name=locker]').html(option);
                $('select[name=locker]').trigger('change');
            });

            $('select[name=locker]').on('change', function(event) {
                var name = $(this).val();
                var selectedLocker = null;
                for (var i in lockerList) {
                    if(lockerList[i].name==name){
                        selectedLocker = lockerList[i];
                    } 
                }
                if (selectedLocker!= null) {
                    var address = selectedLocker.address;
                    address += "\n"+selectedLocker.address_2;
                    address += "\n"+selectedLocker.operational_hours;

                    $('textarea[id=locker_address]').val(address);
                }
            });

            $(document).ready(function() {
                $('select[name=city]').trigger('change');
                // form omaisu submit
                $('#formSubmit').on('submit', function(event) {
                    event.preventDefault();
                    var form = $(this).serialize();
                    var rules = Array();
                    rules['cust_name'] = 'required|min:4';
                    rules['cust_email'] = 'required|min:4|email';
                    rules['cust_phone'] = 'required|min:4|numeric';
                    rules['shoes_total'] = 'required|numeric';
                    rules['shoes_brand'] = 'required';
                    rules['services'] = 'required';
                    rules['locker'] = 'required';

                    var isValid = validate($(this),rules);
                    if (isValid==false) {
                         return;
                    }
                    $.ajax({
                        url: "{{ url('vcleanshoes') }}",
                        data: $('#formSubmit').serialize(),
                        dataType:'json',
                        type: 'post',
                        async: true,
                        success: function(data){
                            if (data.isSuccess==true) {
                                // Show Modal
                                //hideLoading('#subscribe', 'load');
                                $('#formSubmit').find('.form-process').fadeOut();
                                $('#modalSubmit ').modal('show');
                                setTimeout(function() {
                                    location.reload();
                                }, 3000);
                            } else {
                                $('#formSubmit').find('.form-process').fadeOut();
                                //hideLoading('#subscribe', 'load');
                                alert(data.errorMsg)
                            }
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            $('#formSubmit').find('.form-process').fadeOut();
                        }
                    }).fail(function(){
                        //hideLoading('#subscribe', 'load');
                        $('#formSubmit').find('.form-process').fadeOut();
                        alert('Failed To Submit');
                        return;
                    });
                });

                // form contact submit
                $('#contactform').submit(function(event) {
                    event.preventDefault();
                    var rules = Array();
                    rules['name'] = 'required';
                    rules['email'] = 'required|email';
                    rules['message'] = 'required';
                    var isValid = validate($(this),rules);
                    if (isValid==false) {
                        return;
                    }
                    $.ajax({
                        url: "{{ url('contact') }}",
                        data: $('#contactform').serialize(),
                        dataType:'json',
                        type: 'post',
                        async: true,
                        success: function(data){
                            if (data.isSuccess==true) {
                                // Show Modal
                                //hideLoading('#subscribe', 'load');
                                $('#contactform').find('.form-process').fadeOut();
                                $('#modalSubscribe').modal('show');
                                $('input[name="email"]').val('');
                                $('input[name="name"]').val('');
                                $('input[name="message"]').val('');
                                $('input[name="subject"]').val('');

                            } else {
                                $('#contactform').find('.form-process').fadeOut();
                                //hideLoading('#subscribe', 'load');
                                alert(data.errorMsg)
                            }
                        },
                        error: function(data){
                            var errors = data.responseJSON;
                            $('#contactform').find('.form-process').fadeOut();
                        }
                    }).fail(function(){
                        //hideLoading('#subscribe', 'load');
                        $('#contactform').find('.form-process').fadeOut();
                        alert('Failed To Submit');
                        return;
                    });
                });
            });
        </script>
</body>

</html>
