@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')

 <link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 
 <section id="slider" class="boxed-slider">
            <div class="container clearfix">
                <img src="{{ asset('img/merchant/mereg-banner01.png') }}" alt="Image">
            </div>
        </section><br>
        <section id="section-welcome">
            <div class="container clearfix">
                <div class="center">
                    <h3>Lengkapi data berikut ini, tim PopBox akan melakukan verifikasi data 2-3 hari kerja dan kamu dapat segera menikmati layanan kami.</h3>
                </div>
            </div>
        </section>
        <section id="section-form">
            <div class="container clearfix">
                <form method="post" action="{{ url('merchant/register') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-full">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger xleft">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('error'))
                            <div class="alert alert-danger xleft">
                                <ul>
                                    <li>{{ \Session::get('error') }}</li>
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success xleft">
                                <ul>
                                    <li>{{ \Session::get('success') }}</li>
                                </ul>
                            </div>
                        @endif
                    </div>
                    
                    <div class="col_half">
                        <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;">
                            <label>Nama Toko Online : *</label>
                            <input type="text" name="merchant_name" id="merchant_name" placeholder="Input nama toko" class="sm-form-control" value="{{ old('merchant_name') }}">                            
                            <label>Nama Pemilik Toko / PIC : *</label>
                            <input type="text" name="owner_name" id="owner_name" placeholder="Input pemilik toko" class="sm-form-control" required="true" 
                                value="{{ old('owner_name') }}">
                            <label>Email : *</label>
                            <input type="text" name="email" id="email" class="sm-form-control" placeholder="Input email" required="true" value="{{ old('email') }}">  <label>Nomor Handphone : *</label>
                            <input type="text" name="phone" id="phone" class="sm-form-control" placeholder="Input phone" required="true" value="{{ old('phone') }}">                                </div>
                    </div>
                    <div class="col_half col_last">
                        <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;">                          
                            <label>Link Website / Toko Online</label>
                            <input type="text" name="website" id="website" placeholder="Input website"  class="sm-form-control" value="{{ old('website') }}">
                            <label>Tulis nama toko yang mereferensikan Anda untuk bergabung dengan PopBox</label>
                            <input type="text" name="merchant_referral" placeholder="input toko yang mereferensikan jika tidak ada kosongkan" id="merchant_referral" class="sm-form-control" value="{{ old('merchant_referral') }}">
                            <label>Logo Toko</label>
                            <input id="picture" name="picture" type="file">
                        </div>
                    </div>
                    <div class="col_full">
                        <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;">
                            <label>City</label>
                            <select class="form-control" id="city" name="city" value="{{ old('city') }}">
                                @foreach ($citiesList as $group)
                                    <optgroup label="{{ $group['Province'] }}">
                                        @foreach ($group['Cities'] as $city)
                                            <option value="{{ $city }}">{{ $city }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select><br/><br/>
                            <label>Alamat pengambilan : *</label>
                            <textarea class="sm-form-control" rows="5" name="address" placeholder="Input address" required="true">{{ old('address') }}</textarea>
                            <span>Tuliskan Alamat lengkap seperti : nama gedung, nomor, jalan, kecamatan, kota, kodepos</span> <br>
                            <label>Detail Alamat</label>
                            <input id="detail-address" name="detail-address" placeholder="Input detail address"  type="text" class="sm-form-control" value="{{ old('detail-address') }}">
                            <span>Tuliskan panduan untuk menemukan lokasi, misalkan, "lantai 21 blok J"</span>
                        </div>
                    </div>
                    <div class="col-full">
                        <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;">
                            <label>Jadwal Pengambilan Barang : </label>
                            <div>
                                 <input id="checkbox-6" class="checkbox-style" name="days[]" value="Senin" id="senin" type="checkbox"><label for="checkbox-6" class="checkbox-style-1-label">Senin
                                 <input id="checkbox-7" class="checkbox-style" name="days[]" value="Selasa" id="selasa" type="checkbox" ><label for="checkbox-7" class="checkbox-style-1-label">Selasa
                                 <input id="checkbox-8" class="checkbox-style" name="days[]" value="Rabu" id="rabu"type="checkbox" ><label for="checkbox-8" class="checkbox-style-1-label">Rabu
                                 <input id="checkbox-9" class="checkbox-style" name="days[]" value="Kamis" id="kamis" type="checkbox" ><label for="checkbox-9" class="checkbox-style-1-label">Kamis
                                 <input id="checkbox-10" class="checkbox-style" name="days[]" value="Jumat" id="jumat" type="checkbox" ><label for="checkbox-10" class="checkbox-style-1-label">Jumat
                                 <input id="checkbox-11" class="checkbox-style" name="days[]" value="Sabtu" id="sabtu" type="checkbox" ><label for="checkbox-11" class="checkbox-style-1-label">Sabtu
                               
                                </div>
                              <label>Preferensi waktu penjemputan paket: </label>
                            <div>
                                 <span class="clstime"><input type="radio" name="time" value="10:00 - 13:00">10:00 - 13:00</span>
                                 <input type="radio" name="time" value="13:00 - 16:00">13:00 - 16:00
                             </div>
                        </div>
                    </div>
                    <div class="col-full">
                        <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;">
                            <label>layanan yang  anda dapatkan adalah : *</label>
                            {{-- LastMile --}}
                            <div class="fitur-box">
                                <div class="pull-left">
                                    <h3>Pengiriman Barang (last mile)</h3>
                                     <span>Kurir PopBox akan mengambil barang dari alamat penjual dan mengirimkan ke lokasi locker pilihan customer. <br/>Berikan konsumen Anda kenyamanan baru untuk mengambil barang kapanpun dan dimanapun! Harga pengiriman Rp 7.500/Kg</span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                         
                        </div>
                    </div>
                    <div class="col_full">
                        <span>* Wajib diisi</span>
                        <div>
                            <input id="checkbox-5" class="checkbox-style" name="syarat" value="syarat" type="checkbox" required="true">
                            <label for="checkbox-5" class="checkbox-style-1-label">Dengan klik pada tombol "Daftar Sekarang" Anda telah menyatakan bahwa indormasi yang dituliskan adalah BENAR, dan Anda menyetujui <a href ="#"data-toggle="modal" data-target=".bs-example-modal-lg" >syarat dan ketentuan</a> yang berlaku.</label>
                        </div>
                        <div align="center">
                            <button class="button button-circle" type="submit">Daftar Sekarang</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>



        
@stop

@section('js')
<!-- Select-Boxes Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/select-boxes.js') }}"></script> 
<!-- Bootstrap Switch Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/bs-switches.js') }}"></script> 

 <script type="text/javascript"> 
    jQuery(document).ready(function($) { 
        

        @if (is_array(old('days')))
            @if(in_array("Senin", old('days'))) 
                $("#checkbox-6").attr("checked", "checked");
            @endif
            @if(in_array("Selasa", old('days'))) 
                $("#checkbox-7").attr("checked", "checked");
            @endif
            @if(in_array("Rabu", old('days'))) 
                $("#checkbox-8").attr("checked", "checked");
            @endif
            @if(in_array("Kamis", old('days'))) 
                $("#checkbox-9").attr("checked", "checked");
            @endif
            @if(in_array("Jumat", old('days'))) 
                $("#checkbox-10").attr("checked", "checked");
            @endif
            @if(in_array("Sabtu", old('days'))) 
                $("#checkbox-11").attr("checked", "checked");
            @endif
        @endif
        
        $('input[name="time"][value="{{old('time')}}"]').prop('checked', true);            
        
        

        @foreach ($errors->all() as $error)
            @if (strpos($error, 'owner_name') !== false)
                $("input[name='owner_name']").addClass("active");
                $("input[name='owner_name']").focus();
            @endif

            @if (strpos($error, 'email') !== false)
                $("input[name='email']").addClass("active");            
                $("input[name='email']").focus();
            @endif
            @if (strpos($error, 'phone') !== false)
                $("input[name='phone']").addClass("active");            
                $("input[name='phone']").focus();
            @endif
            @if (strpos($error, 'merchant_name') !== false) 
                $("input[name='merchant_name']").addClass("active");            
                $("input[name='merchant_name']").focus();
            @endif
            @if (strpos($error, 'address') !== false) 
                $("input[name='address']").addClass("active");            
                $("input[name='address']").focus();
            @endif
            @if (strpos($error, 'detail_address') !== false) 
                $("input[name='detail_address']").addClass("active");            
                $("input[name='detail_address']").focus();
            @endif    
        @endforeach
        
        @if (strpos(\Session::get('error'), 'owner_name') !== false)
            $("input[name='owner_name']").addClass("active");
            $("input[name='owner_name']").focus();
        @endif
        @if (strpos(\Session::get('error'), 'email') !== false)
            $("input[name='email']").addClass("active");            
            $("input[name='email']").focus();
        @endif
        @if (strpos(\Session::get('error'), 'phone') !== false)
            $("input[name='phone']").addClass("active");            
            $("input[name='phone']").focus();
        @endif
        @if (strpos(\Session::get('error'), 'merchant_name') !== false) 
            $("input[name='merchant_name']").addClass("active");            
            $("input[name='merchant_name']").focus();
        @endif
        @if (strpos(\Session::get('error'), 'address') !== false) 
            $("input[name='address']").addClass("active");            
            $("input[name='address']").focus();
        @endif
        @if (strpos(\Session::get('error'), 'detail_address') !== false) 
            $("input[name='detail_address']").addClass("active");            
            $("input[name='detail_address']").focus();
        @endif        


        $('#city').select2(); 
        $(".bt-switch").bootstrapSwitch(); 
    }); 
</script> 
@stop