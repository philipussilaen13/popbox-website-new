@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')

<section id="slider" class="boxed-slider">
    <div class="container clearfix">
        <img src="{{ asset('img/merchant/mereg-banner02.png') }}" alt="Image">
    </div>
</section><br>
<section id="section-welcomt">
    <div class="container clearfix">
        <div class="center">
            <h3>Menjadi merchant/partner PopBox, bantu konsumen untuk mengambil paket kamu dengan lebih MUDAH, AMAN, CEPAT, dan NYAMAN</h3>
        </div>
    </div>
</section>
<section id="section-untung">
    <div class="container clearfix">
        <div class="heading-block center">
            <h3>KEUNTUNGAN MENJADI MITRA POPBOX</h3>
        </div>
        <div class="col_half">
            <div class="feature-box media-box">
                <div class="fbox-media" align="center">
                    <img src="{{ asset('img/merchant/icon03.svg') }}" style="width: 30%; height: auto">
                </div>
                <div class="emphasis-title bottommargin-sm">
                    <h3 style="font-size: 20px" class="font-secondary ls1 t400 center">Free Pick Up</h3>
                </div>
                <p style="font-size: 18px; color: #444;" class="ls1 center t300">
                    Kurir PopBox akan mengambil barang tanpa minimum order
                </p>
            </div>
        </div>
        <div class="col_half col_last">
            <div class="feature-box media-box">
                <div class="fbox-media" align="center">
                    <img src="{{ asset('img/merchant/icon02.svg') }}" style="width: 30%; height: auto">
                </div>
                <div class="emphasis-title bottommargin-sm">
                    <h3 style="font-size: 20px" class="font-secondary ls1 t400 center">Tarif Hemat</h3>
                </div>
                <p style="font-size: 18px; color: #444;" class="ls1 center t300">
                    Harga pengiriman Rp 7.500/Kg <br>
                    Untuk promo khusus klik <a href="{{ url('promo') }}">disini</a>
                </p>
            </div>
        </div>
        <div class="col_half">
            <div class="feature-box media-box">
                <div class="fbox-media" align="center">
                    <img src="{{ asset('img/merchant/icon01.svg') }}" style="width: 30%; height: auto">
                </div>
                <div class="emphasis-title bottommargin-sm">
                    <h3 style="font-size: 20px" class="font-secondary ls1 t400 center">Monitor Order</h3>
                </div>
                <p style="font-size: 18px; color: #444;" class="ls1 center t300">
                    Merchant akan mendapatkan akses dashboard khusus untuk memantau kiriman
                </p>
            </div>
        </div>
        <div class="col_half col_last">
            <div class="feature-box media-box">
                <div class="fbox-media" align="center">
                    <img src="{{ asset('img/merchant/icon04.svg') }}" style="width: 30%; height: auto">
                </div>
                <div class="emphasis-title bottommargin-sm">
                    <h3 style="font-size: 20px" class="font-secondary ls1 t400 center">Promosi Promo</h3>
                </div>
                <p style="font-size: 18px; color: #444;" class="ls1 center t300">
                    Event dan promo yang PopBox berikan sebagai solusi marketing
                </p>
            </div>
        </div>
    </div>
</section>
<section id="section-cara">
    <div class="container clearfix">
        <div class="heading-block center">
            <h3>Cara menjadi mitra popbox</h3>
        </div>
        <div class="col_half">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon">
                    <i class="i-alt">1.</i>
                </div>
                <h3 style="font-size: 20px" class="font-secondary ls1 t400">Daftarkan Online Shop</h3>
                <p style="font-size: 18px; color: #444;" class="ls1 t300">
                    Isi data lengkap tentang online shop kamu <a href="{{ url('merchant/register') }}">di halaman ini</a>.
                </p>
            </div>
        </div>
        <div class="col_half col_last">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon">
                    <i class="i-alt">2.</i>
                </div>
                <h3 style="font-size: 20px" class="font-secondary ls1 t400">Verifikasi Data</h3>
                <p style="font-size: 18px; color: #444;" class="ls1 t300">
                    Tim partnership kami akan memverifikasi data.
                </p>
            </div>
        </div>
        <div class="col_half">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon">
                    <i class="i-alt">3.</i>
                </div>
                <h3 style="font-size: 20px" class="font-secondary ls1 t400">Akses Memasukkan Order</h3>
                <p style="font-size: 18px; color: #444;" class="ls1 t300">
                    Tim PopBox akan mengirimkan akses untuk memasukkan data order.
                </p>
            </div>
        </div>
        <div class="col_half col_last">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon">
                    <i class="i-alt">4.</i>
                </div>
                <h3 style="font-size: 20px" class="font-secondary ls1 t400">Pembayaran</h3>
                <p style="font-size: 18px; color: #444;" class="ls1 t300">
                    Tim finance kami akan menghubungi untuk invoice pengiriman.
                </p>
            </div>
        </div>
    </div>
</section>
<section id="section-box">
    <div class="container clearfix">
        <div class="col_full center" style="background-color: #E5E5E5; padding-top: 10px; padding-bottom: 10px;">
            <h3 style="margin: 10px;">Daftarkan toko online kamu sekarang dapatkan pengiriman special dari PopBox!</h3>
            <a href="{{ url('merchant/register') }}"><button class="button button-circle">Daftar Sekarang</button></a>
            <h3 style="margin: 10px;">Masih ada yang belum jelas? Ingin mengetahui penjelasan lebih lengkap?</h3>
            <button data-toggle="modal" data-target=".bs-example-modal-lg" class="button button-circle" style="word-wrap: normal; white-space: normal; height: auto;">Baca Syarat dan Ketentuan lebih detail</button>
        </div>
    </div>
</section>

@extends('layout.syarat')
        
@stop