@extends('layout.main-article')
@section('meta')
	<meta name="description" content="{{ $articleDb->subtitle }}">
    <meta name="keywords" content="PopBox,Parcel,Locker,Loker,Kirim Barang,Logistik,Logistic">
    {{-- Schema.org for G+ --}}
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="PopBox Asia">
    <meta itemprop="description" content="{{ $articleDb->subtitle }}">
    <meta itemprop="image" content="{{\App\Http\Helper\Helper::createImgUrl('article',$articleDb->image)}}">
    {{-- Twitter Card data --}}
    <meta name="twitter:card" content="{{ $articleDb->subtitle }}">
    <meta name="twitter:site" content="@PopBox_Asia">
    <meta name="twitter:title" content="{{ $articleDb->title }}">
    <meta name="twitter:description" content="{{ $articleDb->subtitle }}">
    <meta name="twitter:creator" content="@PopBox_Asia">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{\App\Http\Helper\Helper::createImgUrl('article',$articleDb->image)}}">
    {{-- OpenGraph --}}
    <meta property="og:title" content='{{ $articleDb->title }}'>
    <meta property="og:image" content="{{\App\Http\Helper\Helper::createImgUrl('article',$articleDb->image)}}">
    <meta property="og:description" content="{{ $articleDb->subtitle }}">
    <meta property="og:url" content="{{ url('article') }}/{{ date('Ymd',strtotime($articleDb->created_date)) }}/{{ $articleDb->short_title }}/{{ $articleDb->id_article }}"
@stop
@section('content')
	<section id="content">
		<div class="content-wrap">
			<div class="container clearfix" style="padding-top: 0">
				<div class="single-post nobottommargin">
					{{-- Single Post --}}
					<div class="entry clearfix">
						{{-- Title --}}
						<div class="entry-title">
							<h2>{{ $articleDb->title }}</h2>
						</div>
						{{-- Meta --}}
						<ul class="entry-meta clearfix">
							<li><i class="icon-calendar3"></i> {{ date('D, j M Y', strtotime($articleDb->created_date)) }}</li>
							<li><i class="icon-folder-open"></i> <a href="#">{{ ucfirst($articleDb->type) }}</a></li>
						</ul>
						{{-- Image --}}
						<div class="entry-image bottommargin" >
							<div style="height: auto;display: block; margin-left: auto; margin-right: auto; width: 50%;">
								<img src="{{ 'http://dashboard.popbox.asia/article/'.$articleDb->image }}" alt="Blog Image">
							</div>
							{{--<a href="#"><img src="{{\App\Http\Helper\Helper::createImg('article',$articleDb->image)}}" alt="Blog Single"></a>--}}
						</div>
						{{-- Content --}}
						<div class="entry-content notopmargin">
							{!! $articleDb->content !!}
						</div>
						<div class="clear"></div>
						{{-- Share --}}
						{{-- Go to www.addthis.com/dashboard to customize your tools  --}}
						{{-- <div class="si-share noborder clearfix">
							<span>Share this Post:</span>
							<div class="addthis_inline_share_toolbox"></div>
						</div><!-- Post Single - Share End --> --}}
					</div>
				</div>
			</div>
		</div>
	</section>


@stop