@extends('layout.main-article')
@section('content')
    <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('img/bg/bg-testimoni.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
        <div class="container clearfix">
            <h1>Testimonials</h1>
        </div>
    </section>
	<section id="content">
		<div class="content-wrap">
			<div class="container clearfix">
				<ul class="testimonials-grid clearfix">
					@if (!empty($testimonials))
                        @foreach ($testimonials as $element)
                             <li>
                                <div class="testimonial">
                                    <div class="testi-image">
                                        <a href="#"><img src="{{\App\Http\Helper\Helper::createImgLocal('article',$element->image)}}" alt="Customer Testimonails"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>{{ strip_tags($element->content) }}</p>
                                        <div class="testi-meta">
                                            {{ $element->title }}
                                            <span>{{ $element->subtitle }}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endif
				</ul>
			</div>
		</div>
	</section>
@stop