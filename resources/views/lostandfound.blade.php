@extends('layout.main-article')
@section('content')
    <style type="text/css">
    	@media only screen and (min-width:480px){
    		.merchant{
    			margin-right: 0px; 
    			width: 33.3333%; 
    		}
    	}
    	.card{
	        width: 100%;
	        min-height: 400px;
	        padding: 15px 15px;
	        border: 1px solid #DDDDDD;
	        border-radius: 10px;
	        margin-top: 30px;
	        text-align: center;
	        background: #fff;
	   }

	   .landing-page .section-popbox .card{
	        width: 100%;
	        min-height: 340px;
	        padding: 10px 0 0px 0;
	        border: 1px solid #DDDDDD;
	        border-radius: 0px;
	        margin-top: 30px;
	        text-align: center;
	        background: #FFFFFF;
	   }

	   .landing-page .section-benefit .card{
	        width: 100%;
	        min-height: 340px;
	        padding: 10px 0 0px 0;
	        border-radius: 0px;
	        margin-top: 30px;
	        text-align: center;
	   }

	   .card .icon i{
	        font-size: 63px;
	        color: #9a9a9a;
	        width: 85px;
	        height: 85px;
	        line-height: 80px;
	        text-align: center;
	        border-radius: 50%;
	        margin: 0 auto;
	        margin-top: 20px;
	   }

	    .card.card-blue{
	      border-color: #0C87CC;
	    }
	    .card.card-blue .icon i{
	      color:#00bbff;
	    }

	    .landing-page .section-popbox .card.card-white{
	      border-color: #FFFFFF;
	    }
	    .landing-page .section-popbox .card.card-blue{
	      border-color: #0C87CC;
	      background-color: #0C87CC;
	      color: #ffffff;
	    }
	    .card.card-white .icon i{
	      color:#00bbff;
	    }

	    .card.card-green{
	      border-color: #BDF8C0;
	    }
	    .card.card-green .icon i{
	      color:#3ABE41;
	    }
	    .card.card-orange{ 
	      border-color: #FAD9CD;
	    }
	    .card.card-orange .icon i{
	      color:#ff967f;
	    }
	     .card.card-red{ 
	      border-color: #FFBFC3;
	    }
	    .card.card-red .icon i{
	      color:#E01724;
	    }
	    .card  h4{
	        font-weight: 300;
	        font-size: 18px;
	        margin: 20px 0 15px;
	        color: #000;
	   }

	   .card  p{
	        font-size: 14px;
	        line-height: 22px;
	        font-weight: 400;
	        color: #000;
	   }
        .gMap-controls{
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 25px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            width: 30%;
        }

        .gMap-controls:focus{
            box-shadow: 0 0 5px rgba(12, 135, 204, 1);
        }

        #pac-input{
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
        }
	.title-lost {
		margin-top:-300px; margin-left:200px;
	}
	@media only screen and (max-width:480px){
    		.title-lost {
			margin-top:-300px;
			margin-left:40px;
		}
    	}

    </style>
    <link rel="icon" type="image/png" href="img/favicon.ico">
    <section>
	<img src='img/banner.png' class='img-responsive' style="height:500px;">
	<div class="title-lost">
            <h1 style="color: #fff; font-size: 40px; font-weight: 400; letter-spacing: 2px;">LOST AND FOUND</h1>
        </div>
    </section>

    
    <section id="content">
	    <div class="content-wrap">
			<div class="container clearfix">
				<div class="nobottommargin">
					<div class="col_full clearfix">

						<h4>
<!--
							Berikut daftar lost and found PopBox dan PopExpress per 13 Juni 2018, bagi yang merasa memiliki barang silahkan langsung menghubungi menghubungi kami lewat email ke info@popbox.asia dengan subject "Lost And Found [No AWB]". 
<br><br>
Jika sampai 31 Juli 2018 belum ada klaim terhadap barang ini, maka pihak PopBox akan melakukan pemusnahan.			
				-->

Pelanggan Yang Terhormat,<br><br>



Sehubungan telah berlalunya masa simpan yang kami lakukan selama lebih dari tiga bulan tanpa ada pengakuan, serta telah dilakukannya usaha untuk menghubungi pelanggan sesuai dengan kontak yang terdapat pada database kami, maka bersama ini dapat kami sampaikan, bagi pelanggan PopBox dan PopExpress pemilik Kiriman terlampir, agar menghubungi kami melalui email <a href="mailto:info@popbox.asia" style="color:#0096ff;">info@popbox.asia</a> dengan subject <span  style="color:#0096ff;"><b>"Lost And Found"</b></span> dengan melampirkan No AWB.<br><br>

Apabila dalam jangka waktu 3 minggu setelah tanggal pemberitahuan ini, tidak terjadi pengakuan apapun dari pemilik, maka <span  style="color:#0096ff;">PopBox dan PopExpres</span> tidak bertanggung jawab atas keberadaan barang kiriman tersebut. <br><br>

Terima kasih.<br><br>

Jakarta, 14 Juni 2018<br>
<span  style="color:#0096ff;">PopBox dan PopExpres</span><br><br>


Customer Experience Management

						</h3>
					</div>
					<div class="row" align="center">
						<div class="col-md-6 col-md-offset-3" style="text-align:center;">
						       <table class="table table-bordered">
        <thead>
        <tr>
            <th style="text-align:center;">No.</th>
            <th style="text-align:center;">No. Resi</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>101211700031454	</td>
        </tr>
        <tr>
            <td>2</td>
            <td>13870556166917</td>
        </tr>
        <tr>
            <td>3</td>
            <td>PLL170602171143	</td>
        </tr>
        <tr>
            <td>4</td>
            <td>RN3121674892140601	</td>
        </tr>
        <tr>
            <td>5</td>
            <td>00006621209200280</td>
        </tr>
        <tr>
            <td>6</td>
            <td>071270038644417	</td>
        </tr>
        <tr>
            <td>7</td>
            <td>PAA1709130063<br>(013870660884017)	</td>
        </tr>
        <tr>
            <td>8</td>
            <td>2150316641917	</td>
        </tr>
        <tr>
            <td>9</td>
            <td>010280569304517	</td>
        </tr>
        <tr>
            <td>10</td>
            <td>0/61856816</td>
        </tr>
        <tr>
            <td>11</td>
            <td>010591469187417	</td>
        </tr>
        <tr>
            <td>12</td>
            <td>012710271362317	</td>
        </tr>
        <tr>
            <td>13</td>
            <td>013780146298917	</td>
        </tr>
        <tr>
            <td>14</td>
            <td>CGK17000007163	</td>
        </tr>
        <tr>
            <td>15</td>
            <td>CGK01170001599	</td>
        </tr>
        <tr>
            <td>16</td>
            <td>150790036891117	</td>
        </tr>
        <tr>
            <td>17</td>
            <td>RN3538372272250801	</td>
        </tr>
        <tbody>
    </table>            
						</div>
					</div>
				</div>
			</div>
		</div>	
   </section>
@stop

@section('js')
@stop
