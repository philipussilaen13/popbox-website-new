@extends('layout.main-article')
@section('content')
    <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('img/bg/bg-contact.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
        <div class="container clearfix">
            <h1>{{ trans('contact.title') }}</h1>
            <span>{{ trans('contact.sub') }}</span>
        </div>
    </section>
    <section id="google-map" class="gmap slider-parallax"></section>
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <!-- Postcontent
				============================================= -->
                <div class="postcontent nobottommargin divcenter">
                    <div class="rows">
                    <!-- Contact Info
                    ============================================= -->
                    <div class="col_half">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="icon-map-marker2"></i></a>
                                </div>
                               <h3><span class="subtitle">{!! trans('contact.contact_address') !!}
                                </span></h3>
                            </div>
                        </div>
                        <div class="col_half col_last">
                            <div class="feature-box fbox-center fbox-bg fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="icon-phone3"></i></a>
                                </div>
                                <h3>{{ trans('contact.call') }}<span class="subtitle">
                                <a href="tel:{{ trans('contact.contact-phone') }}"><i class="icon-call i-alt"></i>&nbsp; {{ trans('contact.contact-phone') }}</a><br>
                                <a href="mailto:info@popbox.asia"><i class="icon-mail i-alt"></i>&nbsp; info@popbox.asia</a>

                                </h3>
                                <div class="clear"></div>
                            </div>
                        </div>
                    <!-- Contact Info End -->
                    <div class="contact-widget col_full divcenter">
                        <div class="contact-form-result"></div>
                        <form class="nobottommargin" id="contactform">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-process"></div>
                            <div class="col_one_third">
                                <label for="template-contactform-name">{{ trans('contact.input-name') }} <small>*</small></label>
                                <input type="text" id="name" name="name" value="" class="sm-form-control required" />
                            </div>
                            <div class="col_one_third">
                                <label for="template-contactform-email">{{ trans('contact.input-email') }} <small>*</small></label>
                                <input type="email" id="email" name="email" value="" class="required email sm-form-control" />
                            </div>
                            <div class="col_one_third col_last">
                                <label for="template-contactform-phone">{{ trans('contact.input-hp') }}<small>*</small></label>
                                <input type="text" id="phone" name="phone" value="" class="sm-form-control required" />
                            </div>
                            <div class="clear"></div>
                            <div class="col_full">
                                <label for="template-contactform-subject">{{ trans('contact.input-subjek') }} <small>*</small></label>
                                <input type="text" id="subject" name="subject" value="" class="required sm-form-control" />
                            </div>
                            <div class="clear"></div>
                            <div class="col_full">
                                <label for="template-contactform-message">{{ trans('contact.input-message') }} <small>*</small></label>
                                <textarea class="required sm-form-control" id="message" name="message" rows="4" cols="30"></textarea>
                            </div>
                            <div class="col_full">
                                 <div class="g-recaptcha" data-sitekey="6LerZAsUAAAAAKGr1poO2iTXLlvM_tE6OJYmlle4" style="display: inline-block;"></div>
                            </div>
                            <div class="col_last">
                                <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">{{ trans('contact.input-btn') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- .postcontent end -->
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="modalSubscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('contact.modal-title')}}</h4>
                </div>
                <div class="modal-body">
                    {{ trans('contact.modal-content') }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijkBYFlPmwsLEAfvljffdrnhM7EStYF4&libraries=places"></script>
    <script type="text/javascript" src="{{ elixir('js/jquery.gmap.js') }}"></script>
	<script type="text/javascript">
	    $('#google-map').gMap({
	        address: '{{ trans('contact.contact-location') }}',
	        maptype: 'ROADMAP',
	        zoom: 16,
	        center: '{{ trans('contact.contact-location') }}',
	        markers: [{
	            address: '{{ trans('contact.contact-location') }}',
	            html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">Hi, we\'re <span>PopBox</span></h4><p class="nobottommargin"></div>',
	            icon: {
	                image: "img/icon/icon-marker-red.png",
	                iconsize: [57, 100],
	                iconanchor: [68, 160]
	            }
	        }],
	        doubleclickzoom: false,
	        controls: {
	            panControl: true,
	            zoomControl: true,
	            mapTypeControl: true,
	            scaleControl: false,
	            streetViewControl: false,
	            overviewMapControl: false
	        }
	    });
	    jQuery(document).ready(function($) {
	    	$('#contactform').submit(function(event) {
	    		event.preventDefault();
	    		var rules = Array();
                rules['name'] = 'required';
                rules['email'] = 'required|email';
                rules['message'] = 'required';
                var isValid = validate($(this),rules);
                if (isValid==false) {
                    return;
                }
                $.ajax({
                    url: "{{ url('contact') }}",
                    data: $('#contactform').serialize(),
                    dataType:'json',
                    type: 'post',
                    async: true,
                    success: function(data){
                        if (data.isSuccess==true) {
                            // Show Modal
                            //hideLoading('#subscribe', 'load');
                            $('#contactform').find('.form-process').fadeOut();
                            $('#modalSubscribe').modal('show');
                            $('input[name="email"]').val('');
                            $('input[name="name"]').val('');
                            $('input[name="message"]').val('');
                            $('input[name="subject"]').val('');

                        } else {
                            $('#contactform').find('.form-process').fadeOut();
                            //hideLoading('#subscribe', 'load');
                            alert(data.errorMsg)
                        }
                    },
                    error: function(data){
                        var errors = data.responseJSON;
                        $('#contactform').find('.form-process').fadeOut();
                    }
                }).fail(function(){
                    //hideLoading('#subscribe', 'load');
                    $('#contactform').find('.form-process').fadeOut();
                    alert('Failed To Submit');
                    return;
                });
	    	});
	    });
    </script>
@stop