<!doctype html>
<html lang="en">
<head>
    <title>PopWarung</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="PopWarung">
    <meta name="keywords" content="popwarung, popbox">

    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link href="{{ asset('popwarungasset/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('popwarungasset/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('popwarungasset/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('popwarungasset/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('popwarungasset/css/custom.css') }}" rel="stylesheet">

</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

<div class="nav-menu fixed-top bg-black">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-dark navbar-expand-lg">
                    <a class="navbar-brand label-brand" href="{{ url('/') }}"><img src="popwarungasset/images/logo.png" alt="phone" class="img-fluid"></a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"> <a class="nav-link active" href="#home">HOME <span class="sr-only">(current)</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#keuntungan">KEUNTUNGAN</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#fitur">FITUR</a> </li>
                            <li class="nav-item"><a class="nav-link btn btn-daftar color-white" href="#daftar" >DAFTAR SEKARANG</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<header class="bg-gradient" id="home">
    <div class="img-holder mt-5" style="padding-top: 20px;"><img src="popwarungasset/images/banner.png" alt="phone" class="img-fluid"></div>
</header>

<div class="section pad-top-bot-80" id="keuntungan">
    <div class="container">
        <div class="section-title">
            <h3 class="color-black">Keuntungan Mitra PopWarung</h3>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <ul class="list-green">
                    <li><span>Tanpa Modal Awal</span></li>
                    <li><span>Keuntungan 100% untuk Mitra</span></li>
                    <li><span>Potensi Profit > 5jt/bulan </span></li>
                    <li><span>Bagian dari Pengusaha Digital</span></li>
                </ul>
            </div>
            <div class="col-12 col-lg-6">
                <ul class="list-green">
                    <li><span>Didukung Sistem Teknologi Warung Terkini</span></li>
                    <li><span>Monitoring Stock, Penjualan, dan Keuntungan RealTime</span></li>
                    <li><span>Stock Barang Dikirim</span></li>
                    <li><span>Jaminan Harga Kompetitif</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="section bg-black-gray pad-top-80" id="fitur">
    <div class="container">
        <div class="section-title">
            <h3 class="color-white">Fitur PopWarung</h3>
        </div>
        <div class="row">
            <div class="col-12 col-lg-4 col-md-4 col-sm-12">
                <center><img src="popwarungasset/images/fitur1.png" class="img-fluid"></center>
            </div>
            <div class="col-12 col-lg-8 col-md-8 col-sm-12">
                <img src="popwarungasset/images/fitur2.png" class="img-fluid">
            </div>
        </div>
    </div>
</div>

<div class="section bg-gradient pad-top-80" id="daftar">
    <div class="container">
        <div class="call-to-action">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSe71lL4TqumsqTH_V9LmKE2T7Q3kprP1psyOekkSxSKhrg70Q/viewform?embedded=true" width="100%" height="1000" frameborder="0">Loading...</iframe>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>

<div class="bg-black" id="contact" style="margin-right: 20px;">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-6 text-center text-lg-left color-white" style="font-size: 12px; padding-top: 40px;">
                <img src="popwarungasset/images/logo.png" class="img-fluid"><br><br>
                <p class="mb-2"> <img src="popwarungasset/images/phone.png"> 0815 1897 889</p>
                <p class="mb-2"> <img src="popwarungasset/images/mail.png"> info@popbox.asia</p>
                <p class="mb-2"> <img src="popwarungasset/images/map.png"> &nbsp; Jl. Palmerah Utara III no 62 FGH Palmerah<br>
                    <span style="padding-left: 25px;">&nbsp;</span>Jakarta Barat 11480</p>
                <p class="mb-2"> &copy; 2018 All Rights Reserved by Popbox Asia Services </p>
            </div>
            <div class="col-lg-4" style="text-align: right;">
                <img src="popwarungasset/images/footer.png">
            </div>
        </div>
</div>

<script src="{{ asset('popwarungasset/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('popwarungasset/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('popwarungasset/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('popwarungasset/js/script.js') }}"></script>
</body>

</html>
