@extends('layout.main-article')
@section('content')
<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('img/jobs/bg-jobs.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
	<div class="container clearfix">
		<h1>{{ trans('career.title') }}</h1>
		<span>{{ trans('career.sub') }}</span>
	</div>
</section>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nobottommargin">
				<div class="col_three_fifth nobottommargin">
					@if (!empty($careerDb))
						@foreach ($careerDb as $element)
							<div class="fancy-title title-bottom-border">
								<h3>{{ $element->job_title }}</h3>
							</div>
							<p>{{ $element->description }}.</p>
							<div class="accordion accordion-bg clearfix">
								<div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Requirements</div>
								<div class="acc_content clearfix">
									{!! $element->requirement !!}
								</div>
								<div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>What we Expect from you?</div>
								<div class="acc_content clearfix">
									{!! $element->job_desc !!}
								</div>
								For interested candidate, please email your latest CV to <a href="mailto:HRD@popbox.asia">HRD@popbox.asia</a>  with subject [{{ $element->job_title }}]
							</div>
							<div class="divider divider-short"><i class="icon-star3"></i></div>
						@endforeach
					@endif
				</div>
				{{-- <div class="col_two_fifth nobottommargin col_last">
					<p>
						
					</p>
				</div> --}}
			</div>
		</div>
	</div>
</section>
@stop