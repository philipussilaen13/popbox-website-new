@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
<link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 
<div class="row row-upload">
    <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT ORDER BARU</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/panduan">
                        Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-9">  
        <div class="title-right-content">Buat Order Baru</div>
            <section id="section-tab" class="right-content">   
                <div class="row row-tab">
                    <div class="col-md-2 tab">
                        <a href="/dashboards/uploadpickup">BULK ORDER</a>
                    </div>
                    <div class="col-md-2 tab active">
                        <a href="/dashboards/uploadpickupsingle">SINGLE ORDER</a>
                    </div>
                    <div class="col-md-8">                        
                    </div>
                </div>
            
                <div class="clearfix">                
                    <form method="post" action="{{ url('dashboards/postpickup') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-full">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (\Session::has('error'))
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{ \Session::get('error') }}</li>
                                    </ul>
                                </div>
                            @endif
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{{ \Session::get('success') }}</li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="col_full cls-content-dashboard">
                            <div class="feature-box fbox-center fbox-border fbox-bg fbox-effect" style="margin-top: 0px; padding: 25px 30px 30px;text-align: left;">                            
                                <label>Nama Konsumen : *</label>
                                {{ Form::text('customer_name', old('customer_name'), ['class' => 'sm-form-control', 'required' => 'true']) }}
                                <label>Nomor HP Konsumen : *</label>
                                {{ Form::text('phone', old('phone'), ['class' => 'sm-form-control', 'required' => 'true']) }}
                                <div class="phone-notify">
                                    Contoh : 085221597333<br/>
                                    Pastikan No HP Konsumen BENAR, no HP ini akan
                                    dikirimkan SMS notifikasi pengambilan barang.
                                </div>
                                <br/>

                                <label>Pilih Kota : *</label>
                                <select class="sm-form-control" name="city-locker">
                                      <option>Pilih Kota</option>
                                   @foreach ($lockerBycities as $key => $value)
                                        @if (
                                                (strtolower($key)=="depok") || 
                                                (strtolower($key)=="tangerang") || 
                                                (strtolower($key)=="bogor") ||
                                                (strtolower($key)=="cibinong") ||
                                                (strtolower($key)=="bekasi") ||
                                                ((strpos(strtolower($key), 'jakarta') !== false)) 
                                            )
                                            <option>{{$key}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <br/>
                                <label>Pilih Loker : *</label>
                                <select name="popbox_location" class='sm-form-control'>
                                 
                                </select>   

                                <label>Email konsumen *</label>
                                {{ Form::text('customer_email', old('customer_email'), ['class' => 'sm-form-control']) }}
                           
                                <label>Alamat Pengambilan  : *</label><br/>
                                {{ Form::textarea('pickup_location', $address, ['class' => 'sm-form-control','rows' => 3]) }}                            
                                
                                <label>Detail Items</label>
                                {{ Form::text('detail_items', old('detail_items'), ['class' => 'sm-form-control']) }}

                                <label>Harga</label>
                               {{ Form::text('price', old('price'), ['class' => 'sm-form-control']) }}
                               
                            </div>
                            <div align="center">
                                <button class="button button-circle" type="submit">Buat Order</button>
                            </div>
                            <div class="note-pickup">
                                Informasi halaman<br/>
                                + Halaman ini digunakan untuk mengirimkan data pickup ke tim logistik PopBox.<br/><br/>
                                 Catatan :<br/>
                                + Informasi lebih lanjut silakan menghubungi logistic@popbox.asia atau ke nomor 021-29022537/8.<br/>
                                + Konsumen akan menerima SMS dengan kode PIN setelah barang sampai di loker.<br/>
                                + Barang akan berada di loker maksimal 3 hari setelah barang di drop.<br/>
                                + Pastikan nomor telepon yang dituliskan di atas benar dan menggunakan nomor HP (bukan nomor telepon rumah/kantor).<br/>
                                + Barang-barang yang ilegal (obat, senjata tajam, barang yang mudah meledak, binatang, tanaman hidup, pecah belah, barang/surat berharga, dll) dilarang untuk dikirimkan via PopBox.<br/>                               
                                + Kurir PopBox akan mengambil paket sesuai dengan waktu preferensi yang kamu tentukan sebelumnya.<br/>
                                + Pastikan paket kamu kirimkan memenuhi <span id="syarat">syarat dan ketentuan</span> dari PopBox.<br/>
                            </div>
                        </div>
                        
                            
                        
                    </form>
                </div>
            </section>
        </div>
</div>



        
@stop

@section('js')
<!-- Select-Boxes Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/select-boxes.js') }}"></script> 
<!-- Bootstrap Switch Plugin --> 
<script type="text/javascript" src="{{ asset('js/components/bs-switches.js') }}"></script> 

 <script type="text/javascript"> 

    var lockerList = {!! json_encode($lockerJkt) !!};
    var lockerByCities = new Array();
    <?php foreach ($lockerBycities as $key => $value): ?>
        lockerByCities['<?php echo $key ?>']= <?php echo ($value); ?>;
    <?php endforeach ?>

    var arrLockers = new Array();
    <?php foreach ($lockers as $key => $value): ?>          
        arrLockers['<?php echo $value->name ?>']= <?php echo (json_encode($value)); ?>;
    <?php endforeach ?>
    var lockername = "";
    var newLoc = false;

    jQuery(document).ready(function($) { 
        $("select[name='city-locker']").change(function(){
            $('select[name="popbox_location"]').empty();
            $.each(lockerByCities[$(this).val()], function( index, value ){                   
                $('select[name="popbox_location"]').append($('<option/>', { 
                    value: value.name,
                    text : value.name
                }));
            });
        })        
    }); 
</script> 
@stop