@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
<link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 
<div class="row row-upload">
    <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT ORDER BARU</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/panduan">
                        Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-9">  
        <div class="title-right-content">Buat Order Baru</div>
        <section id="section-tab" class="right-content">             
            <div class="row row-tab">
                <div class="col-md-2 tab active">
                        <a href="/dashboards/uploadpickup">BULK ORDER</a>
                </div>
                <div class="col-md-2 tab">
                        <a href="/dashboards/uploadpickupsingle">SINGLE ORDER</a>
                </div>
                <div class="col-md-8">                        
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="center">
                        <div class="col-full">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger xleft">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="alert xleft" style="display: none;">
                                <ul>
                                    <li></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

                @if (isset($merchant))
                    @if (isset($user->active))             
                        {!!Form::open(['method' => 'post', 'url' => 'dashboards/uploadpickupxls', 'id'=>'formupload' , 'files' => true]) !!}
                         {{ csrf_field() }}
                    @endif
                @endif
                <div class="col-full">
                    <input type="file" name="import_file" class='sm-form-control' id="import_file">
                    <button class="button button-circle" id="submit-unggah" type="button" >Unggah Data Order</button>  
                </div>
                @if (isset($merchant))
                    @if (isset($user->active))   
                    </form>            
                    @endif
                @endif
            
            <div class="note-pickup">
                Informasi halaman :<br/>
                + Halaman ini digunakan untuk mengirimkan data pickup ke tim logistik PopBox.<br/>
                + Rekap data order dan lakukan upload data. <br/>
                + Format data harus sesuai dengan template, silahkan unduh di sini template data. <br/>
                <a href="{{ asset('download/pickupsubmit.xlsx') }}" class="button button-circle download">Unduh template  file</a><br/>                
                <br/>
                Catatan :<br/>
                + Informasi lebih lanjut silakan menghubungi logistic@popbox.asia atau ke nomor 021-29022537/8.<br/>
                + Konsumen akan menerima SMS dengan kode PIN setelah barang sampai di loker.<br/>
                + Barang akan berada di loker maksimal 3 hari setelah barang di drop.<br/>
                + Pastikan nomor telepon yang dituliskan di atas benar dan menggunakan nomor HP (bukan nomor telepon rumah/kantor).<br/>
                + Barang-barang yang ilegal (obat, senjata tajam, barang yang mudah meledak, binatang, tanaman hidup, pecah belah, barang/surat berharga, dll) dilarang untuk dikirimkan via PopBox.<br/>                               
                + Kurir PopBox akan mengambil paket sesuai dengan waktu preferensi yang kamu tentukan sebelumnya.<br/>
                + Pastikan paket kamu kirimkan memenuhi <span id="syarat">syarat dan ketentuan</span> dari PopBox.<br/>

                

            </div>
    </div>
</div>


@extends('layout.syarat') 
@stop

@section('js')


 <script type="text/javascript"> 
    $(document).ready(function() {         
        $("#syarat").click(function(){
            $(".bs-example-modal-lg").modal("show");                
        })        

       
        $(".alert").removeClass("alert-danger");
        $(".alert").removeClass("alert-success");
        @if (\Session::has('error'))
            $(".alert").addClass("alert-danger");            
            $(".alert ul li").text("{{ \Session::get('error') }}");
             $(".alert").show();
        @endif

        @if (\Session::has('success'))
            $(".alert").addClass("alert-success");
            $(".alert ul li").text("{{ \Session::get('success') }}");
             $(".alert").show();
        @endif
        
        @if (!isset($merchant))
            $(".alert").addClass("alert-warning");            
            $(".alert ul li").html("Selamat! Anda sudah mendaftar partner PopBox.  Mohon lengkapi data anda dengan mengklik  <a href='/dashboards/myshop/edit'>di sini</a>");
            $(".alert").show();
        @elseif (!isset($user->active))
            $(".alert").addClass("alert-warning");            
            $(".alert ul li").html("Untuk dapat melakukan order silakan melakukan aktivasi terlebih dahulu.<br/>
                        "Mohon mengecek email dengan subject [PopBox] Aktivasi Account Kamu (nama toko online) atau kirim kembali email aktivasi.");
            $(".alert").show();
        @endif  

       

        $("#submit-unggah").click(function(){
            @if (isset($merchant))
                @if (isset($user->active))            
                    $("#formupload").submit();
                @else                                
                    $('html, body').animate({scrollTop: $(".title-right-content").offset().top}, 500);
                @endif
            @else                                
                 $('html, body').animate({scrollTop: $(".title-right-content").offset().top}, 100);
            @endif
        });
    }); 
</script> 
@stop