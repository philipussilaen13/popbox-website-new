@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
<link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 

<div class="row row-upload">
    <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT ORDER BARU</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/panduan">
                        Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-9">
        <div class="title-right-content">Toko Saya</div>
        <section id="section-tab" class="right-content">

            <form method="post" action="{{ url('dashboards/myshop/update') }}" enctype="multipart/form-data">
             {{ csrf_field() }}
            <div class="row row-edit-merchant">
                <div class="col-md-10">
                    <div class="left"><img src="{{config('config.api')}}img/merchant/{{$merchant->picture}}" class="img-logo" /></div>
                    <div class="left title-shop-name">{{$merchant->merchant_name}}</div>
                </div>                
            </div>
            @if (session('status'))
                <div class="row">
                    <div class="col-md-9 alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="row">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Prefix 
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8">                      
                    {{$merchant->prefix}}
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Nama Pemilik Toko
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8">  
                    <input type="text" class="sm-form-control"  name="owner_name" value="{{$merchant->owner_name}}"/> 
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Email
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8"> 
                    {{$merchant->email}} 
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Nomor Handphone
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8">         
                     {{$merchant->phone}}
                </div>
            </div>   
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Link Website
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8">  
                    <input type="text" class="sm-form-control"  name="website" value="{{$merchant->website}}"/>
                </div>
            </div>

            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Kota
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8">                   
                    <input type="text" class="sm-form-control"  name="city" value="{{$merchant->city}}"/>
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Detail Alamat
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8">
                    <textarea name="address" class="sm-form-control">{{$merchant->address}}, {{$merchant->detail_address}}</textarea>
                    
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-3">
                    Waktu Pickup
                </div>
                <div class="col-md-1">
                    :
                </div>
                <div class="col-md-8"> 
                    : {{$merchant->time_pickup}}                   
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-4">                  
                </div>                
                <div class="col-md-8"> 
                    Jika ingin mengubah waktu penjemputan, silahkan masukkan data baru.<br/>
                    Jadwal pengambilan ini akan berlaku 1 hari setelah edit data.                    
                    <div>
                        <input id="checkbox-6" class="checkbox-style" name="days[]" value="Senin" id="senin" type="checkbox"><label for="checkbox-6" class="checkbox-style-1-label">Senin
                        <input id="checkbox-7" class="checkbox-style" name="days[]" value="Selasa" id="selasa" type="checkbox" ><label for="checkbox-7" class="checkbox-style-1-label">Selasa
                        <input id="checkbox-8" class="checkbox-style" name="days[]" value="Rabu" id="rabu"type="checkbox" ><label for="checkbox-8" class="checkbox-style-1-label">Rabu
                        <input id="checkbox-9" class="checkbox-style" name="days[]" value="Kamis" id="kamis" type="checkbox" ><label for="checkbox-9" class="checkbox-style-1-label">Kamis
                        <input id="checkbox-10" class="checkbox-style" name="days[]" value="Jumat" id="jumat" type="checkbox" ><label for="checkbox-10" class="checkbox-style-1-label">Jumat
                        <input id="checkbox-11" class="checkbox-style" name="days[]" value="Sabtu" id="sabtu" type="checkbox" ><label for="checkbox-11" class="checkbox-style-1-label">Sabtu
                    </div>
                        Preferensi waktu penjemputan paket:
                    <div>
                        <span class="clstime"><input type="radio" name="time" value="10:00 - 13:00">10:00 - 13:00</span>
                                 <input type="radio" name="time" value="13:00 - 16:00">13:00 - 16:00
                    </div>
                    <div>
                        Catatan :<br/>
                        Kurir PopBox akan mengambil barang pada waktu yang dipilih. Pada waktu tersebut, barang sudah siap diambil.
                    </div>
                </div>
            </div>
            <div class="row row-edit-merchant">
                <div class="col-md-4">                  
                </div>                
                <div class="col-md-8"> 
                      <button class="button button-circle" type="submit">Edit Data Toko</button>
                </div>
            </div>
        </section>
    </div>
</div>


@extends('layout.syarat') 
@stop

@section('js')


 <script type="text/javascript"> 
    jQuery(document).ready(function() {         
        $("#syarat").click(function(){
            $(".bs-example-modal-lg").modal("show");                
        })        
    }); 
</script> 
@stop