
@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
<link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 

<div class="row row-upload">
    <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT ORDER BARU</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                	 <a href="/dashboards/panduan">
                    	Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-9">
        <div class="title-right-content">Panduan</div>
        	<section id="section-tab " class="right-content">	
				<div class="accordion accordion-border clearfix" data-state="closed">

					<div class="acctitle acctitlec">Cara Membuat Bulk Order</div>
					<div class="acc_content clearfix howto" style="display: block;">
                        <ol type="1">
						    <li>
                                Klik tombol "BUAT ORDER BARU" kemudian pilih tab "BULK ORDER"
                            </li>
						    <li>
                                Siapkan file excel yang berisi data order yang formatnya sesuai template. <br/>Jika belum memiliki tempate filenya silakan klik tombol "UNDUH FILE FORMAT" terlebih dahulu
                            </li>
                            <li>
                                Jika data order sudah siap unggah data tersebut dengan cara klik pada tombol "Choose File" kemudian pilih file.
                            </li>
                        </ol>
                        <img src="{{asset('img/merchant/panduan-bulkorder.png')}}"><br/><br/>
                        <img src="{{asset('img/merchant/panduan-bulkorderexcel.png')}}">
                        
					</div>

					<div class="acctitle">Single Order</div>
					<div class="acc_content clearfix howto" style="display: none;">
                         <ol type="1">
                            <li>
                                Klik tombol "BUAT ORDER BARU" kemudian pilih tab "SINGLE ORDER"
                            </li>
                            <li>
                                Lengkapi data pada form yang tersedia kemudian klik button buat order
                            </li>                            
                        </ol>
                        <img src="{{asset('img/merchant/panduan-singleorder.png')}}"/>
                    </div>

					<!-- <div class="acctitle">Data Order</div>
					<div class="acc_content clearfix" style="display: none;">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, fugiat iste nisi tempore nesciunt nemo fuga? Nesciunt, delectus laboriosam nisi repudiandae nam fuga saepe animi recusandae. Asperiores, provident, esse, doloremque, adipisci eaque alias dolore molestias assumenda quasi saepe nisi ab illo ex nesciunt nobis laboriosam iusto quia nulla ad voluptatibus iste beatae voluptas corrupti facilis accusamus recusandae sequi debitis reprehenderit quibusdam. Facilis eligendi a exercitationem nisi et placeat excepturi velit!
					</div>
                    <div class="acctitle">Tracking</div>
                    <div class="acc_content clearfix" style="display: none;">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, fugiat iste nisi tempore nesciunt nemo fuga? Nesciunt, delectus laboriosam nisi repudiandae nam fuga saepe animi recusandae. Asperiores, provident, esse, doloremque, adipisci eaque alias dolore molestias assumenda quasi saepe nisi ab illo ex nesciunt nobis laboriosam iusto quia nulla ad voluptatibus iste beatae voluptas corrupti facilis accusamus recusandae sequi debitis reprehenderit quibusdam. Facilis eligendi a exercitationem nisi et placeat excepturi velit!
                    </div> -->
			</div>	
		</section>
   </div>
</div>


@extends('layout.syarat') 
@stop

@section('js')


 <script type="text/javascript"> 
    jQuery(document).ready(function() {         
        $("#syarat").click(function(){
            $(".bs-example-modal-lg").modal("show");                
        })        
    }); 
</script> 
@stop
