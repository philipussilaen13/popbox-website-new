@extends('layout.main-article')
@section('css')
    <!-- Bootstrap Switch CSS --> 
    <link rel="stylesheet" href="{{ asset('css/components/bs-switches.css') }}" type="text/css" /> 
    <!-- Radio Checkbox Plugin --> 
    <link rel="stylesheet" href="{{ asset('css/components/radio-checkbox.css') }}" type="text/css" /> 
@stop
@section('content')
<link rel="stylesheet" href="{{ asset('css/dashboardmerchant.css') }}" type="text/css" /> 

<div class="row row-upload">
    <div class="col-md-2 left-content">
        <div class="row">
            <div class="col-md-12">
                 <a href="/dashboards/uploadpickup"><button class="button button-circle" type="button">BUAT ORDER BARU</button>
            </div>
        </div>
        <section id="menu">
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/myshop">
                        Toko Saya
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/listuploadpickup">
                        Data Order
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                     <a href="/dashboards/panduan">
                        Panduan
                    </a>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <a href="/dashboards/tracking">
                        Tracking                    
                    </a>                
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-9">
        <div class="title-right-content">Tracking</div>
        <section id="section-tab" class="right-content">
        <form class="nobottommargin" id="trackForm" action="{{ url('track') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div align="center"><strong><span id="form-track-error" style="color:red"></span></strong></div>
                    <div class="">
                        <label for="name">{{ trans('tracking.parcel') }} <small>*</small></label>
                            <input type="text" id="order" name="order" value="" class="sm-form-control required" />
                                    </div>
                                    <br>
                                    <div align="center">
                                        <button class="button button-3d nomargin" type="button" id="template-contactform-submit" name="template-contactform-submit" value="submit">{{ trans('tracking.parcel-btn') }}</button>
                                    </div>
                                    <div class="table-responsive hide" id="track-result">
                                        <table class="table cart">
                                            <tbody>
                                                <tr class="cart_item">
                                                    <td class="notopborder cart-product-name">
                                                        <strong>Locker</strong>
                                                    </td>

                                                    <td class="notopborder cart-product-name">
                                                        {{-- Locker Name --}}
                                                        <span class="amount" id="track-name"></span>
                                                    </td>
                                                </tr>
                                                <tr class="cart_item">
                                                    <td class="cart-product-name">
                                                        <strong>Status</strong>
                                                    </td>

                                                    <td class="cart-product-name">
                                                        {{-- Status --}}
                                                        <span class="amount color lead" id="track-status"><strong></strong></span>
                                                    </td>
                                                </tr>
                                                <tr class="cart_item">
                                                    <td class="cart-product-name">
                                                        <strong>Store Time</strong>
                                                    </td>

                                                    <td class="cart-product-name">
                                                        <span class="amount" id="track-store"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </form>

        </section>

    </div>
</div>


@extends('layout.syarat') 
@stop

@section('js')


 <script type="text/javascript"> 
    jQuery(document).ready(function() {         
        $("#syarat").click(function(){
            $(".bs-example-modal-lg").modal("show");                
        })        


                                        $('#template-contactform-submit').on('click', function(event) {               
                                    $('#track-result').addClass('hide');
                        $.ajax({
                            url: '{{ url('track') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#trackForm').serialize(),
                            success: function(data){
                                if (data.isSuccess==true) {
                                    data = data.data;
                                    $('#trackForm').find('.form-process').fadeOut();
                                    $('#track-name').html(data.locker_name);
                                    $('#track-status').html(data.status);
                                    if (data.status=="ORDER CREATED"){
                                        $('#track-store').html("-");    
                                    }else{
                                        $('#track-store').html(data.storetime);
                                    }
                                    $('#track-result').removeClass('hide');
                                } else {
                                    $('#trackForm').find('.form-process').fadeOut();
                                    $('#form-track-error').html(data.errorMsg);
                                    $('#form-track-error').removeClass('hide');
                                }
                            }
                        })
                        .done(function() {
                            $('#trackForm').find('.form-process').fadeOut();
                            console.log("success");
                        })
                        .fail(function() {
                            $('#trackForm').find('.form-process').fadeOut();
                            console.log("error");
                        })
                        .always(function() {
                            $('#trackForm').find('.form-process').fadeOut();
                            console.log("complete");
                        });    
                    });
                                    })
                                </script>
                                @stop