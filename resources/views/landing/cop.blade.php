@extends('layout.main-article')
@section('css')
<style type="text/css">
    .divBorder{
       border: 1px solid #000;
    }
</style>
@stop
@section('content')
	<section id="content">
		<div class="container clearfix">
            <div class="heading-block center">
                <h2>Belanja di mitra PopBox, bayar dan ambil di loker PopBox</h2>
                <span>Mau bayar belanjaan saat pengambilan barang? <br> Bisa, pilih pembayaran COD di loker PopBox aja!</span>
            </div>
        	<div class="col-full">
        		<div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/COD-step-1.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Belanja di mitra PopBox pilih opsi pembayaran di tempat</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/COD-step-2.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Bayar di kasir Superindo (hanya cash) atau di loker PopBox (dengan eMoney Mandiri)</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third col_last nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/COD-step-3.png') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Ambil paket dengan kode PIN yang PopBox kirimkan</h3>
                        </div>
                    </div>
                </div>
        	</div>
    	</div>
	</section>
    <section id="content">
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>Lokasi Pembayaran</h2>
            </div>
            <div class="row">
               {{--  <div class="col-md-2">
                    <button class="button button-border button-rounded">Urutkan</button>
                </div> --}}
                <div class="col-md-12" id="div-form">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <form id="filter-form">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Kata Kunci</label>
                                            <input type="text" name="keyword" id="keyword" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Kota</label>
                                            <select class="form-control" name="city" id="city">
                                                <option value="all">Semua Kota</option>
                                                @foreach ($cities as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tipe Lokasi</label>
                                            <select class="form-control" name="type" id="type">
                                                <option value="all">Semua Tipe</option>
                                                @foreach ($types as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tipe Layanan</label>
                                            <select class="form-control" name="service" id="service">
                                                <option value="all">Semua Layanan</option>
                                                <option value="cop">COP</option>
                                                <option value="emoney">eMoney</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <span class="pull-right">Total Lokasi : <span id="counter">{{ count($lockerList) }}</span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            @if (!empty($lockerServices[0]))
                                @foreach ($lockerServices[0] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2.'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if (!empty($lockerServices[1]))
                                @foreach ($lockerServices[1] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2.'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                         <div class="col-md-4">
                            @if (!empty($lockerServices[2]))
                                @foreach ($lockerServices[2] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2.'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container clearfix">
            <div class="heading-block center">
                <h2>Cara Pembayaran</h2>
            </div>
            <div class="col-full">
                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/img_01.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih Bahasa</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/img_02.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih Pembayaran</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third col_last nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/img_03.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pilih Bayar Pesanan</h3>
                        </div>
                    </div>
                </div>

                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/img_04.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Masukkan Nomor Order</h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/img_05.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Tempelkan Kartu e-Money Untuk Pembayaran </h3>
                        </div>
                    </div>
                </div>
                <div class="col_one_third col_last nobottommargin">
                    <div class="feature-box media-box">
                        <div class="fbox-media" align="center">
                            <img src="{{ asset('img/cop/img_06.jpg') }}" style="width: 75%; height: auto;">
                        </div>
                        <div class="emphasis-title bottommargin-sm">
                            <h3 style="font-size: 18px;" class="font-secondary ls1 t400 center">Pembayaran Sukses</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </section>
@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#filter-form').submit(function(event) {
            event.preventDefault();
        });
        var delay = (function(){
            var timer = 0;
            return function (callback,ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            }
        })();

        $('#filter-form').on('change', function(event) {
            var keyword = $('input[name=keyword]').val();
            var city = $('select[name=city]').val();
            var type = $('select[name=type]').val(); 
            var service =$('select[name=service]').val();

            // reset to show
            $('.locker').hide('fast');

            delay(function(){
               var length = $('.locker:visible').length;
               $('#counter').html(length)
            },1000);
            filterLocker(keyword,city,type,service);
        });

        function filterLocker(keyword,city,type,service){
            // console.log('============');
            $('.locker').filter(function(index) {
                var dataKeyword = $(this).data('keyword');
                var dataCity = $(this).data('city');
                var dataType = $(this).data('type');
                var dataService = $(this).data('service');

                var status = false;
                var statusKeyword = false;
                var statusCity = false;
                var statusType = false;
                var statusService = false;

                if (keyword=='') {
                    statusKeyword = true;
                } else {
                    if (dataKeyword.toLowerCase().indexOf(keyword) >=0) statusKeyword = true;
                }
                if (city=='all') {
                    statusCity = true;
                } else {
                    if (city == dataCity) statusCity = true;
                }

                if (type=='all') {
                    statusType = true;
                } else {
                    if (type == dataType) statusType = true;
                }

                if (service=='all') {
                    statusService = true;
                } else {
                    if (dataService.toLowerCase().indexOf(service) >=0) statusService = true;
                }
                /*console.log(dataKeyword+"=="+dataCity+"=="+dataType);
                console.log(keyword+"=="+city+"=="+type);
                console.log(statusKeyword+"=="+statusCity+"=="+statusType);*/

                if (statusKeyword == true && statusCity == true && statusType == true && statusService == true) status = true;
                return status;
            }).show('fast');
        }

        /*$('input[name=keyword]').on('keyup', function(event) {
            delay(function(){
                $('input[name=keyword]').trigger('change');
            },1000);
        }); */
    });
</script>
@stop
