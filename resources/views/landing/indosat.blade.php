@extends('layout.main-article')
@section('meta')
@section('css')
<style type="text/css">

    .title{
        background: #FF0000;
        width: 50%;
        color: #FCEE21;
        border-radius: 5px;
        font-size: 37px;
        font-weight: bold;
        margin: 0 auto;
        text-align: center;        
    }

    .pad-two{
        padding: 4% 4% 2% 4%;
    }
    .box{
        border-radius: 16px;        
        margin-bottom: 100px;
        height: 253px;
        color:#000;
    }

    .box.paket{
        height: 206px;
    }

    .rounded{        
        color: #FFF;
        border-radius: 50%;
        width: 100px;
        height: 100px;
        text-align: center;
        padding-top: 26px;
        position: absolute;
        top: -60px;
        z-index: 1000;
        margin: 0 auto;        
        left: 0;
        right: 0;
        margin-left: auto;
        margin-right: auto;
    } 

    .curr{
        font-size: 20px;
        top: -10px;
        position: relative;
    }

    .plus{
        font-size: 59px;
        font-weight: bold;
        line-height: 57px;
    }

    .price{
        margin-top: 47px;
        font-size: 35px;
    }

    .voucher{
        color: #ff0000;
        font-size: 19px;
        line-height: 12px;
    }

    .val-voucher{
        font-size: 30px;
        margin-bottom: 8%;    
    }

    .marg-9{
        margin: 0% 9%;
    }

    .marg-9.head{
        margin-top: 8%;
    }

    .marg-22{
        margin: 0% 22%;
    }

    .rounded.one{
        background: #a6b8c6;
    }

    .box.one{
        border: 1px solid #bccad4;
        box-shadow: 0px 6px 0px #bccad4;
    }
    .box.one .plus{
        color: #bccad4;
    }

    .rounded.two{
        background: #f7941e;
    }

    .box.two{
        border: 1px solid #f7941e;
        box-shadow: 0px 6px 0px #f7941e;
    }
    .box.two .plus{
        color: #f7941e;
    }

    .rounded.tree{
        background: #91278f;
    }

    .box.tree{
        border: 1px solid #91278f;
        box-shadow: 0px 6px 0px #91278f;
    }

    .box.tree .plus{
        color: #91278f;
    }

    .rounded.four{
        background: #45aea5;
    }

    .box.four{
        border: 1px solid #45aea5;
        box-shadow: 0px 6px 0px #45aea5;
    }

    .box.four .plus{
        color: #45aea5;
    }

    .rounded.five{
        background: #0071bc;
    }

    .box.five{
        border: 1px solid #0071bc;
        box-shadow: 0px 6px 0px #0071bc;
    }

    .box.five .plus{
        color: #0071bc;
    }

    .wrong{
        font-size: 23px;
        line-height: 7px;
        margin-top: 56px;
        color:red;
        text-decoration:line-through;
    }

    .wrong.paket{
        line-height: 37px;
    }

    .wrong .curr{
        top: -9px;
        font-size: 15px;
    }

    .periode{
        font-size: 30px;   
    }

    .batas-promo{
        border: 1px solid #cc1f20;
        border-radius: 14px;
        width: 41%;
        margin: 0 auto;
        color: #cc1f20;
        font-size: 34px;
        font-weight: bold;
        line-height: 41px;
    }

    .batas-promo .note{
        font-size: 30px;
        font-weight: 100;
    }

    .syarat-title{
      font-size: 16px;
      margin-top: 4px;
      color: #000;
    }

    .line{
        border: 1px solid #cccccc;
        width: 90%;
        margin: 0 auto;
        margin-top: 34px;
        height: 1px;
    }

    .line.w11{
        width: 11%;
        margin-top: 6px;
    }

    .label-howto-fill{
        font-size: 34px;
        color: #000;
        font-weight: bold;
        margin-top: 17px;
    }

    .list-howto{
        font-size: 22px;
    }

    .location-popbox{
        width: 19%;
    }

    .bonus-note{
        font-size: 24px;
        margin-top: -46px;
    }

    @media (min-width: 1200px){     
        .container {
          width: 1024px;
        }
    }

    @media (max-width: 450px){
        .title{
              font-size: 13px;
        }
        .marg-9.head {
             margin-top: 28%;
        }

        .marg-22 {
          margin: 0% 9%;
        }

        .location-popbox {
          width: 59%;
        }

        .batas-promo{
            font-size: 20px;
            width: 80%;
            line-height: 28px;
        }

        .batas-promo .note{
            font-size: 16px;
            font-weight: 100;
        }

        .img-howto{
            width: 50%;
        }

        .bonus-note {
          font-size: 14px;
        }

    }
</style>
@stop
@section('content')
	<section id="content">
		<div class="container clearfix">           
        	<div class="row">        		
                <div class="col-md-12">
                    <img src="/img/indosat/banner.png"/>                
                </div>
        	</div>            
            <div class="row">
                <div class="col-md-12 pad-two">
                    <div class="title">
                        PULSA HIGH DENOM
                    </div>
                </div>
            </div>
            <div class="row marg-9 head">
                <div class="col-md-4">
                    <div class="rounded one">
                        Pulsa<br/>
                        150K
                    </div>
                    <div class="box one">
                        <div class="center price">
                            <span class="curr">Rp</span><span>150.000</span>
                        </div>
                        <div class="plus center">
                            +
                        </div>
                        <div class="voucher center">
                             Voucher Indomaret
                        </div>
                        <div class="val-voucher center">
                            <span class="curr">Rp</span><span>10.000</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="rounded two">
                        Pulsa<br/>
                        200K
                    </div>
                    <div class="box two">
                        <div class="center price">
                            <div class="wrong">
                                <span style='color:black'><span class="curr">Rp</span><span>200.000</span></span>
                             </div>
                            <span class="curr">Rp</span><span>195.000</span>
                        </div>
                        <div class="plus center">
                            +
                        </div>
                        <div class="voucher center">
                             Voucher Indomaret
                        </div>
                        <div class="val-voucher center">
                            <span class="curr">Rp</span><span>20.000</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="rounded tree">
                        Pulsa<br/>
                        250K
                    </div>
                    <div class="box tree">
                         <div class="center price">
                            <div class="wrong">
                                <span style='color:black'><span class="curr">Rp</span><span>250.000</span></span>
                             </div>
                            <span class="curr">Rp</span><span>243.000</span>
                        </div>
                        <div class="plus center">
                            +
                        </div>
                        <div class="voucher center">
                             Voucher Indomaret
                        </div>
                        <div class="val-voucher center">
                            <span class="curr">Rp</span><span>25.000</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row marg-22">            
                <div class="col-md-6">
                    <div class="rounded four">
                            Pulsa<br/>
                            500K
                    </div>
                    <div class="box four">
                        <div class="center price">
                            <div class="wrong">
                                <span style='color:black'><span class="curr">Rp</span><span>500.000</span></span>
                             </div>
                            <span class="curr">Rp</span><span>487.500</span>
                        </div>
                        <div class="plus center">
                            +
                        </div>
                        <div class="voucher center">
                             Voucher Indomaret
                        </div>
                        <div class="val-voucher center">
                            <span class="curr">Rp</span><span>50.000</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="rounded five">
                            Pulsa<br/>
                            1000K
                        </div>
                    <div class="box five">
                             <div class="center price">
                                <div class="wrong">
                                    <span style='color:black'><span class="curr">Rp</span><span>1.000.000</span></span>
                                 </div>
                                <span class="curr">Rp</span><span>975.000</span>
                            </div>
                            <div class="plus center">
                                +
                            </div>
                            <div class="voucher center">
                                 Voucher Indomaret
                            </div>
                            <div class="val-voucher center">
                                <span class="curr">Rp</span><span>100.000</span>
                            </div>
                    </div>         
                </div>           
            </div>

            <div class="row">
                <div class="col-md-12 center bonus-note">
                    <p> 
                        *Bonus voucher Indomaret akan dikirimkan 1x24 jam setelah transaksi berhasil
                    </p>
                </div>
            </div>
          
            <div class="row">
                    <div class="col-md-12">
                        <div class="title">
                            PAKET DATA
                        </div>
                    </div>
                </div>      
            <div class="row marg-9 head">
                <div class="col-md-3">
                     <div class="rounded one">
                        Freedom<br/>
                        M
                    </div>
                     <div class="box one paket">
                       <div class="center price">
                            <div class="wrong paket">
                                <span style='color:black'><span class="curr">Rp</span><span>59.000</span></span>
                             </div>
                            <span class="curr">Rp</span><span>50.150</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="rounded two">
                        Freedom<br/>
                        L
                    </div>
                    <div class="box two paket">
                        <div class="center price">
                            <div class="wrong paket">
                                <span style='color:black'><span class="curr">Rp</span><span>99.000</span></span>
                             </div>
                            <span class="curr">Rp</span><span>84.150</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="rounded tree">
                        Freedom<br/>
                        XL
                    </div>
                    <div class="box tree paket">
                        <div class="center price">
                            <div class="wrong paket">
                                <span style='color:black'><span class="curr">Rp</span><span>149.000</span></span>
                             </div>
                            <span class="curr">Rp</span><span>126.650</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="rounded five">
                        Freedom<br/>
                        XXL
                    </div>
                    <div class="box five paket">
                       <div class="center price">                            
                            <span class="curr">Rp</span><span>199.000</span>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="row">
                <div class="col-md-12 center periode">
                    Periode : 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 center">
                    <div class="batas-promo">
                        12 Mei - 12 Juli 2017<br/>
                        <span class="note">(batas terakhir promo)</span>
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 center syarat-title">
                    <span>*</span>Syarat dan ketentuan berlaku
                </div>
            </div>
            <div class="row">
                <div class="line"></div>
            </div>
    	</div>

	</section>
    <section id="content">
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-12 center label-howto-fill">
                    Cara Isi Paket <br/>
                    di Loker Popbox
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="line w11"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 center list-howto">
                    <div><img src="img/indosat/lp1.png" class="img-howto"/></div>
                    <p>1. Pilih Pembayaran</p>
                </div>
                <div class="col-md-4 center list-howto">
                    <div><img src="img/indosat/lp2.png" class="img-howto"/></div>
                    <p>2. Pilih isi pulsa</p>
                </div>
                <div class="col-md-4 center list-howto">
                    <div><img src="img/indosat/lp3.png" class="img-howto" /></div>
                    <p>3. Masukkan Nomor HP</p>
                </div>
            </div>
             <div class="row">
                <div class="col-md-4 center list-howto">
                    <div><img src="img/indosat/lp4.png" class="img-howto"/></div>
                    <p>4. Pilih paket data</p>
                </div>
                <div class="col-md-4 center list-howto">
                    <div><img src="img/indosat/lp5.png" class="img-howto"/></div>
                    <p>5. Pilih konfirmasi</p>
                </div>
                <div class="col-md-4 center list-howto">
                    <div><img src="img/indosat/lp6.png" class="img-howto"/></div>
                    <p>6. Pilih Pembayaran <span><img src="img/indosat/emoney.png"/></span></p>
                </div>
            </div>
        </div>        
    </section>
    <section id="content">
        <div class="container clearfix">
            <div class="center" style="margin:4%">                
                <img src="{{ asset('img/promopulsa/lokasi.png') }}" class="location-popbox">
            </div>
            <div class="row location">
               {{--  <div class="col-md-2">
                    <button class="button button-border button-rounded">Urutkan</button>
                </div> --}}
                <div class="col-md-12" id="div-form">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <form id="filter-form">                               
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Kota</label>
                                            <select class="form-control" name="city" id="city">
                                                <option value="all">Semua Kota</option>
                                                @foreach ($cities as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tipe Lokasi</label>
                                            <select class="form-control" name="type" id="type">
                                                <option value="all">Semua Tipe</option>
                                                @foreach ($types as $element)
                                                    <option value="{{ $element }}">{{ $element }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>
                            <span class="pull-right">Total Lokasi : <span id="counter">{{ count($lockerList) }}</span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default location">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            @if (!empty($lockerServices[0]))
                                @foreach ($lockerServices[0] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if (!empty($lockerServices[1]))
                                @foreach ($lockerServices[1] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                         <div class="col-md-4">
                            @if (!empty($lockerServices[2]))
                                @foreach ($lockerServices[2] as $element)
                                    <?php 
                                        $title = '<div align="center">';
                                        $title.= '<strong style="color:#FF6300;font-size:15px;">'.$element->name.'</strong><br>';
                                        $title.= '<strong>'.$element->address.'</strong><br>';
                                        $title.= $element->address_2+'<br>';
                                        $title.= $element->operational_hours;
                                        $title.= '</div>';
                                        $service = '';
                                        foreach ($element->services as $item) {
                                            if(empty($service)) $service = $item->service;
                                            else $service .= '-'.$item->service;
                                        }
                                    ?>
                                    <h4 class="locker" data-keyword="{{ $element->name }}-{{ $element->address }}-{{ $element->district }}-{{ $element->building_type }}" data-city="{{ $element->district }}" data-type="{{ $element->building_type }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}" data-html="true" data-service = "{{ $service }}">
                                        {{ $element->name }}
                                        @foreach ($element->services as $item)
                                            @if ($item->service=='cop')
                                                <small class="label label-info" style="font-size: 60%;"> {{ $item->service }}</small>
                                            @elseif ($item->service=='emoney')
                                                <small class="label label-warning" style="font-size: 60%;">{{ $item->service }}</small>
                                            @endif
                                        @endforeach
                                    </h4>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  

@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.location').hide();
        $('#filter-form').submit(function(event) {
            event.preventDefault();
        });
        var delay = (function(){
            var timer = 0;
            return function (callback,ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            }
        })();

        $(".location-popbox").click(function(){
            if ($('.location').is(":visible")){
                 $('.location').hide();
            }else{
                 $('.location').show();
            }
            
        })

        $('#filter-form').on('change', function(event) {
            var keyword = $('input[name=keyword]').val();
            var city = $('select[name=city]').val();
            var type = $('select[name=type]').val(); 
            var service =$('select[name=service]').val();

            // reset to show
            $('.locker').hide('fast');

            delay(function(){
               var length = $('.locker:visible').length;
               $('#counter').html(length)
            },1000);
            filterLocker(keyword,city,type,service);
        });

        function filterLocker(keyword,city,type,service){
            // console.log('============');
            $('.locker').filter(function(index) {                
                var dataCity = $(this).data('city');
                var dataType = $(this).data('type');                

                var status = false;                
                var statusCity = false;
                var statusType = false;                
                
                if (city=='all') {
                    statusCity = true;
                } else {
                    if (city == dataCity) statusCity = true;
                }

                if (type=='all') {
                    statusType = true;
                } else {
                    if (type == dataType) statusType = true;
                }

              
                /*console.log(dataKeyword+"=="+dataCity+"=="+dataType);
                console.log(keyword+"=="+city+"=="+type);
                console.log(statusKeyword+"=="+statusCity+"=="+statusType);*/

                if (statusCity == true && statusType == true) status = true;
                return status;
            }).show('fast');
        }

        /*$('input[name=keyword]').on('keyup', function(event) {
            delay(function(){
                $('input[name=keyword]').trigger('change');
            },1000);
        }); */
    });
</script>
@stop