@extends('layout.main-article')
@section('meta')
@section('css')
<style type="text/css">
    .content-promo{
        padding: 1% 10%;
    }

    .title{
       margin: 2%;
    }

    .img2{
        padding: 1% 5%;
    }

    .img2 img{
        width: 95%;
    }

    .how-to img{
        width: 75%;
    }

    .how-to.note2{
        margin-bottom: 10%;
    }
    
    .line{
        border-bottom: 1px solid #c0c0c0;
        width: 90%;
        margin: 0 auto;
        margin-bottom: 5%;
    }
    .ticket{
        margin-top: 1%;
    }

    .note1{
        font-weight: bold;
        font-size: 22px;
        margin: 3% 0%;
    }

    .img-popsend{
        padding: 3% 10%;
    }

    .popsend-how-to{
        padding:0% 17%;
    }

    .popsend-how-to img{
        height: 435px;
    }

    .note3{
        font-weight: bold;
        font-size: 20px;
        line-height: 23px;
    }

    .end-ticket{
          margin-bottom: 2%;
    }

    .service img{
        width:60%;
    }

    .label-service{
        padding: 3% 12%;
    }

    .title-howto-service{
        font-weight: bold;
        font-size: 36px;
    }

    .service .service-description p{
        font-size: 34px;
        line-height: 35px !important;
    }

    .img-banner{
        cursor:pointer;
        margin-top:1%;
    }

    .popbox-content, .popsend-content, .payment-content, .service-content{
        display:none;        
    } 

     


    @media (max-width: 450px){
        .note1{
            font-size: 14px;
            line-height: 14px !important;
        }

        .popsend-how-to img{
            height: auto;
        }

        .title-howto-service {         
          font-size: 12px;
        }

        .service .service-description p{
            font-size: 13px;  
            line-height: 15px !important;          
        }
    }
</style>
@stop
@section('content')
    <section id="banner">
        <div class="container clearfix">    
            <div class="col-full">
                <img src="img/ramadhan/banner.png">
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container clearfix">    
            <div class="col-full center title">
                <h1>PROMO!</h1>                
            </div>
            <div class="row content-promo">
                <div class="col-md-4 col-xs-5 col-sm-5">
                    <img src="img/ramadhan/promo1.png">
                </div>
                <div class="col-md-8 col-xs-7 col-sm-7 img2">
                    <img src="img/ramadhan/promo2.png">
                </div>
            </div>
            <div class="row">
                <div class="line"></div>
            </div>
        </div>
    </section>      
    <section id="content">
        <div class="container clearfix">
            <div class="col-full center title">
                <h1>FITUR</h1>                
            </div>
             <div class="row">   
                <div class="col-md-12">
                    <img src="/img/ramadhan/sub-banner1.png" class="img-banner" val="popbox"/>
                </div>
            </div>
            <div class="popbox-content">
                <div class="row">   
                    <div class="col-md-12 ticket">
                        <img src="/img/ramadhan/tiket-gratis-ongkir.png"/>
                    </div>
                </div>
                <div class="row">
                    <p class="center note1">
                        PopBox adalah sistem loker otomatis dimana anda bisa mengambil sendiri barang<br/> belanjaan anda kapanpun dan dimanapun.<br/>
                        PopBox Membantu meminimalisir<br/>
                        kemungkinan paket rusak dan barang hilang saat proses pengiriman
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-12 center">
                        <H1>CARA MENGGUNAKAN</H1>
                    </div>
                </div>
                <div class="row how-to note2">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2 center">
                        <div><img src="/img/ramadhan/how-to1.png"></div>
                        <p>Belanja online, <br/>pilih PopBox<br/> untuk alamat<br/> pengiriman</p>
                    </div>
                    <div class="col-md-2 center">
                        <div><img src="/img/ramadhan/how-to2.png"></div>
                        <p>Pilih lokasi<br/> PopBox<br/> terdekatmu</p>
                    </div>
                    <div class="col-md-2 center">
                        <div><img src="/img/ramadhan/how-to3.png"></div>
                        <p>Barang sampai, <br/>kamu akan terima <br/>SMS PIN untuk <br/>buka loker
                    </div>
                    <div class="col-md-2 center">
                        <div><img src="/img/ramadhan/how-to4.png"></div>
                        <p>Ambil paketmu <br/>dan enjoy</p>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
            </div>
        </div>         
    </section>
    <section id="content">
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-12">
                    <img src="/img/ramadhan/sub-banner2.png" class="img-banner" val="popsend" />
                </div>
            </div>         
            <div class="popsend-content">               
                <div class="row">   
                    <div class="col-md-12 ticket">
                        <img src="/img/ramadhan/tiket-popsend.png"/>
                    </div>
                </div>
                <div class="row img-popsend">                
                    <div class="col-md-3">
                        <img src="/img/ramadhan/popsend1.png"/>
                    </div>
                    <div class="col-md-5">
                        <img src="/img/ramadhan/popsend2.png">
                    </div>
                    <div class="col-md-3">
                        <img src="/img/ramadhan/popsend3.png">
                    </div>                
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img src="/img/ramadhan/footer-popsend.png"/>
                    </div>
                </div> 
                <div class="row popsend-how-to">
                    <div class="col-md-4 center">
                        <div><img src="/img/ramadhan/howto-popsend1.png"/></div>
                         <p class="note3">1. Kamu bisa Memilih<br/>lokasi loker terdekat</p>
                    </div>
                    <div class="col-md-4 center">
                        <div><img src="/img/ramadhan/howto-popsend2.png"/></div>
                        <p class="note3">2. Masukkan data<br/>Penerima</p>
                    </div>
                    <div class="col-md-4 center">
                        <div><img src="/img/ramadhan/howto-popsend3.png"/></div>
                        <p class="note3">3. Masukkan paketmu<br/> di loker dan akan<br/> dikirimkan ke tujuan </p>
                    </div>
                </div>  
            </div>          
        </div>  
    </section>
    <section id="content">
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-12">
                    <img src="/img/ramadhan/sub-banner3.png" class="img-banner" val="payment"/>
                </div>
            </div>
            <div class="payment-content">
                <div class="row">   
                    <div class="col-md-12 ticket end-ticket">
                        <img src="/img/ramadhan/isi-pulsa.png"/>
                    </div>
                </div>        
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container clearfix">
            <div class="row">   
                <div class="col-md-12">
                    <img src="/img/ramadhan/sub-banner4.png" class="img-banner" val="service"/>
                </div>
            </div>
            <div class="service-content">
                <div class="row">   
                    <div class="col-md-12 ticket">
                        <img src="/img/ramadhan/service.png"/>
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-12 ticket">
                        <img src="/img/ramadhan/note.png" class="label-service" />
                    </div>
                </div>
                <div class="row">   
                    <div class="col-md-12 ticket center title-howto-service">
                        <p>Cara mudah untuk laundry sepatu dan pakaianmu!!</p>
                    </div>
                </div>
                <div class="row service">   
                    <div class="col-md-2"></div>
                    <div class="col-md-3 center service-description">
                        <div><img src="/img/ramadhan/service-howto1.png"></div>
                        <p>Masukkan Data<br/>pesanan</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3 center service-description">
                        <div><img src="/img/ramadhan/service-howto2.png"></div>
                        <p>Letakkan<br/>laundry</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                
                <div class="row service">   
                    <div class="col-md-2"></div>
                    <div class="col-md-3 center service-description">
                        <div><img src="/img/ramadhan/service-howto3.png"></div>
                         <p>Proses<br/>laundry</p>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3 center service-description">
                        <div><img src="/img/ramadhan/service-howto4.png"></div>
                        <p>Pengambilan<br/>laundry</p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </section>
    
        

@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".img-banner").click(function(){
            var bannername = $(this).attr("val");            
            $('.' +bannername+ '-content').fadeToggle("fast");                            
        })
       
    });
</script>
@stop