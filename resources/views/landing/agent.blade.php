<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta charset="utf-8">
   
    <meta property="og:url" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
     <meta property="og:type" content="article" />
               <meta property="og:title" content="Promo Ramadhan By PopBox" />
                <meta property="og:description" content="Menyambut bulan Ramadhan. PopBox bagi-bagi kejutan! FREE ONGKIR sejabodetabek selama periode." />
                <meta property="og:image" content="{{ URL::asset('img/ramadhan/banner.png')}}" />

    <title>PopBox</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="PopBox Asia Team" />
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('img/favico.ico') }}">
    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/dark.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/magnific-popup.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/responsive.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ elixir('css/flag-icon.min.css') }}">    

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

     <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-72128831-1', 'auto');
            ga('send', 'pageview');
        </script>

</head>


<link rel="stylesheet" href="/css/agent.css">
<body  class="stretched">
  <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">
            <div id="header-wrap">
                <div class="container clearfix menu-head">
                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="{{ url('/') }}" class="standard-logo" data-dark-logo="{{ asset('img/logo/logo-dark.png') }}"><img src="{{ asset('img/logo/logo.png') }}" alt="PopBOX Logo"></a>
                        <a href="{{ url('/') }}" class="retina-logo" data-dark-logo="{{ asset('img/logo/logo-dark@2x.png') }}"><img src="{{ asset('img/logo/logo@2x.png') }}" alt="PopBOX Logo"></a>
                    </div>
     
                    <nav id="primary-menu" class="dark">
                        <ul class="sf-js-enabled" style="touch-action: pan-y;">                            
                             <li><a href="#" onclick="onGoto('#know-agent')" class="sf-with-ul">Apa itu PopBox Agent?</a></li>
                             <li><a href="#" onclick="onGoto('#keuntungan')" class="sf-with-ul">Keuntungan</a></li>
                             <li><a href="#" onclick="onGoto('#fitur')" class="sf-with-ul">Fitur</a></li>
                             <li><a href="#" onclick="onGoto('#syarat')" class="sf-with-ul">Syarat</a></li>
                             <li><a href="#" onclick="onGoto('#daftar')" class="sf-with-ul"><div>Jadi Agent PopBox</div></a></li>
                        </ul>

                    </nav>
                    <!-- #primary-menu end -->
                </div>
            </div>
        </header>
        <!-- #header end -->
        
        
        
    </div>

@section('content')
    <section id="content">
        <div class="container clearfix">
            <div class="row banner">
                <div class="col-md-12 col-xs-12 col-sm-12" >
                    <p class="banner-note"> 
                        Memiliki usaha logistik & retail<br/>
                        dengan <strong>modal minim</strong>
                    </p>
                    
                    <div class="btn-agent"  onclick="onGoto('#daftar')" id="know-agent">Jadi Agen PopBox</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 center">
                    <img src="img/agent/tablet1.png"/>
                </div>
                <div class="col-md-8 color-six know-agent">
                    <h4 class="color-six ha4"><strong>APAKAH</strong> PopBox Agent ?</h4>
                    <p class="color-six note1">
                        <strong>PopBox Agent</strong> adalah mintra bisnis <strong>PopBox</strong> dalam melayani konsumen sebagai <br/>
                        <strong>Virtual Locker</strong> untuk penyediaan layanan pengiriman penerimaan dan<br/>
                        pengembalian paket bagi masyarakat indonesia
                    </p>
                    <h4 class="color-six ha4"><strong>SIAPAKAH</strong> PopBox Agent ?</h4>
                    <p class="color-six note1">
                        Semua pemilik usaha retail yang memenuhi kriteria PopBox Agent,<br/>
                        pedagang pulsa, pemilik travel agen, pemilik laundry, koperasi<br/>
                        usaha fotokopi, dll.
                    </p>
                </div>
            </div>

            <div class="row iconkeuntungan" id="keuntungan">
                <div>
                    <div class="row">
                        <div class="col-md-12 center col-title">
                            <p class="title">
                                <strong>KEUNTUNGAN</strong> menjadi <strong>PopBox Agent</strong>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                          <div class="col-md-1">
                        </div> 
                        <div class="col-md-2 center">
                            <img src="img/agent/keuntungan11.png"/>
                            <p class="note">Memiliki usaha<br/>logistik & retail<br/>dengan modal minim</p>
                        </div>
                        <div class="col-md-2 center">
                            <img src="img/agent/keuntungan12.png"/>
                            <p class="note">Melengkapi bisnis<br/>usaha Anda saat ini</p>
                        </div>
                        <div class="col-md-2 center">
                            <img src="img/agent/keuntungan13.png"/>
                            <p class="note">Menjadi bagian dari<br/>pengusaha digital</p>
                        </div>
                        <div class="col-md-2 center">
                            <img src="img/agent/keuntungan14.png"/>
                            <p class="note">Mendapatkan<br/>penghasilan tambahan</p>
                        </div>   
                           <div class="col-md-2 center">
                            <img src="img/agent/keuntungan15.png"/>
                            <p class="note">Mendapat komisi<br/>yang menarik</p>
                        </div>
                        <div class="col-md-1">
                        </div>  
                    </div>                   
                </div>
            </div>

            <div class="row keuntungan">
                <div class="col-md-12 col-benefit">
                    <div class="row">
                        <div class="col-md-12 center col-title">
                            <p class="title">
                                <strong>BENEFIT & KOMISI</strong> PopBox Agent
                            </p>
                        </div>
                    </div>
                    <div class="row row-komisi">     
                        <div class="col-md-9 col-xs-12 col-sm-12 col-table">
                            <table class="table">
                                <thead>
                                    <th class="title1">Transaksi</th>
                                    <th class="title1 title2">Komisi/Benefit</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="row1">Logistik</td>
                                        <td class="row1 row12">s/d. 30%</td>
                                    </tr>
                                    <tr>
                                        <td class="row2">
                                            <span class="note1">Pulsa & Paket Data</span>
                                        </td>
                                        <td class="row2">                                            
                                            <span class="note1">s/d. 5%</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="row3">Tagihan</td><td class="row3 row32">Rp 2.000/trx</td>
                                    </tr>
                                    <tr>
                                        <td class="row4">Belanja</td><td class="row42">Harga Kompetitif</td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/>
                            <p class="note-table1">*Harga/ komisi dapat berubah sewaktu-waktu</p><p class="note-table2">*Selama periode promo berlangsung</p>
                        </div>   

                        <div class="col-md-3 center note2">   
                            <img src="img/agent/topupicon.png" width="70px"><br>             
                            <strong>Top Up Deposit</strong> <br>
                            dilakukan melalui<br/>
                            <strong>transfer Virtual Account</strong><br/>
                            (minimal deposit awal <br/>
                            Rp250.000)
                        </div>
                                         
                    </div>
                </div>
            </div>
            
            <div class="row fitur" id="fitur">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12 center title">
                            <strong>FITUR</strong> pada <strong>PopBox Agent</strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 center">
                            <img src="img/agent/fitur1.png"/>
                            <p>Aplikasi ini membantu<br/>Agent untuk menggunakan <br/> semua layanan PopBox</p>
                        </div>
                        <div class="col-md-6 center">
                            <img src="img/agent/fitur2.png"/>
                            <p>Popbox Agent bisa melakukan<br/>pembelian pulsa handphone.<br/>token listrik, asuransi dan bayar<br/>tagihan lainnya</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 center">
                            <img src="img/agent/fitur3.png"/>
                            <p>Agent bisa menerima dan<br/>mengirim paket dari semua<br/>titik PopBox, ke loker ataupun ke<br/>alamat tempat tinggal</p>
                        </div>
                        <div class="col-md-6 center">
                            <img src="img/agent/fitur4.png"/>
                            <p>Dengan layanan PopShop<br/>agen dapat melayani<br/>belanja online dengan<br/>merchant - merchant popbox</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 center">   
                        <img src="img/agent/tablet2.png"/>                 
                </div>
            </div>

            <div class="row syarat" id="syarat">
                <div class="row">
                    <div class="col-md-12 center">
                        <p class="title"><strong>SYARAT</strong> menjadi <strong>PopBox Agent</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 table-mobile">
                        <div class="title">Toko / Kios</div>
                        <ul>
                            <li><span class="note">Foto KTP</span></li>
                            <li><span class="note">Foto NPWP</span></li>
                            <li><span class="note">Foto Pemilik/PIC</span></li>
                            <li><span class="note">No Rekening Bank</span></li>
                            <li><span class="note">Foto Surat Keterangan usaha</span></li>
                            <li><span class="note">Min. usaha sudah berjalan 1 tahun (memiliki SKu)</span></li>
                            <li><span class="note">Tempat usaha harus bangunan permanen</span></li>
                            <li><span class="note">Minimal buka 5 hari dalam seminggu & 8 jam dalam sehari</span></li>
                        <ul>
                    </div>
                    <div class="col-md-12 table-mobile">
                        <div class="title">Badan Usaha</div>
                        <ul>
                        <li><span class="note">Akta</span></li>
                            <li><span class="note">Identitas pemilik usaha KTP & NPWP</span></li>
                            <li><span class="note">Min. usaha sudah berjalan 2 tahun</span></li>
                            <li><span class="note">No Rekening Bank</span></li>
                            <li><span class="note">Foto lokasi dan bangunan tempat usaha</span></li>
                            <li><span class="note">Tempat usaha harus bangunan permanen</span></li>
                            <li><span class="note">Minimal buka 5 hari dalam seminggu & 8 jam dalam sehari</span></li>
                        </ul>
                    </div>
                    <div class="col-md-12 table-web">
                        <table class="table">
                              <thead>
          <tr>
            <th class="tbl-head center">Toko / Kios</th>
            <th class="tbl-head center tb-right">Badan usaha</th>
            
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="left">
                <ul>
                    <li><span class="note">Foto KTP</span></li>
                    <li><span class="note">Foto NPWP</span></li>
                    <li><span class="note">Foto Pemilik/PIC</span></li>
                    <li><span class="note">No Rekening Bank</span></li>
                    <li><span class="note">Foto Surat Keterangan usaha</span></li>
                    <li><span class="note">Min. usaha sudah berjalan 1 tahun (memiliki SKu)</span></li>
                    <li><span class="note">Tempat usaha harus bangunan permanen</span></li>
                    <li><span class="note">Minimal buka 5 hari dalam seminggu & 8 jam dalam sehari</span></li>
                <ul>
            </td>            
            <td class="right tb-right">
                <ul>
                    <li><span class="note">Akta</span></li>
                    <li><span class="note">Identitas pemilik usaha KTP & NPWP</span></li>
                    <li><span class="note">Min. usaha sudah berjalan 2 tahun</span></li>
                    <li><span class="note">No Rekening Bank</span></li>
                    <li><span class="note">Foto lokasi dan bangunan tempat usaha</span></li>
                    <li><span class="note">Tempat usaha harus bangunan permanen</span></li>
                    <li><span class="note">Minimal buka 5 hari dalam seminggu & 8 jam dalam sehari</span></li>
                <ul>
            </td>
          </tr>          
        </tbody>
    </table>
           </div>
                </div>
            </div>
        </div>
       
    </section>
	<section id="content">
		<div class="container clearfix">   
            <div class="box" id="daftar">    
                <div class="title center">
                    <div class="note">DAFTAR MENJADI AGENT</div>
                    <div class="line-daftar"></div>
                    <div class="note2">Daftar sekarang dan dapatkan keuntungan menjadi agen PopBox</div>
                </div>
                <div class="daftar">
                <div class="row">
                    
                    <div class="modal fade" tabindex="-1" role="dialog" id="myModal">

                          <div class="modal-dialog" role="document">
                            <div class="modal-content">                          
                              <div class="modal-body">
                                 <div type="button" class="close button-close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                                 </button>
                                <div class="col-md-12 center error-msg">   
                                    <div class="message">
                                        Terima kasih telah mendaftar menjadi PopBox Agen, tim kami akan segera menghubungi anda dalam waktu 2x24 jam (hari kerja)
                                    </div>                                
                                </div>
                              </div>
                               
                           </div>
                        </div>
                    </div>                        
                </div>
                <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
                    <form class="form-horizontal" method="post" action="/submitpopboxagent" id="form-register"  enctype="multipart/form-data">         
                         {!! csrf_field() !!}
                        <div class="row">                    
                            <div class="col-md-12">
                                <input type="text" name="full_name" class="form-control" placeholder="Nama Lengkap" value="{{ old('full_name') }}" required>
                            </div>
                        </div><br/>                     
                        <div class="row">                    
                            <div class="col-md-12">
                                <input type="text" name="phone" class="form-control" placeholder="No Handphone" value="{{ old('phone') }}" required>
                            </div>
                        </div><br/>
                        <div class="row">                    
                            <div class="col-md-12">
                                <input type="text" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                            </div>
                        </div>
                        <br/>                  
                     
                        <div class="row">                    
                            <div class="col-md-12">
                                <input type="text" name="toko_name" class="form-control" placeholder="Nama Toko"  value="{{ old('toko_name') }}" required>
                            </div>
                        </div><br/>                                                     

                        <div class="row">                    
                            <div class="col-md-12">
                                <textarea name="address" rows="10" class="form-control" placeholder="Alamat Lengkap" required>{{ old('address') }}</textarea>
                            </div>
                        </div><br/>
                        
                        <div class="row">                    
                            <div class="col-md-12 checkbox-msg"></div>
                            </div>
                        </div>
                        <div class="row">                    
                            <div class="col-md-12 center">  
                                <input type="submit" value="DAFTAR" class="btn btn-primary ">
                            </div>                    
                        </div>                
                    </form>
                </div>
            </div>           
        </div>
    </section>   
 

     <footer id="footer" class="dark">
    <div class="container">
        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">
            <div class="row clearfix">
                <div class="col-md-5">
                    <div class="widget widget_links clearfix">
                        <div class="row clearfix">
                            <div class="col-md-8 bottommargin-sm clearfix" style="color:#888;">
                                <img src="{{ asset('img/footer-logo.png') }}" alt="PopBox Logo" style="display: block;" class="bottommargin-sm">
                                <p>{{ trans('layout.tag') }}</p>
                                <a href="https://www.facebook.com/pboxasia" class="social-icon si-small si-borderless si-colored si-rounded si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="https://twitter.com/popbox_asia" class="social-icon si-small si-borderless si-colored si-rounded si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="https://instagram.com/popbox_asia" class="social-icon si-small si-borderless si-colored si-rounded si-instagram">
                                    <i class="icon-instagram"></i>
                                    <i class="icon-instagram"></i>
                                </a>
                                <a href="https://www.youtube.com/channel/UCYGHLg4Xdc_IHD5DHdDIRTA" class="social-icon si-small si-borderless si-colored si-rounded si-youtube">
                                    <i class="icon-youtube"></i>
                                    <i class="icon-youtube"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/pt-popbox-asia-services" class="social-icon si-small si-borderless si-colored si-rounded si-linkedin">
                                    <i class="icon-linkedin"></i>
                                    <i class="icon-linkedin"></i>
                                </a>
                                <a href="#" class="social-icon si-small si-borderless si-colored si-rounded si-line">
                                    <i class="icon-line"></i>
                                    <i class="icon-line"></i>
                                </a>
                            </div>
                        </div>
                        <div>
                            {{ trans('layout.callus') }}<br>
                            <a href="tel:{{ trans('contact.contact-phone') }}"><i class="icon-call i-alt"></i>&nbsp; {{ trans('contact.contact-phone') }}</a><br>
                            <a href="mailto:info@popbox.asia"><i class="icon-mail i-alt"></i>&nbsp; info@popbox.asia</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row clearfix">
                        <div class="col-md-4 bottommargin-sm">
                            <div class="widget widget_links app_landing_widget_link clearfix">
                            </div>
                        </div>
                        <div class="col-md-4 bottommargin-sm">
                            <div class="widget widget_links clearfix">
                                <h4>{{ trans('layout.our-service') }}</h4>
                                <ul>
                                    <li><a href="https://popsend.popbox.asia">PopSend</a></li>
                                    <li><a href="http://shop.popbox.asia/">PopShop</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 bottommargin-sm">
                            <div class="widget widget_links clearfix">
                                <h4>PopBox</h4>
                                <ul>
                                    <li><a href="{{ url('blog') }}">{{ trans('layout.blog-url') }}</a></li>
                                    <li><a href="{{ url('contact') }}">{{ trans('layout.contact-url') }}</a></li>
                                    <li><a href="{{ url('career') }}">{{ trans('layout.career-url') }}</a></li>
                                    <li><a href="{{ url('faq') }}">{{ trans('layout.faq-url') }}</a></li>
                                    <li><a href="{{ url('merchant') }}">{{ trans('layout.merchant-url') }}</a></li>
                                    {{-- <li><a href="#">{{ trans('layout.partner-url') }}</a></li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div id="copyrights" class="nobg notoppadding">
        <div class="container clearfix">
            <div class="col_half">
                &copy; 2016 All Rights Reserved by PopBox Asia Services
                <br>
                {{-- <div class="copyright-links"><a href="#">{{ trans('layout.terms') }}</a> / <a href="#">{{ trans('layout.privacy') }}</a></div> --}}
                <div class="copyright-links">
                    <a href="http://www.geoplugin.com/geolocation/" target="_new">IP Geolocation</a> by <a href="http://www.geoplugin.com/" target="_new">geoPlugin</a>
                </div>
            </div>
        </div>
    </div>
    <!-- #copyrights end -->
</footer>
<!-- #footer end -->

</body>
<script type="text/javascript" src="{{ elixir('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
    <!-- Footer Scripts
    ============================================= -->
<script type="text/javascript" src="{{ elixir('js/functions.js') }}"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools --> 
{{-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5823e82fc2a9cb90"></script>  --}}
<script type="text/javascript" src="{{ elixir('js/validation.js') }}"></script>


<script type="text/javascript">


    jQuery(document).ready(function($) {
        
            @if(Session::has('flash_message'))
                $('#myModal').modal('toggle');
            @endif
        
          });

    function onGoto(idelement){
        $('html,body').animate({ scrollTop: $(idelement).offset().top }, 'slow');
    }
</script>



